@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                          <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">

                                  <li class="breadcrumb-item active"><a href="javascript:void(0);">     مصادر التمويل
                                        </a></li>
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                        @permission('Fundingsources-Create')
                            <div class="row">
                                <div class="col-md-12 text-right mb-5">
                                
                                    <a style="color:black !important; font-weight:bold;" href="{{ route('fundingsources.create',['executive_target_id'=>request()->input('executive_target_id')]) }}" class="btn btn-outline-success">أضف    </a>
                                 
                                </div>
                            </div>
                        @endpermission
                    </div>
          


                    <input value="{{Auth::user()->company_id}}" name="company_id" hidden/>



                    
                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                            <table class="table table-success table-striped">
                                <thead>
                                    <tr>
                                        <!-- <th>م</th> -->
                                        <th>   مصدر او جهه التمويل </th>
                                        <th>  قيمة  التمويل </th>
                                         
                                        <th class='text-center' style='width:26px;'></th>

                                        <th class='text-left' style='width:26px;'>تعديل</th>
                                       
                                        <th class='text-center' style='width:26px;'>مسح</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($fundingsourcescompview as $item)
                                        <tr>

                                            <td>{{ $item->funding_entity}}</td>
                                             <td>{{ $item->funding_value}}</td>
                                            
                                        

                                             <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('Fundingsource-Edit')
                                                     <li>
                                                            <a href="{{ route('fundingsources.edit', ['fundingsource' => $item->id]) }}"
                                                                data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="edit">
                                                                <i class="far fa-edit text-primary"></i>
                                                            </a>
                                                          
                                                        </li>
                                                    @endpermission
                                            </td>

                                             <td class="text-center">
                                                <ul class="table-controls">
                                                   
                                            </td>
                                        
                                            <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('Fundingsources-Delete')
                                                        
                                                <li>
                                                    <form action="{{route('fundingsources.destroy',['fundingsource'=>$item->id])}}" method="post">
                                                     @method('DELETE')
                                                     @csrf
                                                     <button style="border: none; background: none;" type="submit" class="fa fa-trash text-primary"></button>
                                                    </form>
                                                </li>
                                                    @endpermission

                                                </ul>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr class="text-center">
                                            <td colspan="7">{{ trans('home.no_data_found') }}</td>
                                        </tr>
                                    @endforelse

                                </tbody>

                            </table>
                        </div>
                        <div class="paginating-container">
                          
                            {{-- <a style="color:black !important; font-weight:bold;" href="" class="btn btn-outline-success">عــــــــودة </a> --}}

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
