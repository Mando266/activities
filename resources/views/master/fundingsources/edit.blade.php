@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                        <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                {{-- <li class="breadcrumb-item"><a href="javascript:void(0);"> بيانات الحركة</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('fundingsources.index') }}"> تعديل </a></li>
                                <li class="breadcrumb-item active"><a href="javascript:void(0);"> تعديل  </a></li> --}}
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                    </div>
                    <div class="widget-content widget-content-area">
                        <form id="createForm" action="{{ route('fundingsources.update', ['fundingsource' => $fundingsource]) }}" method="POST">
                            @csrf
                            @method('put')
                            <div class="form-row">




                            <div class="form-group col-md-3">
                                    <label for="nameInput">   مصدر او جهة التمويل  </label>
                                     <input type="text" class="form-control" id="emailInput" name="funding_entity"
                                        value="{{ old('funding_entity',$fundingsource->funding_entity) }}" >

                                    @error('funding_entity')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>






                                <div class="form-group col-md-3">
                                    <label for="nameInput">   قيمة التمويل  </label>
                                     <input type="text" class="form-control"  name="funding_value"
                                        value="{{ old('funding_value',$fundingsource->funding_value) }}" >

                                    @error('funding_value')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                    
                      </div>
                       <div class="form-row">
                    
                      
                      </div>
                  
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary mt-3">تعديل</button>
                                    <a href="{{ route('fundingsources.index',['executive_target_id'=>request()->input('executive_target_id')])}}" class="btn btn-danger mt-3">إلغاء / عودة </a>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
