@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                          <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                  {{-- <li class="breadcrumb-item active"><a href="javascript:void(0);">    متابعةالتنفيذ الفعلى للنشاط --}}
                                        </a></li>
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>

                        @permission('Activity-Create')
                            <div class="row">
                                <div class="col-md-12 text-right mb-5">
                                     {{-- @if(Auth::user()->company_id > 45)
                                        @else --}}
                                    {{-- <a style="color:black !important; font-weight:bold;" href="{{ route('tasks.create') }}" class="btn btn-outline-success">أضف    </a> --}}
                                      {{-- @endif --}}
                                </div>
                            </div>
                        @endpermission
                    </div>
                       <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />

<h3>Calendar</h3>

<div id='calendar'></div>





                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@push('scripts')

<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>
<script>
    $(document).ready(function() {
        // page is now ready, initialize the calendar...
        $('#calendar').fullCalendar({
            // put your options and callbacks here
            events : [
                @foreach($tasks as $task)
                {
                    title : '{{ $task->name }}',
                    start : '{{ $task->task_date }}',
                    url : '{{ route('tasks.edit', $task->id) }}'
                },
                @endforeach
            ]
        })
    });
</script>
@endpush
