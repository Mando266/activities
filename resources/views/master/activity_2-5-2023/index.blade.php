@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                          <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                  <li class="breadcrumb-item active"><a href="javascript:void(0);">    متابعةالتنفيذ الفعلى للنشاط
                                        </a></li>
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>

                        @permission('Activity-Create')
                            <div class="row">
                                <div class="col-md-12 text-right mb-5">
                                     {{-- @if(Auth::user()->company_id > 45)
                                        @else --}}
                                    <a style="color:black !important; font-weight:bold;" href="{{ route('activity.create',['executive_target_id'=>request()->input('executive_target_id')]) }}" class="btn btn-outline-success">أضف    </a>
                                      {{-- @endif --}}
                                </div>
                            </div>
                        @endpermission
                    </div>
                      {{-- <form action="{{ route('activity.index') }}">
                    <div class="form-row">

                                  <div class="form-group col-md-3">
                                    <label for="nameInput">  اسم  البـــنك</label>
                                      <select class=" form-control"  data-live-search="true"
                                name="name" data-size="10" title="ÃÎÊÑ">
                                <option value="">اختر</option>
                                @foreach ($bankName as $item)
                                    <option value="{{ $item->name }}"
                                        {{ $item->name == old('name',request()->input('name')) ? 'selected' : '' }}>{{ $item->name }}
                                    </option>
                                @endforeach
                            </select>
                                    @error('main_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>


                    </div>
                        <div class="col-md-8 text-center">
                        <button type="submit" class="btn btn-primary mt-3"> ابحث</button>
                            <a href="{{route('Bank.index') }}" class="btn btn-danger mt-3">الغاء
                            </a>
                    </div>
                    </div>
                    </form> --}}
                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <!-- <th>م</th> -->
                                        <th>  اسم النشاط </th>
                                        <th>  تاريخ مخطط لبداية النشاط</th>
                                         <th>  تاريخ مخطط لنهاية النشاط</th>
                                          <th>  تاريخ فعلي لبداية النشاط</th>
                                           <th>  تاريخ فعلي لنهاية النشاط</th>
                                           <th>    المدة المخططة للتنفيذ باليوم </th>
                                             <th>       الموقف </th>
                                             <th></th>
                                             <th></th>
                                              <th></th>
                                                <th></th>



                                        <th class='text-center' style='width:26px;'>تعديل</th>
                                        {{-- <th class='text-center' style='width:26px;'>أظهار</th> --}}
                                        <th class='text-center' style='width:26px;'>مسح</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($activityfollowexecutiveiew as $item)
                                        <tr >

                                            <td style="white-space: nowrap;">{{ $item->activity_name}}</td>
                                             <td>{{ $item->planned_from}}</td>
                                              <td>{{ $item->planned_to}}</td>
                                               <td>{{ $item->actual_from}}</td>
                                                <td>{{ $item->actual_to}}</td>
                                                 <td></td>
                                                   <td></td>
                                                     <td></td>



                                            {{-- <td>{{ $item->name}}</td>
                                            <td>{{ $item->unit}}</td> --}}



                                            {{-- @if(Auth::user()->company_id > 45)
                                            @else --}}
                                                <td>
                                                    <a style="color:black !important; font-weight:bold;white-space: nowrap;font-size:14px;"
                                                        href="{{ route('empinactivity.index', ['activity_id' => $item->id]) }}"
                                                        class="btn btn-outline-success">   الموظفين المشاركين فى النشاط </a>
                                                </td>
                                                  <td>
                                                    <a style="color:black !important; font-weight:bold;white-space: nowrap;font-size:14px;"
                                                        href="{{ route('activityfollowup.index', ['activity_id' => $item->id,'activity_name' => $item->activity_name,
                                                            'planned_from' => $item->planned_from,'planned_to' => $item->planned_to]) }}"
                                                        class="btn btn-outline-success">      متابعة حالة النشاط فى اسبوع </a>
                                                </td>
                                                   <td>
                                                    <a style="color:black !important; font-weight:bold;white-space: nowrap;font-size:14px;"
                                                        href="{{ route('admininactivity.index', ['activity_id' => $item->id]) }}"
                                                        class="btn btn-outline-success">         القطاع المشارك فى النشاط  </a>
                                                </td>
                                            <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('Activity-Edit')
                                                     <li>
                                                            <a href="{{ route('activity.edit', ['activity' => $item->id]) }}"
                                                                data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="edit">
                                                                <i class="far fa-edit text-primary"></i>
                                                            </a>
                                                            {{-- @endif --}}
                                                        </li>
                                                    @endpermission
                                            </td>
{{--
                                            @if(Auth::user()->company_id > 45)
                                            @else --}}
                                            <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('activity-Delete')
                                                        <li>
                                                            <form
                                                                action="{{ route('activity.destroy', ['activity' => $item->id]) }}"
                                                                method="post">
                                                                @method('DELETE')
                                                                @csrf
                                                                <button type="submit" class="fa fa-trash text-primary"></button>
                                                            </form>
                                           {{-- @endif --}}
                                                        </li>
                                                    @endpermission

                                                </ul>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr class="text-center">
                                            <td colspan="15">{{ trans('home.no_data_found') }}</td>
                                        </tr>
                                    @endforelse

                                </tbody>

                            </table>
                        </div>
                        <div class="paginating-container">
                      


                            {{ $activityfollowexecutiveiew->appends(request()->query())->links() }}
                            {{-- <a style="color:black !important; font-weight:bold;" href="" class="btn btn-outline-success">عــــــــودة </a> --}}

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
