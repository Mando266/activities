@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                        <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                {{-- <li class="breadcrumb-item"><a href="javascript:void(0);"> الاكواد</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('activity.index') }}"> تعديل نشاط</a></li>
                                <li class="breadcrumb-item active"><a href="javascript:void(0);"> تعديل اسم النشاط</a></li> --}}
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                    </div>
                    <div class="widget-content widget-content-area">
                        <form id="createForm" action="{{ route('empinactivity.update', ['empinactivity' => $empinactivity]) }}" method="POST">
                            @csrf
                            @method('put')
                            <div class="form-row">
                               <div class="form-group col-md-3">
                                    <label for="nameInput"> اسم  الموظف</label>
                                         <select class=" form-control" id="stationNamesInput" data-live-search="true"
                                        name="employee_id" data-size="10" title="">
                                        <option value="">اختر</option>
                                        @foreach ($emp as $item)
                                            <option value="{{ $item->id }}"
                                                {{ $item->id == old('employee_id', $empinactivity->employee_id) ? 'selected' : '' }}>{{ $item->name }}
                                            </option>
                                        @endforeach
                                    </select>

                                    @error('employee_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                       <div class="form-group col-md-3">
                                    <label for="nameInput">  الوحدة الادارية </label>
                                          <select class=" form-control" id="stationNamesInput" data-live-search="true"
                                        name="admin_unit_id" data-size="10" title="">
                                        <option value="">اختر</option>
                                        @foreach ($adminunit as $item)
                                            <option value="{{ $item->id }}"
                                                {{ $item->id == old('admin_unit_id', $empinactivity->admin_unit_id) ? 'selected' : '' }}>{{ $item->name }}
                                            </option>
                                        @endforeach
                                    </select>

                                    @error('admin_unit_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                             
                                  <div class="form-group col-md-4">
                                    <label for="nameInput">      ملاحظات </label>
                                     <input type="text" class="form-control" id="emailInput" name="notes"
                                        value="{{ old('notes', $empinactivity->planned_from) }}" placeholder=" ملاحظات " autocomplete="off">

                                    @error('notes')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>


                      </div>


</div>

                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary mt-3">تعديل</button>
                                    <a href="{{ route('empinactivity.index') }}" class="btn btn-danger mt-3">إلغاء / عودة </a>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
