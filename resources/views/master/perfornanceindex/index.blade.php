@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-one">
                    <div class="widget-heading">
                          <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                  <li class="breadcrumb-item active"><a href="javascript:void(0);">    مؤشر  الاداء
                                        </a></li>
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                        @permission('perfornanceindex-Create')
                            <div class="row">
                                 @if(Auth::user()->company_id > 45)
                                @else
                                <div class="col-md-12 text-right mb-5">
                                    <a style="color:black !important; font-weight:bold;" href="{{ route('perfornanceindex.create') }}" class="btn btn-outline-success">اضافة مؤشر اداء   </a>
                                       @endif
                                </div>
                            </div>
                        @endpermission
                    </div>
                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <!-- <th>م</th> -->
                                        <th>  كود مؤشر الاداء </th>
                                        <th>  اسم مؤشر الاداء </th>


                                        <th class='text-center' style='width:26px;'>تعديل</th>

                                        <th class='text-center' style='width:26px;'>مسح</th>

                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ( $perfornanceindex as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->perfornance_index_name }}</td>
                                             
                              
                                            <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('perfornanceindex-Edit')
                                                        <li>
                                                            <a href="{{ route('perfornanceindex.edit', ['perfornanceindex' => $item->id]) }}"
                                                                data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="edit">
                                                                <i class="far fa-edit text-primary"></i>
                                                            </a>
                                                           
                                                        </li>
                                                    @endpermission
                                            </td>
                                        
                                           
                                          
                                            <td class="text-center">
                                                <ul class="table-controls">
                                                @permission('perfornanceindex-List')
                                                    <li>
                                                    <form action="{{route('perfornanceindex.destroy',['perfornanceindex'=>$item->id])}}" method="post">
                                                     @method('DELETE')
                                                     @csrf
                                                     <button style="border: none; background: none;" type="submit" class="fa fa-trash text-primary"></button>
                                                    </form>
                                                    </li>
                                                    @endpermission
                                                </ul>
                                            </td>

                                            @endforeach
                                        </tr>

                                        <tr class="text-center">
                                            <!-- <td colspan="7">{{ trans('home.no_data_found') }}</td> -->
                                        </tr>


                                </tbody>

                            </table>
                        </div>
                        <div class="paginating-container">
                            {{ $perfornanceindex->links() }}
                             <!-- <a style="color:black !important; font-weight:bold;" href="" class="btn btn-outline-success">عــــــــودة </a>  -->

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

