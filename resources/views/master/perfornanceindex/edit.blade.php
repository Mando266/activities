@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-one">
                    <div class="widget-heading">
                        <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                {{-- <li class="breadcrumb-item"><a href="javascript:void(0);"> الاكواد</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('activity.index') }}"> تعديل نشاط</a></li>
                                <li class="breadcrumb-item active"><a href="javascript:void(0);"> تعديل اسم النشاط</a></li> --}}
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                    </div>
                    <div class="widget-content widget-content-area">
                        <form id="createForm" action="{{ route('perfornanceindex.update', ['perfornanceindex' => $perfornanceindex]) }}" method="POST">
                            @csrf
                            @method('put')
                            <div class="form-row">
                                     
                                
                            <div class="form-group col-md-4">
                                    <label for="telephoneInput">كود مؤشر الاداء </label>
                                    <input type="text" class="form-control" id="phoneInput" name="id"
                                        value="{{ old('id', $perfornanceindex->id) }}" placeholder=" " autocomplete="off">
                                    @error('id')
                                       
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>

                                   <div class="form-group col-md-4">   
                                    <label for="telephoneInput"> اسم مؤشر الاداء </label>
                                    <input type="text" class="form-control" id="phoneInput" name="perfornance_index_name"
                                        value="{{ old('perfornance_index_name', $perfornanceindex->perfornance_index_name) }}" placeholder=" " autocomplete="off">
                                    @error('perfornance_index_name')
                                       
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>

</div>
</div>

                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary mt-3">تعديل</button>
                                    <a href="{{ route('perfornanceindex.index') }}" class="btn btn-danger mt-3">إلغاء / عودة </a>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
