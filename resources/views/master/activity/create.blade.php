@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                        <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active"><a href="javascript:void(0);"> أضف نشاط جديد </a></li>
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                    </div>


                    <br>
                    <p style="font-size: 12pt;color:red;">يجب ادخال تاريخ النشاط بين تاريخي مخطط  الخطة *</p>
                    <div class="widget-content widget-content-area">

                        <form id="createForm" action="{{ route('activity.store') }}" method="POST">
                            @csrf
                            <input type="text" value="{{ request()->input('executive_target_id') }}"
                                name="executive_target_id" hidden>


                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="nameInput"> اسم النشاط</label>
                                    <input type="text" class="form-control" id="emailInput" name="activity_name"
                                        value="{{ old('activity_name') }}" placeholder="  اسم النشاط" autocomplete="off"
                                        required>
                                    @error('activity_name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="nameInput"> تاريخ مخطط بداية النشاط</label>
                                    <input type="date" class="form-control" id="target_from" name="planned_from"
                                        value="{{ request()->input('target_from') }}" placeholder="  " autocomplete="off">
                                    @error('planned_from')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="nameInput"> تاريخ مخطط نهاية النشاط</label>
                                    <input type="date" class="form-control" id="target_to" name="planned_to"
                                        value="{{ request()->input('target_to') }}" placeholder="  " autocomplete="off">
                                    <span id="error" style="font-size: 14px;color:red;"></span>

                                    @error('planned_to')
                                        <div style="color:red">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>




                                <div class="form-group col-md-4">
                                    <label for="nameInput"> تاريخ فعلى لبداية النشاط</label>
                                    <input type="date" class="form-control" id="emailInput" name="actual_from"
                                        value="{{ old('actual_from') }}" placeholder=" الوحدة الادارية" autocomplete="off"
                                        required>

                                    @error('actual_from')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-3">

                                    <label for="nameInput"> تاريخ فعلى لنهاية النشاط</label>
                                    <input type="date" class="form-control" id="emailInput" name="actual_to"
                                        value="{{ old('actual_to') }}" placeholder=" الوحدة الادارية" autocomplete="off"
                                        required>

                                    @error('actual_to')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" id="submit" class="btn btn-primary mt-3" disabled>أضف</button>
                                    <a href="{{ route('activity.index',['executive_target_id'=>request()->input('executive_target_id'),'target_from'=>request()->input('target_from'),'target_to'=>request()->input('target_from')]) }}" class="btn btn-danger mt-3">إلغاء / عودة
                                    </a>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    @endsection

    @push('scripts')
        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>

        <script>
            $(document).ready(function() {

                $("#target_to").on('input', function(e) {
                    e.preventDefault();
                    var selectedStartDate = new Date($("#target_from").val());
                    var selectedEndtDate = new Date($("#target_to").val());
                    var startDate = new Date("{{ request()->input('target_from') }}");
                    var endDate = new Date("{{ request()->input('target_to') }}");
                    
                    if (selectedStartDate < startDate || selectedEndtDate > endDate) {
                        // Selected date is outside the valid range, display an error message
                        $("#error").text("يجب ادخال التاريخ الصحيح  2023-01-01 و 2023-06-22");
                        $(this).val("{{ request()->input('target_to') }}"); // Clear the invalid selection
                        $("#submit").prop("disabled", true);
                        //   console.log(startDate);
                        //   console.log(endDate);
                    } else {
                        // Date is valid, no error message
                        $("#error").text("");
                        $("#submit").removeAttr("disabled");
                    }

                });
            });
        </script>
    @endpush
