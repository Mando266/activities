@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                        <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                {{-- <li class="breadcrumb-item"><a href="javascript:void(0);"> الاكواد</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('activity.index') }}"> تعديل نشاط</a></li>
                                <li class="breadcrumb-item active"><a href="javascript:void(0);"> تعديل اسم النشاط</a></li> --}}
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                    </div>
                    <div class="widget-content widget-content-area">
                        <form id="createForm" action="{{ route('activity.update', ['activity' => $activity]) }}" method="POST">
                            @csrf
                            @method('put')
                            <div class="form-row">
              
                       <div class="form-group col-md-4">
                                    <label for="nameInput"> اسم  النشاط</label>
                                     <input type="text" class="form-control" id="emailInput" name="activity_name"
                                        value="{{ old('activity_name', $activity->activity_name) }}" placeholder="  اسم النشاط" autocomplete="off" disabled>

                                    @error('activity_name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="nameInput">   تاريخ مخطط  بداية النشاط</label>
                                     <input type="date" class="form-control" id="emailInput" name="planned_from"
                                        value="{{ old('planned_from', $activity->planned_from) }}" placeholder="  " autocomplete="off">

                                    @error('planned_from')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                     <div class="form-group col-md-4">
                                    <label for="nameInput">   تاريخ مخطط  نهاية النشاط</label>
                                     <input type="date" class="form-control" id="emailInput" name="planned_to"
                                        value="{{ old('planned_to', $activity->planned_to) }}" placeholder="  " autocomplete="off">

                                    @error('planned_to')
                                        <div style="color:red">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>


                      </div>
                      
                       <div class="form-group col-md-4">

                                    <label for="nameInput">   تاريخ فعلى  لبداية النشاط</label>
                                     <input type="date" class="form-control" id="emailInput" name="actual_from"
                                        value="{{ old('actual_from', $activity->actual_from) }}" placeholder=" الوحدة الادارية" autocomplete="off">

                                    @error('actual_from')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                     <div class="form-group col-md-3">

                                    <label for="nameInput">   تاريخ فعلى  لنهاية النشاط</label>
                                     <input type="date" class="form-control" id="emailInput" name="actual_to"
                                        value="{{ old('actual_to', $activity->actual_to) }}" placeholder=" الوحدة الادارية" autocomplete="off">

                                    @error('actual_to')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                         </div>

</div>

                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary mt-3">تعديل</button>
                                    <a href="{{ route('activity.index',['executive_target_id'=>request()->input('executive_target_id')])}}" class="btn btn-danger mt-3">إلغاء / عودة </a>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
