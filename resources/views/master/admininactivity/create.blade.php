@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                        <nav class="breadcrumb-two" aria-label="breadcrumb">
                             <ol class="breadcrumb">
    
                                <li class="breadcrumb-item active"><a href="javascript:void(0);"> أضف    متابعة جديدة </a></li>
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                    </div>
                    <div class="widget-content widget-content-area">
                        <form id="createForm" action="{{ route('admininactivity.store') }}" method="POST">
                            @csrf
                            <input type="text" value="{{request()->input('activity_id')}}" name="activity_id" hidden>
                            <div class="form-row">
                                              <div class="form-group col-md-3">
                                    <label for="nameInput">     الوحدة الادارية </label>
                                         <select class=" form-control" id="stationNamesInput" data-live-search="true"
                                        name="admin_unit_id" data-size="10" title="">
                                        <option value="">اختر</option>
                                        @foreach ($dropadminincompview as $item)
                                            <option value="{{ $item->id }}"
                                                {{ $item->id == old('id') ? 'selected' : '' }}>{{ $item->name }}
                                            </option>
                                        @endforeach
                                    </select>

                                    @error('admin_unit_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                       </div>
                         <div class="form-row">
                         <div class="form-group col-md-3">
                                    <label for="nameInput">   تاريخ مخطط  بداية النشاط</label>
                                     <input type="date" class="form-control" id="emailInput" name="planned_from"
                                        value="{{ old('planned_from') }}" placeholder="  " autocomplete="off">

                                    @error('planned_from')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                     <div class="form-group col-md-3">
                                    <label for="nameInput">   تاريخ مخطط  نهاية النشاط</label>
                                     <input type="date" class="form-control" id="emailInput" name="planned_to"
                                        value="{{ old('planned_to') }}" placeholder="  " autocomplete="off">

                                    @error('planned_to')
                                        <div style="color:red">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>


                      </div>
                       <div class="form-row">
                       <div class="form-group col-md-3">

                                    <label for="nameInput">   تاريخ فعلى  لبداية النشاط</label>
                                     <input type="date" class="form-control" id="emailInput" name="actual_from"
                                        value="{{ old('actual_from') }}" placeholder=" الوحدة الادارية" autocomplete="off">

                                    @error('actual_from')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                     <div class="form-group col-md-3">

                                    <label for="nameInput">   تاريخ فعلى  لنهاية النشاط</label>
                                     <input type="date" class="form-control" id="emailInput" name="actual_to"
                                        value="{{ old('actual_to') }}" placeholder=" الوحدة الادارية" autocomplete="off">

                                    @error('actual_to')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                         </div>

                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary mt-3">أضف</button>
                                        <a href="{{ route('admininactivity.index') }}" class="btn btn-danger mt-3">إلغاء / عودة
                                        </a>
                                    </div>
                                </div>




                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
