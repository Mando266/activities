@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                          <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                  <li class="breadcrumb-item active"><a href="javascript:void(0);">  القطاع المشارك فى النشاط
                                        </a></li>
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>

                        @permission('AdminInActivity-Create')
                            <div class="row">
                                <div class="col-md-12 text-right mb-5">
                                     {{-- @if(Auth::user()->company_id > 45)
                                        @else --}}
                                    <a style="color:black !important; font-weight:bold;" href="{{ route('admininactivity.create',['activity_id'=>request()->input('activity_id')]) }}" class="btn btn-outline-success">أضف    </a>
                                      {{-- @endif --}}
                                </div>
                            </div>
                        @endpermission
                    </div>
                      {{-- <form action="{{ route('activity.index') }}">
                    <div class="form-row">

                                  <div class="form-group col-md-3">
                                    <label for="nameInput">  اسم  البـــنك</label>
                                      <select class=" form-control"  data-live-search="true"
                                name="name" data-size="10" title="ÃÎÊÑ">
                                <option value="">اختر</option>
                                @foreach ($bankName as $item)
                                    <option value="{{ $item->name }}"
                                        {{ $item->name == old('name',request()->input('name')) ? 'selected' : '' }}>{{ $item->name }}
                                    </option>
                                @endforeach
                            </select>
                                    @error('main_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>


                    </div>
                        <div class="col-md-8 text-center">
                        <button type="submit" class="btn btn-primary mt-3"> ابحث</button>
                            <a href="{{route('Bank.index') }}" class="btn btn-danger mt-3">الغاء
                            </a>
                    </div>
                    </div>
                    </form> --}}
                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                              <table class="table table table-striped">
                                <thead>
                                    <tr>

                                         <th>   كود القطاع</th>
                                        <th>     اسم القطاع </th>
                                        <th class='text-center' style='width:26px;'>تعديل</th>
                                        <th class='text-center' style='width:26px;'>مسح</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($dropadminincompview as $item)
                                        <tr class="table-secondary">


                                             <td>{{ $item->admin_unit_id}}</td>
                                              <td>{{ $item->admin_unit_name}}</td>
                                            <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('AdminInActivity-Edit')
                                                     <li>
                                                            <a href="{{ route('admininactivity.edit', ['admininactivity' => $item->id,'activity_id'=>request()->input('activity_id')]) }}"
                                                                data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="edit">
                                                                <i class="far fa-edit text-primary"></i>
                                                            </a>
                                                            {{-- @endif --}}
                                                        </li>
                                                    @endpermission
                                            </td>
{{--
                                            @if(Auth::user()->company_id > 45)
                                            @else --}}
                                            <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('AdminInActivity-Delete')
                                                        <li>
                                                            <form
                                                                action="{{ route('admininactivity.destroy', ['admininactivity' => $item->id]) }}"
                                                                method="post">
                                                                @method('DELETE')
                                                                @csrf
                                                                <button type="submit" class="fa fa-trash text-primary"></button>
                                                            </form>
                                           {{-- @endif --}}
                                                        </li>
                                                    @endpermission

                                                </ul>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr class="text-center">
                                            <td colspan="7">{{ trans('home.no_data_found') }}</td>
                                        </tr>
                                    @endforelse

                                </tbody>

                            </table>
                        </div>
                        <div class="paginating-container">
                            {{ $dropadminincompview->links() }}
                            {{-- <a style="color:black !important; font-weight:bold;" href="" class="btn btn-outline-success">عــــــــودة </a> --}}

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
