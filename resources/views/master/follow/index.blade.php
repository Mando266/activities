@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                          <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                  <!-- <li class="breadcrumb-item active"><a href="javascript:void(0);">      تقرير متابعة الأهـداف والأنشـطة
                                        </a></li> -->
                                <!-- <li class="breadcrumb-item"></li> -->
                            </ol>
                        </nav>
                   
                
                         
                             
                    <!-- <h3 style="text-align:center ;text-decoration:underline; font-weight:bold; color:#000">  تقرير متابعة الاهداف  </h3> -->


                   <div class="form-group col-md-12">
                    <input type="text" style="border:none; color:black;text-decoration:underline; margin-right:20%; font-size:30px; font-weight:bold ; " class="form-control" value="   تقــرير متابعة الأهـداف والأنشـطة {{request()->input('activity_status1')}} " readonly >
                    </div>

   <div class="form-group col-md-12">
<p style="font-size:24px;text-align:center;text-decoration:underline;"> لتنفيذ الهدف الاستراتيجي ({{$strategictarget->strategic_target_name}})
    <br />خلال <span
        style="font-size:24px; font-weight:bold ; color:red;text-decoration:underline;">({{ optional($strategicplan)->strategic_plan_name }})</span>
    لتحقيق ايراد قدره <span
        style="font-size:24px; font-weight:bold ; color:red;text-decoration:underline;">({{ request()->input('revenue') }})</span>
    مليون جنيه





                    <div class="form-group col-md-12">
                    <input type="text" style="border:none; color:black;text-decoration:underline; margin-right:30%; font-size:20; font-weight:bold ; " class="form-control" value=" خـــلال الـفتـرة مـن  {{request()->input('start_plan_date')}}  إلـى  {{request()->input('end_plan_date')}}" readonly >
                    </div>
                       </div>
                  
                    </div>
                   
                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                         <th>م</th> 
                                        <th>   الهدف التنفيذي </th>
                                        <th>  النشاط   </th>
                                        <th>    الموازنة التقديرية </th>
                                         <th>   مصادر التمويل  </th>
                                        
                                           <th>  مسئول التنفيذ   </th>
                                           <th>       مؤشر الاداء </th>
                                            
                                          



                                       

                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($follow as $item)
                                        <tr >
                                          
                                           <td>{{ $loop->iteration}}</td>
                                            <td >{{ $item->executive_target_name}}</td>
                                            <td>{{ $item->activity_name}}</td>
                                             <td>{{ $item->revenue_achieved}}</td>
                                              <td>{{ $item->funding_entity}}</td>
                                             
                                               <td>{{ $item->admin_group}}</td>
                                                <td>{{ $item->perfornance_index_name}}</td>
                                                
                                               

                                               
                                             
                                      
                                           
                                        </tr>
                                    @empty
                                        <tr class="text-center">
                                            <td colspan="15">{{ trans('home.no_data_found') }}</td>
                                        </tr>
                                    @endforelse

                                </tbody>

                            </table>


                            <div class="paginating-container">
                            {{ $follow->appends(request()->query())->links()}}
                        </div> 
                        
                            <div class="col-md-12 text-center">
                              <button  type="submit"  onClick="window.print()" value="Print This Page" class="btn btn-dark mt-3 hide" > طباعه التقرير</button>
                            </div>

                        
                            <div class="col-md-12 text-right mb-5">
                                
                            <a  style='margin-right:1%' href="javascript: history.go(-1)" class="btn btn-outline-success">عــــــــودة  </a> 

                           <!-- <a style="color:black !important; font-weight:bold;" href="{{ route('planWeeks.index') }} "class="btn btn-outline-success">عــــــــودة </a> -->

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection






@push('styles')
<!-- <style>
    @media print {
        .search_row, .hide{
            display:none !important;
        }

    }
</style> -->



<style>
@media print{
	#header {
		display: none !important; 
	}
	.html_header_left #main {
        margin-left: 80px !important; 
	}
}

</style>
@endpush

