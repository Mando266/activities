@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                </br>
                <div class="center" style="text-align:center;text-decoration:underline;padding-top:0%;">
                     <p  hidden style="font-size:24px;text-align:center;text-decoration:underline;">     {{ $get_strategic_target_id->strategic_target_id }}</p>

                     <p style="font-size:24px;text-align:center;text-decoration:underline;"> متابعة الأهداف والأنشطة المخططة </p>

                    <p style="font-size:24px;text-align:center;text-decoration:underline;"> لتنفيذ الهدف الاستراتيجي
                      ({{optional($strategic_target)->strategic_target_name}})
                        <br />خــــــلال <span
                        
                            style="font-size:24px; font-weight:bold ; color:red;text-decoration:underline;">({{ optional($strategicplan)->strategic_plan_name }})</span>
                        لتحقيق ايراد قدره <span
                            style="font-size:24px; font-weight:bold ; color:red;text-decoration:underline;">({{ request()->input('revenue') }})</span>
                        مليون جنيه
                        <br /> خــــلال الفترة من <span
                            style="font-size:24px; font-weight:bold ; color:red;text-decoration:underline;">({{ request()->input('start_plan_date') }})</span>
                        الى <span
                            style="font-size:20px; font-weight:bold ; color:red;text-decoration:underline;">({{ request()->input('end_plan_date') }})</span>
                    </p>

                    <div class="widget widget-one">
                        <div class="widget-heading">
                            <nav class="breadcrumb-two" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    </a></li>
                                    <li class="breadcrumb-item"></li>
                                </ol>
                            </nav>
                            <div class="widget-content widget-content-area">
                                </br>

                                @if (count($data) == 0)
                                    <div class="center">
                                        <p style="text-align:center;color:blue;font-weight:bold;">
                                            {{ trans('home.no_data_found') }}</p>
                                    </div>
                                @else
                                    <div class="table-responsive">
                                        <table class="table table-bordered " style="background-color: #CFCFC3;">

                                            <tr>
                                                <th rowspan="2" style="white-space: nowrap;padding-top:1%;">الهدف
                                                    التنفيذى </th>
                                                <th rowspan="2" style="white-space: nowrap;padding-top:1%;">
                                                    الأنــــــــــــشــــطـــة </th>
                                                <th rowspan="2" style="white-space: nowrap;padding-top:1%;">مسئول التنفيذ
                                                </th>
                                                @foreach ($month as $key => $value)
                                                    <th colspan="4"
                                                        value=""style="padding:20px; text-align:center;font-size:20px;font-weight:bold;white-space: nowrap;">
                                                        شهر
                                                        ({{$value->month_no }} / {{$value->year}})
                                                    </th>
                                                @endforeach
                                            </tr>
                                            @foreach ($column_headers as $column_header)
                                                <th style="font-size:14px;font-weight:bold;height: 13px;color:transparent;">
                                                    ({{$column_header}})
                                                </th>
                                            @endforeach
                                            </tr>
                                            </thead>
                                            <tbody>
                                                 @php
                                                $last_name = null; // Initialize a variable to keep track of the last name seen
                                                @endphp
                                                   {{-- @foreach ($executive_target_names as $executive_target_name)
                                                   @if ($executive_target_name->executive_target_id !== $last_name)
                                                <tr>
                                                    <td rowspan="{{ $data->where('executive_target_id', $executive_target_name->executive_target_id)->count() }}">{{$executive_target_name}}</td>
                                                    <td> </td>
                                                </tr>
                                                    @endforeach --}}
                                               @foreach ($row_headers as $row_header)
                                                    <tr>
                                                        <td style="white-space: nowrap;">
                                                            {{$data->where('activity_name', $row_header)->pluck('executive_target_name')->first()}}
                                                        </td>

                                                        <td style="width:90%;">{{ $row_header }}</td>
                                                      <td>{{$data->where('activity_name', $row_header)->pluck('admin_group')->first()}}
                                                        </td>
                                                        @foreach ($column_headers as $column_header)
                                                            <?php
                                                            $activity = $data
                                                                ->where('activity_name', $row_header)
                                                                ->where('plan_week_no', $column_header)
                                                                ->pluck('activity_status1')
                                                                ->first();
                                                            ?>
                                                            @if ($activity == 'اعمال جارية')
                                                                <td style="background: yellow;"> </td>
                                                            @elseif($activity == 'اعمال متأخرة')
                                                                <td style="background: red;"> </td>
                                                            @elseif($activity == 'اعمال انتهت')
                                                                <td style="background: green;"></td>
                                                            @elseif($activity == 'اخرى')
                                                                <td style="background: white;"></td>
                                                            @else
                                                                <td></td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>


                                        <div>


                        <a href="{{ route('follow.index',['strategic_target_id'=>$get_strategic_target_id->strategic_target_id , 'strategic_plan_name'=>$strategicplan->strategic_plan_name ,                   'strategic_plan_id'=>request()->input('strategic_plan_id'),                             'strategic_plan_name'=>request()->input('strategic_plan_name'),'revenue'=>request()->input('revenue'),'start_plan_date'=>request()->input('start_plan_date') ,'end_plan_date'=>request()->input('end_plan_date') ,'company_id'=>request()->input('company_id'),'activity_status1'=>' التى انتـــــهـت','activity_status'=>2]) }}" class="btn btn-sm" style="background: green;color:black;font-weight:bold;">     </a>
                          <!-- <button  class="btn btn-sm" style="background: green;color:black;font-weight:bold; "></button> -->
                         <span  style="color: black;font-size:16px;" style="background: green;color:black;font-weight:bold; "> التى انتهت</span>


                         &nbsp; &nbsp;

                         <a href="{{ route('follow.index',['strategic_target_id'=>$get_strategic_target_id->strategic_target_id ,'strategic_plan_id'=>request()->input('strategic_plan_id'),'start_plan_date'=>request()->input('start_plan_date') ,'end_plan_date'=>request()->input('end_plan_date') ,  'strategic_plan_name'=>request()->input('strategic_plan_name'),'revenue'=>request()->input('revenue'),'company_id'=>request()->input('company_id'),'activity_status1'=>' الجـــــارية','activity_status'=>1]) }}" class="btn btn-sm" style="background: yellow;color:black;font-weight:bold;">     </a>
                          <!-- <button  class="btn btn-sm" style="background: green;color:black;font-weight:bold; "></button> -->
                         <span  style="color: black;font-size:16px;" style="background: yellow;color:black;font-weight:bold;">  الجـــارية</span>




                         &nbsp; &nbsp;

<a href="{{ route('follow.index',['strategic_target_id'=>$get_strategic_target_id->strategic_target_id ,'strategic_plan_id'=>request()->input('strategic_plan_id'),'strategic_plan_name'=>request()->input('strategic_plan_name'),'revenue'=>request()->input('revenue'),'start_plan_date'=>request()->input('start_plan_date') ,'end_plan_date'=>request()->input('end_plan_date') ,'company_id'=>request()->input('company_id'),'activity_status1'=>' المتـاخـرة','activity_status'=>3]) }}" class="btn btn-sm" style="background: red;color:black;font-weight:bold;">     </a>
 <!-- <button  class="btn btn-sm" style="background: green;color:black;font-weight:bold; "></button> -->
<span  style="color: black;font-size:16px;" style="background: red;color:black;font-weight:bold;">  المتـأخرة</span>




                            <!-- <button class="btn btn-sm" style="background: yellow;color:black;font-weight:bold;"></button>
                            <span  style="color: black;font-size:14px;">  أعمال جارية</span>&nbsp; &nbsp;



                            <button class="btn btn-sm" style="background: red;color:black;font-weight:bold;"></button>
                            <span style="color: black; font-size:14px;"> أعمال متأخرة</span>&nbsp; &nbsp;
                        </div> -->

                            <div class="paginating-container">
                         {{-- {{ $data->links() }} --}}
                            {{-- <a style="color:black !important; font-weight:bold;" href="" class="btn btn-outline-success">عــــــــودة </a> --}}

                        </div>
                    </div>

                </div>
            </div>
        </div>
        @endif
    @endsection

                @push('styles')
                    <style>
                        table,
                        th,
                        td {
                            border: 1px solid;
                        }
                    </style>
                @endpush
