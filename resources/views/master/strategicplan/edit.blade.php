@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                        <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                {{-- <li class="breadcrumb-item"><a href="javascript:void(0);"> بيانات اساسية</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('strategicplan.index') }}"> تعديل </a></li>
                                <li class="breadcrumb-item active"><a href="javascript:void(0);"> تعديل  </a></li> --}}
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                    </div>
                    <div class="widget-content widget-content-area">
                        <form id="createForm" action="{{ route('strategicplan.update', ['strategicplan' => $strategicplan]) }}" method="POST">
                            @csrf
                            @method('put')
                            <div class="form-row">
                    
                      </div>
                       <div class="form-row">
                    
                       <input value="{{Auth::user()->company_id}}" name="company_id" hidden/>





                       <div class="form-group col-md-3">
                                    <label for="nameInput">   الهدف الاستراتيجي</label>
                                         <select class=" form-control" id="stationNamesInput" data-live-search="true"
                                        name="strategic_target_id" data-size="10" title="">
                                        <option value="">اختر</option>
                                        @foreach ($strategictarget as $item)
                                        <option value="{{ $item->id }}"
                                                {{ $item->id == old('strategic_target_id', $strategicplan->strategic_target_id) ? 'selected' : '' }}>{{ $item->strategic_target_name}}
                                        @endforeach
                                    </select>

                                    @error('strategic_target_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>

                       <div class="form-group col-md-3">
                                    <label for="nameInput">   </label>
                                        <input type="text" class="form-control"  name="strategic_plan_name"
                                        value="{{ old('strategic_plan_name',$strategicplan->strategic_plan_name) }}" placeholder="   " autocomplete="off">
                                        @error('strategic_plan_name')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-3">
                                    <label for="liquidInput">       الحالى/السابق     </label>
                                        <select class="form-control" name="plan_status">
                                       
                                        <option value="0" {{ old('plan_status',$strategicplan->plan_status) == "0" ? 'selected':'' }}>      الحالى  </option>
                                        <option value="1" {{ old('plan_status',$strategicplan->plan_status) == "1" ? 'selected':'' }}>    السابق   </option>
                                        </select>

                                        @error('plan_status')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>

                        
                            <div class="form-group col-md-3">
                                    <label for="nameInput">   تاريخ   البداية </label>
                                     <input type="date" class="form-control"  name="start_plan_date"
                                     value="{{ old('start_plan_date',$strategicplan->start_plan_date) }}"  placeholder="  " autocomplete="off">
                                        @error('start_plan_date')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                </div>    
                            


                              
                                <div class="form-group col-md-3">
                                    <label for="nameInput">   تاريخ   النهاية </label>
                                     <input type="date" class="form-control"  name="end_plan_date"
                                     value="{{ old('end_plan_date',$strategicplan->end_plan_date) }}"  placeholder="  " autocomplete="off">
                                        @error('end_plan_date')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                </div>    
                            


                                <!-- <div class="form-group col-md-3">
                                    <label for="nameInput"> عدد سنوات الخطة</label>
                                        <input type="text" class="form-control"name="plan_years_no"
                                        value="{{ old('plan_years_no',$strategicplan->plan_years_no) }}" placeholder="   " autocomplete="off">
                                        @error('plan_years_no')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div> -->

                         
                           
                                  
                                   
                                    <div class="form-group col-md-3">
                                    <label for="nameInput"> الايراد المطلوب تحقيقه  </label>
                                        <input type="text" class="form-control"name="revenue_achieved"
                                        value="{{ old('revenue_achieved',$strategicplan->revenue_achieved) }}" placeholder="   " autocomplete="off">
                                        @error('revenue_achieved')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>



                              
                              


                      </div>
                  
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary mt-3">تعديل</button>
                                    <a href="{{ route('strategicplan.index') }}" class="btn btn-danger mt-3">إلغاء / عودة </a>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
