@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                          <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">

                                  <li class="breadcrumb-item active"><a href="javascript:void(0);">       الخطة الاستراتيجية للشركة خلال فترة
                                        </a></li>
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                        @permission('Strategicplan-Create')
                            <div class="row">
                                <div class="col-md-12 text-right mb-5">
                                     {{-- @if(Auth::user()->company_id > 45)
                                        @else --}}
                                    <a style="color:black !important; font-weight:bold;" href="{{ route('strategicplan.create') }}" class="btn btn-outline-success">أضف    </a>
                                    <button style="color:black !important; font-weight:bold;" id="insert"  class="btn btn-outline-success">   تفعيل  الخطة </button>
                                      {{-- @endif --}}
                                </div>
                            </div>
                        @endpermission
                    </div>
                      {{-- <form action="{{ route('strategicplan.index') }}">
                    <div class="form-row">

                                  <div class="form-group col-md-3">
                                    <label for="nameInput">  اسم  البـــنك</label>
                                      <select class=" form-control"  data-live-search="true"
                                name="name" data-size="10" title="ÃÎÊÑ">
                                <option value="">اختر</option>
                                @foreach ($bankName as $item)
                                    <option value="{{ $item->name }}"
                                        {{ $item->name == old('name',request()->input('name')) ? 'selected' : '' }}>{{ $item->name }}
                                    </option>
                                @endforeach
                            </select>
                                    @error('main_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>


                    </div>
                        <div class="col-md-8 text-center">
                        <button type="submit" class="btn btn-primary mt-3"> ابحث</button>
                            <a href="{{route('strategicplan.index') }}" class="btn btn-danger mt-3">الغاء
                            </a>
                    </div>
                    </div>
                    </form> --}}




                   
                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr class='text-center'>
                                  
                                        <th>      </th>
                                        <th>     تاريخ البداية  </th>
                                        <th>     تاريخ النهاية  </th>

                                        <th class='text-center'>   حالى / سابق   </th>
                                        <!-- <th>       عدد سنوات الخطة </th> -->
                                        <th>    الايراد المطلوب تحقيقة  </th>

                                        <!-- <th class='text-center' style='width:26px;'>تعديل</th> -->

                                        <th class='text-center' style='width:26px;'>مسح</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($strategicplan as $item)
                                        <tr class='text-center'>

                                            <td>{{ $item->strategic_plan_name}}</td>
                                             <td>{{ $item->start_plan_date}}</td>

                                             <td>{{ $item->end_plan_date}}</td>

                                             <!-- <td>{{ $item->plan_years_no}}</td> -->
                                             <!-- <td>{{ $item->plan_status}}</td> -->
                                             <td   class='text-center'style='text-align:center;' class="text-center">
             @if($item->plan_status == 0)
                 <span> حالى </span>
             @else
                 <span> سابق </span>
             @endif
         </td>
                                             <td>{{ $item->revenue_achieved}}</td>



                                            <!-- <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('targetindex-Edit')
                                                     <li>
                                                            <a href="{{ route('strategicplan.edit', ['strategicplan' => $item->id]) }}"
                                                                data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="edit">
                                                                <i class="far fa-edit text-primary"></i>
                                                            </a>

                                                        </li>
                                                    @endpermission
                                            </td> -->
{{--
                                            @if(Auth::user()->company_id > 45)
                                            @else --}}
                                            <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('targetindex-Delete')
                                                        <li>
                                                            <form
                                                                action="{{ route('strategicplan.destroy', ['strategicplan' => $item->id]) }}"
                                                                method="post">
                                                                @method('DELETE')
                                                                @csrf
                                                                <button type="submit" class="fa fa-trash text-primary"></button>
                                                            </form>

                                                        </li>
                                                    @endpermission

                                                </ul>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr class="text-center">
                                            <td colspan="7">{{ trans('home.no_data_found') }}</td>
                                        </tr>
                                    @endforelse

                                </tbody>

                            </table>
                        </div>
                        <div class="paginating-container">
                            {{ $strategicplan->links() }}
                            {{-- <a style="color:black !important; font-weight:bold;" href="" class="btn btn-outline-success">عــــــــودة </a> --}}

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@push('scripts')

<script>

function post(url, data) {
  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    return response.json();
  });
}

document.getElementById('insert').addEventListener('click', function() {

  
  post(`/api/plan`).then(function(response) {
    alert(" تم الالحاق");
  }).catch(function(error) {
    alert("تم التفعيل!");
  });
});

</script>


   <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
@endpush