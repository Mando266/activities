@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                        <nav class="breadcrumb-two" aria-label="breadcrumb">
                             <ol class="breadcrumb">
                               {{-- <li class="breadcrumb-item"><a href="javascript:void(0);"> بيانات الحركة</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('executivetarget.index') }}">  </a></li>--}}
                                <li class="breadcrumb-item active"><a href="javascript:void(0);"> أضافة      </a></li>
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                    </div>


                    <BR>
                    <div class="widget-content widget-content-area">
                        <form id="createForm" action="{{ route('executivetarget.store') }}" method="POST">
                            @csrf
                            <div class="form-row">
                            <div class="form-group col-md-3">
                                    <label for="nameInput"> اسم  الخطة</label>
                                         <select class=" form-control" id="stationNamesInput" data-live-search="true"
                                        name="strategic_plan_id" data-size="10" title="">
                                        <option value="">اختر</option>
                                        @foreach ($strategicplan as $item)
                                        <option value="{{ $item->id }}"
                                                {{ $item->id == old('strategic_plan_id') ? 'selected' : '' }}>{{ $item->strategic_plan_name }}
                                        @endforeach
                                    </select>

                                    @error('strategic_plan_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>

                                    <div class="form-group col-md-3">
                                    <label for="nameInput">   الهدف الاستراتيجي</label>
                                         <select class=" form-control" id="stationNamesInput" data-live-search="true"
                                        name="strategic_target_id" data-size="10" title="">
                                        <option value="">اختر</option>
                                        @foreach ($targetexecutive as $item)
                                        <option value="{{ $item->id }}"
                                                {{ $item->id == old('strategic_target_id') ? 'selected' : '' }}>{{ $item->strategic_target_name}}
                                        @endforeach
                                    </select>

                                    @error('strategic_target_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>

                                   <div class="form-group col-md-3">
                                    <label for="nameInput">   الهدف</label>
                                     <input type="text" class="form-control" id="emailInput" name="executive_target_name"
                                        value="{{ old('executive_target_name') }}" placeholder="الهدف   " autocomplete="off">

                                    @error('executive_target_name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="nameInput">   تاريخ   البداية </label>
                                     <input type="date" class="form-control" id="emailInput" name="target_from"
                                        value="{{ old('target_from') }}" placeholder="  " autocomplete="off">

                                    @error('target_from')
                                    <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                     <div class="form-group col-md-3">
                                    <label for="nameInput">   تاريخ   النهاية </label>
                                     <input type="date" class="form-control" id="emailInput" name="target_to"
                                        value="{{ old('target_to') }}" placeholder="  " autocomplete="off">

                                    @error('target_to')
                                    <div style="color:white;font-size:16px;">
                                    {{$message}}
                                </div>
                                    @enderror
                                </div>



                                <div class="form-group col-md-3">
                                    <label for="nameInput">   الموازنة التقديرية</label>
                                     <input type="text" class="form-control" id="emailInput" name="target_estimated_budget"
                                        value="{{ old('target_estimated_budget') }}" placeholder="   " autocomplete="off">

                                    @error('target_estimated_budget')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="liquidInput">     موقف الخطة الحالى/السابق     </label>
                                        <select class="form-control" name="plan_status">
                                       
                                        <option value="0" {{ old('plan_status') == "0" ? 'selected':'' }}>     الحالى  </option>
                                        <option value="1" {{ old('plan_status') == "1" ? 'selected':'' }}>     السابق   </option>
                                        </select>

                                        @error('plan_status')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>

                      </div>

                         </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary mt-3">أضف</button>
                                        <a href="{{ route('executivetarget.index') }}" class="btn btn-danger mt-3">إلغاء / عودة
                                        </a>
                                    </div>
                                </div>




                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
