@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                          <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">

                                  <li class="breadcrumb-item active"><a href="javascript:void(0);">      الاهداف التنفيذية
                                        </a></li>
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                        @permission('Executivetarget-Create')
                            <div class="row">
                                <div class="col-md-12 text-right mb-5">
                                     {{-- @if(Auth::user()->company_id > 45)
                                        @else --}}
                                    <a style="color:black !important; font-weight:bold;" href="{{ route('executivetarget.create') }}" class="btn btn-outline-success">أضف    </a>
                                      {{-- @endif --}}
                                </div>
                            </div>
                        @endpermission
                    </div>
                      {{-- <form action="{{ route('executivetarget.index') }}">
                    <div class="form-row">

                                  <div class="form-group col-md-3">
                                    <label for="nameInput">  اسم  البـــنك</label>
                                      <select class=" form-control"  data-live-search="true"
                                name="name" data-size="10" title="ÃÎÊÑ">
                                <option value="">اختر</option>
                                @foreach ($bankName as $item)
                                    <option value="{{ $item->name }}"
                                        {{ $item->name == old('name',request()->input('name')) ? 'selected' : '' }}>{{ $item->name }}
                                    </option>
                                @endforeach
                            </select>
                                    @error('main_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>


                    </div>
                        <div class="col-md-8 text-center">
                        <button type="submit" class="btn btn-primary mt-3"> ابحث</button>
                            <a href="{{route('executivetarget.index') }}" class="btn btn-danger mt-3">الغاء
                            </a>
                    </div>
                    </div>
                    </form> --}}
                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <!-- <th>م</th> -->
                                        <th>   الهدف </th>
                                        <th>  تاريخ  البداية </th>
                                         <th>  تاريخ  النهاية </th>
                                          <th>  الموازنة التقديرية</th>
                                          <th>موقف الخطة الحالى / السابق</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        <th class='text-center' style='width:26px;'>تعديل</th>

                                        <th class='text-center' style='width:26px;'>مسح</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($executivetargetcompview as $item)
                                        <tr>

                                            <td style="white-space: nowrap;">{{ $item->executive_target_name}}</td>
                                             <td style="white-space: nowrap;">{{ $item->target_from}}</td>
                                              <td style="white-space: nowrap;">{{ $item->target_to}}</td>
                                              <td>{{ $item->target_estimated_budget}}</td>
                                              <td>{{ $item->plan_status}}</td>
                                              <td>
                                                    <a style="color:black !important; font-weight:bold;white-space: nowrap;font-size:14px;"
                                                        href="{{ route('activity.index', ['executive_target_id' => $item->id, 'executive_target_name' => $item->executive_target_name,'target_from' => $item->target_from,'target_to' => $item->target_to]) }}"
                                                        class="btn btn-outline-success">      الانشطة  </a>
                                                </td>

                                              <td>
                                                    <a style="color:black !important; font-weight:bold;white-space: nowrap;font-size:14px;"
                                                        href="{{ route('fundingsources.index', ['executive_target_id' => $item->id]) }}"
                                                        class="btn btn-outline-success">      مصادر التمويل </a>
                                                </td>


                                                <td>
                                                    <a style="color:black !important; font-weight:bold;white-space: nowrap;font-size:14px;"
                                                        href="{{ route('targetoutput.index', ['executive_target_id' => $item->id]) }}"
                                                        class="btn btn-outline-success">       مخرجات الهدف </a>
                                                </td>


                                                <td>
                                                    <a style="color:black !important; font-weight:bold;white-space: nowrap;font-size:14px;"
                                                        href="{{ route('targetindex.index', ['executive_target_id' => $item->id]) }}"
                                                        class="btn btn-outline-success">      مؤشر اداء الهدف  </a>
                                                </td>

                                            <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('Executivetarget-Edit')
                                                     <li>
                                                            <a href="{{ route('executivetarget.edit', ['executivetarget' => $item->id]) }}"
                                                                data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="edit">
                                                                <i class="far fa-edit text-primary"></i>
                                                            </a>

                                                        </li>
                                                    @endpermission
                                            </td>

                                            <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('Executivetarget-Delete')
                                                        <li>
                                                            <form
                                                                action="{{ route('executivetarget.destroy', ['executivetarget' => $item->id]) }}"
                                                                method="post">
                                                                @method('DELETE')
                                                                @csrf
                                                                <button type="submit" class="fa fa-trash text-primary"></button>
                                                            </form>

                                                        </li>
                                                    @endpermission

                                                </ul>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr class="text-center">
                                            <td colspan="7">{{ trans('home.no_data_found') }}</td>
                                        </tr>
                                    @endforelse

                                </tbody>

                            </table>
                        </div>
                        <div class="paginating-container">
                            {{ $executivetargetcompview->links() }}
                            {{-- <a style="color:black !important; font-weight:bold;" href="" class="btn btn-outline-success">عــــــــودة </a> --}}

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
