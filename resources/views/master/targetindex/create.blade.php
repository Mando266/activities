@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                        <nav class="breadcrumb-two" aria-label="breadcrumb">
                             <ol class="breadcrumb">
                               {{-- <li class="breadcrumb-item"><a href="javascript:void(0);"> بيانات اساسية</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('targetindex.index') }}">  </a></li>--}}
                                <li class="breadcrumb-item active"><a href="javascript:void(0);"> أضافة      </a></li>
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                    </div>


                    <BR>
                    <div class="widget-content widget-content-area">
                        <form id="createForm" action="{{ route('targetindex.store') }}" method="POST">
                            @csrf
                            <div class="form-row">


                            <input type="hidden" value="{{request()->input('executive_target_id')}}" name="executive_target_id" >

            
                            <div class="form-group col-md-4">
                                    <label for="">     الهدف التنفيذي</label>
                                         <select class=" form-control" id="" data-live-search="true"
                                        name="perfornance_index_id" data-size="10" title="">
                                        <option value="">اختر</option>
                                        @foreach ($perfornanceindex as $item)
                                        <option value="{{ $item->id }}"
                                                {{ $item->id == old('perfornance_index_id') ? 'selected' : '' }}>{{ $item->perfornance_index_name }}
                                        @endforeach
                                    </select>

                                    @error('perfornance_index_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                  
                                   
                            


                              
                              
                  
                         </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary mt-3">أضف</button>
                                        <a href="{{ route('targetindex.index',['executive_target_id'=>request()->input('executive_target_id')])}}" class="btn btn-danger mt-3">إلغاء / عودة</a>
                                    </div>
                                </div>




                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
