@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                          <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">

                                  <li class="breadcrumb-item active"><a href="javascript:void(0);">       الهدف الاستراتيجي
                                        </a></li>
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                        @permission('strategictarget-Create')
                            <div class="row">
                                <div class="col-md-12 text-right mb-5">

                                    <a style="color:black !important; font-weight:bold;" href="{{ route('strategictarget.create') }}" class="btn btn-outline-success">أضف    </a>

                                </div>
                            </div>
                        @endpermission
                    </div>

                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <!-- <th>م</th> -->
                                        <th>  اسم  الهدف  الاستراتيجي</th>
                                        <th>    مخطط زمنى للهدف من  </th>
                                         <th>   مخطط زمنى للهدف الى  </th>
                                          <th>  الموازنة التقديرية</th>
                                          <th>   الخطة الاستراتيجية</th>


                                        <th class='text-center' style='width:26px;'>تعديل</th>

                                        <th class='text-center' style='width:26px;'>مسح</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($items as $item)
                                        <tr>

                                            <td>{{ $item->strategic_target_name}}</td>
                                             <td>{{ $item->target_from}}</td>
                                              <td>{{ $item->target_to}}</td>
                                              <td>{{ $item->target_estimated_budget}}</td>

                                              <td>{{ $item->strategic_plan_id}}</td>




                                            <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('Strategictarget-Edit')
                                                     <li>
                                                            <a href="{{ route('strategictarget.edit', ['strategictarget' => $item->id]) }}"
                                                                data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="edit">
                                                                <i class="far fa-edit text-primary"></i>
                                                            </a>

                                                        </li>
                                                    @endpermission
                                            </td>


                                            <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('Strategictarget-Delete')
                                                        <li>
                                                            <form
                                                                action="{{ route('strategictarget.destroy', ['strategictarget' => $item->id]) }}"
                                                                method="post">
                                                                @method('DELETE')
                                                                @csrf
                                                                <button type="submit" class="fa fa-trash text-primary"></button>
                                                            </form>

                                                        </li>
                                                    @endpermission

                                                </ul>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr class="text-center">
                                            <td colspan="7">{{ trans('home.no_data_found') }}</td>
                                        </tr>
                                    @endforelse

                                </tbody>

                            </table>
                        </div>
                        <div class="paginating-container">
                            {{ $items->links() }}
                            {{-- <a style="color:black !important; font-weight:bold;" href="" class="btn btn-outline-success">عــــــــودة </a> --}}

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
