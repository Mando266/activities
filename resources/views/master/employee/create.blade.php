@extends('layouts.app')
@section('content')
<div class="layout-px-spacing">
    <div class="row layout-top-spacing">

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-one">
                <div class="widget-heading">
                    <nav class="breadcrumb-two" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);"> الاكواد</a></li>
                            <li class="breadcrumb-item"><a href="{{route('employee.index')}}">الموظفين</a></li>
                            <li class="breadcrumb-item active"><a href="javascript:void(0);"> أضف موظف جديد</a></li>
                            <li class="breadcrumb-item"></li>
                        </ol>
                    </nav>
                </div>

                <div class="widget-content widget-content-area">
                <form id="createForm" action="{{route('employee.store')}}" method="POST">
                        @csrf
                        <div class="form-row">

                        <div class="form-group col-md-6">
                                <label for="nameInput">كود  الموظف</label>
                            <input type="text" class="form-control" id="nameInput" name="code" value="{{old('code')}}"
                                 placeholder=" " autocomplete="disabled" autofocus>
                                @error('code')
                                <div style="color: red">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="nameInput">الاسم </label>
                            <input type="text" class="form-control" id="nameInput" name="name" value="{{old('name')}}"
                                 placeholder=" " autocomplete="disabled" autofocus required>
                                @error('name')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>


                            <div class="form-group col-md-6">
                                <label for="nameInput">الوظيفة </label>
                            <input type="text" class="form-control" id="nameInput" name="job" value="{{old('job')}}"
                                 placeholder=" " autocomplete="disabled" autofocus>
                                @error('job')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="employeeInput">الوحدة الادارية</label>
                                <select class="selectpicker form-control" id="employeeInput" data-live-search="true" name="admin_unit_id" data-size="10"
                                 title="أختر" required>
                                    @foreach ( $adminunit as $item)
                                        <option value="{{$item->id}}"{{$item->id == old('admin_unit_id')? 'selected' : ''}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                                @error('admin_unit_id')
                                <div class="invalid-feedback">
                                    {{$messag5e}}
                                </div>
                                @enderror
                            </div>

                         
                            <div class="form-group col-md-6">
                                <label for="nameInput">العنوان </label>
                            <input type="text" class="form-control" id="nameInput" name="address" value="{{old('address')}}"
                                 placeholder=" " autocomplete="disabled" autofocus>
                                @error('address')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="nameInput">الايميل </label>
                            <input type="text" class="form-control" id="nameInput" name="email" value="{{old('email')}}"
                                 placeholder=" " autocomplete="disabled" autofocus>
                                @error('email')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="nameInput">الرقم القومي </label>
                            <input type="text" class="form-control" id="nameInput" name="nat_id" value="{{old('nat_id')}}"
                                 placeholder=" " autocomplete="disabled" autofocus>
                                @error('nat_id')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="breakInput">    بالخدمة أم /الا  </label>
                                <select class="form-control" name="is_active" id="breakInput">
                                <option value="0" {{ old('is_active') == "0" ? 'selected':'' }}>بالخدمة</option>
                                <option value="1" {{ old('is_active') == "1" ? 'selected':'' }}>لا</option>
                                </select>
                                @error('break')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>



                            <div class="form-group col-md-6">
                                <label for="nameInput"> </label>
                            <input type="hidden" class="form-control" id="nameInput" name="id" value="{{old('id')}}"
                                 placeholder=" " autocomplete="disabled" autofocus>
                                @error('id')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                            </div>
                       <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary mt-3">أضافة</button>
                                <a href="{{route('employee.index')}}" class="btn btn-danger mt-3">إلغاء / عودة</a>
                            </div>
                       </div>
                    </form>
                    <div class="row">
                        <div class="col-md-12 text-left">
                    @if(session('alert'))
                        <div class="alert alert-danger col-md-3" style="font-size: 19px;"> {{ session('alert') }}</div>
                    @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
