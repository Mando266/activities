@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                        <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);"> الاكواد</a></li>
                                <li class="breadcrumb-item active"><a href="javascript:void(0);"> الموظفين </a></li>
                                <li class="breadcrumb-item"> </li>
                            </ol>
                        </nav>
                        @permission('Employee-Create')
                        <div class="row">
                            <div class="col-md-12 text-right mb-5">
                            <a href="{{route('employee.create')}}" class="btn btn-primary">أضف موظف جديد</a>
                            </div>
                        </div>
                        @endpermission
                    </div>
                <!-- <form>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="nameInput" class=" "> العامل</label>
                            <select class="selectpicker form-control" id="nameInput" data-live-search="true" name="name" data-size="10" title="أختر">
                        @foreach ($employe as $item)
                            <option value="{{$item->name}}" {{$item->id == old('name',request()->input('name'))? 'selected' : ''}}> {{$item->code}} {{$item->name}}</option>

                        @endforeach
                            </select>
                        </div>

                    
                    <div class="col-md-12 text-center">
                        <button  type="submit" class="btn btn-success mt-3">بحث</button>
                        <a href="{{route('employee.index')}}" type="submit" class="btn btn-danger mt-3 hide">إلغاء البحث</a>
                </form> -->
                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-condensed mb-4">
                                <thead>
                                    <tr>
                                 <th>كود الموظف  </th> 
                                 <th  style='width:200px;'>أسم الموظف </th>
                                <th>الوحدة الادارية  </th>

                                <!-- <th> الرقم التعريفي بالنظام</th> -->
                             
                                <th  style='width:200px;'>الوظيفة  </th>

                                <th style='width:120px;'>العنوان  </th>

                                <th>الرقم القومي  </th>
                               
                                <th>بالخدمة /أم/لا   </th>
                            
                                        <th class='text-center' style='width:26px;'>تعديل</th>
                                        <th class='text-center' style='width:26px;'>مسح</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @forelse ($employe as $item)
                                        <tr>

                                          <td>{{$item->code}}</td>
                                          <td>{{$item->name}}</td>
                                          <td>{{$item->admin_unit_name}}</td>
                                            <!-- <td>{{$item->id}}</td> -->
                                          
                                            <td>{{$item->job}}</td>

                                            <td>{{$item->address}}</td>
                                            <td>{{$item->nat_id}}</td>
                                          
                                            <td class="text-center">
                                        @if($item->is_active )
                                            <span> لا </span>
                                        @else
                                            <span> بالخدمة </span>
                                        @endif
                                    </td>

                                         
                                            <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('Employee-Edit')
                                                    <li>
                                                        <a href="{{route('employee.edit',['employee'=>$item->id,'code'=>request()->input('code')])}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="edit">

                                                            <i class="far fa-edit text-success"></i>
                                                        </a>
                                                    </li>
                                                    @endpermission
                                                    </td>
                                                <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('Employee-Delete')
                                                    <li>
                                                    <form action="{{route('employee.destroy',['employee'=>$item->id])}}" method="post">
                                                     @method('DELETE')
                                                     @csrf
                                                     <button style="border: none; background: none;" type="submit" class="fa fa-trash text-danger" onclick="alert('لا يمكن الالغاء' );return false;"></button>
                                                    </form>
                                                    </li>
                                                    @endpermission
                                                </ul>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr class="text-center">
                                            <td colspan="18">{{ trans('home.no_data_found')}}</td>
                                        </tr>
                                    @endforelse

                                </tbody>

                            </table>

                        </div>
                      
                    </div>
                </div>

    </div>
@endsection
