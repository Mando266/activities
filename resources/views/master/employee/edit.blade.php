@extends('layouts.app')
@section('content')
<div class="layout-px-spacing">
    <div class="row layout-top-spacing">

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-one">
                <div class="widget-heading">
                    <nav class="breadcrumb-two" aria-label="breadcrumb">
                        <ol class="breadcrumb">

                        <li class="breadcrumb-item"><a href="javascript:void(0);"> الاكواد</a></li>
                        <li class="breadcrumb-item"><a href="{{route('employee.index')}}">الموظفين</a></li>
                            <li class="breadcrumb-item active"><a href="javascript:void(0);"> تعديل بيانات الموظف </a></li>
                            <li class="breadcrumb-item"></li>
                        </ol>
                    </nav>
                </div>



</br>
                <div class="widget-content widget-content-area">
                <form id="createForm" action="{{route('employee.update',['employee'=>$employee])}}" method="POST">
                        @csrf
                        @method('put')
                        <div class="form-row">
                        <div class="form-group col-md-6">
                                <label for="nameInput">كود الموظف </label>
                                <input type="text" class="form-control" name="code" value="{{old('code', $employee->code)}}"
                                    placeholder="كود " autocomplete="off" readonnly>
                            </div>


                            <div class="form-group col-md-6">
                                <label for="nameInput">الاسم </label>
                            <input type="text" class="form-control" id="nameInput" name="name"  value="{{old('name',$employee->name)}}"
                                 placeholder=" " autocomplete="disabled" autofocus>
                                @error('name')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="nameInput">الوظيفة </label>
                            <input type="text" class="form-control" id="nameInput" name="job" value="{{old('job',$employee->job)}}"
                                 placeholder=" " autocomplete="disabled" autofocus>
                                @error('job')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>



                            <div class="form-group col-md-6">
                                <label for="employeeInput">الوحدة الادارية</label>
                                <select class="selectpicker form-control" id="employeeInput" data-live-search="true" name="admin_unit_id" data-size="10"
                                 title="أختر" required>
                                    @foreach ( $adminunit as $item)
                                        <option value="{{$item->id}}" {{$item->id == old('admin_unit_id',$employee->admin_unit_id)? 'selected' : ''}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                                @error('admin_unit_id')
                                <div class="invalid-feedback">
                                    {{$messag5e}}
                                </div>
                                @enderror
                            </div>

                           

                            <div class="form-group col-md-6">
                                <label for="nameInput">العنوان </label>
                            <input type="text" class="form-control" id="nameInput" name="address" value="{{old('address',$employee->address)}}"
                                 placeholder=" " autocomplete="disabled" autofocus>
                                @error('address')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                          
                            <div class="form-group col-md-6">
                                <label for="nameInput">الرقم القومي </label>
                            <input type="text" class="form-control" id="nameInput" name="nat_id" value="{{old('nat_id',$employee->nat_id)}}"
                                 placeholder=" " autocomplete="disabled" autofocus>
                                @error('nat_id')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="breakInput">    بالخدمة /لا  </label>
                                <select class="form-control" name="is_active" id="breakInput">
                             <option value="0"  {{ old('is_active',$employee->is_active) == "0" ? 'selected':'' }}>بالخدمة</option>
                                <option value="1" {{ old('is_active',$employee->is_active) == "1" ? 'selected':'' }}> لا</option>
                                </select>
                                @error('is_active')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            </div>

                       <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary mt-3">تعديل</button>
                                <a href="{{route('employee.index')}}" class="btn btn-danger mt-3">إلغاء</a>
                            </div>
                       </div>


                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
