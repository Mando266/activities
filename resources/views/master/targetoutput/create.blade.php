@extends('layouts.app')
@section('content')
<div class="layout-px-spacing">
    <div class="row layout-top-spacing">

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-one">
                <div class="widget-heading">
                    <nav class="breadcrumb-two" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">   </a></li>
                            <li class="breadcrumb-item"><a href="{{route('targetoutput.index')}}">  </a></li>
                            <li class="breadcrumb-item active"><a href="javascript:void(0);"> أضف   </a></li>
                            <li class="breadcrumb-item"></li>
                        </ol>
                    </nav>
                </div>

<br>




                <div class="widget-content widget-content-area">
                <form id="createForm" action="{{route('targetoutput.store')}}" method="POST">
               @csrf
                <table id="paymentTable" class="table table-bordered">

                <!-- <input type="hidden" value="{{Auth::user()->company_id}}" name="company_id"/> -->
                <input type="hidden" value="{{request()->input('executive_target_id')}}" name="executive_target_id" >

                    <thead>
                    <tr>
                        <th>     كود الناتج </th>
                        <th>اسم الناتج </th>
                    <th>
                            <a id="aadd"><i class="fas fa-plus"></i></a>
                        </th>
                     
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <tr>
                            

                        <td>
                                <input type="text" class="form-control" name="targetoutput[0][executive_target_id]" value="{{request()->input('executive_target_id')}}"
                                 placeholder="  " autocomplete="off" >
                           </td>


                           

                            <td>
                            <input type="text" class="form-control"  name="targetoutput[0][target_output_name]" value="{{request()->input('target_output_name')}}"
                                 placeholder="  " autocomplete="off" >
                            </td>
                            <td>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary mt-3">أضافة</button>
                    <a href="{{route('targetoutput.index',['executive_target_id'=>request()->input('executive_target_id')])}}" class="btn btn-danger mt-3">إلغاء / عودة</a>
                </div>
            </div>
        </form>
    </div>
</div>

        </div>
    </div>
</div>
@endsection
@push('scripts')

<script>
      $(document).ready(function(){
        $("#paymentTable").on("click", ".remove", function () {
        $(this).closest("tr").remove();
        });


        var counter  = 1;
      $("#aadd").click(function(){
        var tr = '<tr>'+
                '<td><input type="text" class="form-control" name="targetoutput['+counter+'][executive_target_id]" value="{{request()->input('executive_target_id')}}"></td>'+
                '<td><input type="text" class="form-control" name="targetoutput['+counter+'][target_output_name]" value="{{request()->input('target_output_name')}}"></td>'+
            '<td style="width:85px;"><button type="button" class="btn btn-danger remove"><i class="fa fa-trash"></i></button></td>'
            '</tr>';
            counter++;
            $('#paymentTable').append(tr);



            
        });
    });

</script>
 <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
@endpush
