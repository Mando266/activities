@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                        <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">البيانات الاساسية</a></li>
                                <li class="breadcrumb-item active"><a href="javascript:void(0);">الوحدات الأدارية</a></li>
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                        @permission('AdminUnit-Create')
                        <div class="row">
                            <div class="col-md-12 text-right mb-5">
                            <a href="{{route('adminunit.create')}}" class="btn btn-primary">أضف وحدة جديدة</a>
                            </div>
                        </div>
                        @endpermission
                    </div>

                    <form>
                            <!-- <div class="form-row"> -->
                                <!-- <div class="form-group col-md-4">
                                    <label for="nameInput" class=" "> كود الوحدة الادارية</label>
                                    <input  type="text" name="code" class="form-control"  value="{{request()->input('code')}}">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="nameInput">  الوحده الاداريه </label>
                                    <select class="selectpicker form-control" name="name" id=""  data-live-search="true" title="أختر">
                                    <option value="">إختار الوحده الاداريه</option>

                                        @foreach ($adminUnit as $item)
                                            <option value="{{$item->name}}" {{$item->name == old('name',request()->input('name')) ? 'selected' : ''}}> {{$item->name}} </option>
                             @endforeach
                        </select>
                                </div> -->
                                <!-- <div class="col-md-12 text-center">
                                    <button  type="submit" class="btn btn-success mt-3" id="search">بحث</button>
                                    <a href="{{route('adminunit.index')}}" type="submit" class="btn btn-danger mt-3 hide">إلغاء البحث</a>
                                </div> -->
                    </form>
                    
                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-condensed mb-7">
                           
                                <thead>
                                    <tr>
                                        <th> الكود  </th>
                                        <th > الوحدة الادارية  </th>
                                        <th>نوع الوحدة الادارية  </th>
                                        <th>العنوان </th>
                                        <th>التليفون  </th>
                                        <th>احداثيات الطول  </th>
                                        <th>احداثيات العرض  </th>
                                       

                                        <th class='text-center' style='width:26px;'>تعديل </th>
                                        <th class='text-center' style='width:26px;'> مسح  </th>

                                    </tr>

                                </thead>

                                <tbody>
                                    @forelse ($adminUnit as $item)
                                        <tr>


                                            <td>{{$item->code}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->admin_unit_typ_id}}</td>
                                            <td>{{$item->address}}</td>
                                            <td>{{$item->tel}}</td>
                                            <td>{{$item->lat}}</td>
                                            <td>{{$item->lang}}</td>
                                           
                                            <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('AdminUnit-Edit')
                                                    <li>
                                                        <a href="{{route('adminunit.edit',['adminunit'=>$item->id])}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="edit">
                                                            <i class="far fa-edit text-primary"></i>
                                                        </a>
                                                    </li>
                                                    @endpermission
                                            </td>



                                            <td class="text-center">
                                                <ul class="table-controls">

                                                    @permission('AdminUnit-Delete')
                                                    <li>
                                                    <form action="{{route('adminunit.destroy',['adminunit'=>$item->id])}}" method="post">
                                                     @method('DELETE')
                                                     @csrf
                                                     <button type="submit" class="fa fa-trash text-primary"></button>
                                                    </form>
                                                    </li>
                                                    @endpermission
                                                </ul>
                                            </td>


                                        </tr>
                                    @empty
                                        <tr class="text-center">
                                            <td colspan="15">{{ trans('home.no_data_found')}}</td>
                                        </tr>
                                    @endforelse

                                </tbody>

                            </table>
                        </div>
                        <div class="paginating-container">
                            {{ $adminUnit->links() }}
                        </div>
                    </div>
                   
                </div>

            </div>
        </div>
    </div>
@endsection
