@extends('layouts.app')
@section('content')
<div class="layout-px-spacing">
    <div class="row layout-top-spacing">

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-one">
                <div class="widget-heading">
                    <nav class="breadcrumb-two" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">  الأكواد </a></li>
                            <li class="breadcrumb-item"><a href="{{route('adminUnitCat.index')}}">المنطقة</a></li>
                            <li class="breadcrumb-item active"><a href="javascript:void(0);"> أسم الوحدة الإدارية</a></li>
                            <li class="breadcrumb-item"></li>
                        </ol>
                    </nav>
                </div>
                <div class="widget-content widget-content-area">
                <form id="createForm" action="{{route('adminUnitCat.edit',['adminUnitCat'=>$adminUnitCat])}}" method="get">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="nameInput">أسم الوحدة</label>
                                <input type="text" class="form-control" id="nameInput"  value="{{$adminUnitCat->name}}" disabled>
                            </div>
                        </div>
                        
                       <div class="row">
                            <div class="col-md-12 text-center">
                                @permission('AdminUnitCat-Edit')
                                <button type="submit" class="btn btn-primary mt-3">تعديل</button>
                                @endpermission
                                <a href="{{route('adminUnitCat.index')}}" class="btn btn-danger mt-3">إلغاء</a>
                            </div>
                       </div>


                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@push('styles')
    <link href="{{asset('plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" >
@endpush
@push('scripts')
<script src="{{asset('plugins/bootstrap-select/bootstrap-select.min.js')}}"></script>
@endpush
