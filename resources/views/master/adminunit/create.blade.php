@extends('layouts.app')
@section('content')
<div class="layout-px-spacing">
    <div class="row layout-top-spacing">

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-one">
                <div class="widget-heading">
                    <nav class="breadcrumb-two" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">  البيانات الاساسية </a></li>
                            <li class="breadcrumb-item"><a href="{{route('adminunit.index')}}">الوحدات الادارية </a></li>
                            <!-- <li class="breadcrumb-item active"><a href="javascript:void(0);"> أضف  وحدة جديدة </a></li> -->
                            <li class="breadcrumb-item"></li>
                        </ol>
                    </nav>
                </div>
                <div class="widget-content widget-content-area">
                <form id="createForm" action="{{route('adminunit.store')}}" method="POST">
                        @csrf
                    <input type="hidden" class="form-control" id="qtyInput" name="company_id" value="{{request()->input('company_id')}}">
                    {{-- <div class="form-row">

                    </div> --}}
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="codeInput"> الكود</label>
                            <input type="text" class="form-control" id="" name="code" value="{{old('code')}}"
                                 placeholder="" autocomplete="disabled" autofocus required>
                                @error('code')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>



                            <div class="form-group col-md-4">
                                    <label for="nameInput"> الوحدة  الادارية</label>
                                        <input type="text" class="form-control" id="name" name="name"
                                        value="{{ old('name') }}" placeholder="   " autocomplete="off">
                                        @error('name')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>

                           


                            <div class="form-group col-md-4">
                                <label for="stationNameInput"> نوع الوحدةالادارية </label>
                                <select class="selectpicker form-control" id="stationNamesInput" data-live-search="true" name="admin_unit_typ_id" data-size="10" title="أختر"required>
                              @foreach ($adminunitTyp as $item)
                             <option value="{{$item->id}}" {{$item->id == old('admin_unit_typ_id')? 'selected' : ''}}>{{$item->admin_unit_typ_name}}</option>
                             @endforeach
                             </select>
                                @error('admin_unit_typ_id')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>


                            <div class="form-group col-md-3">
                                <label for="stationNameInput"> التليفون </label>
                                <input type="text" class="form-control" id="addressInput" name="tel" value="{{old('tel')}}"
                                 placeholder="   " autocomplete="disabled" autofocus>
                                @error('tel')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>


                            <div class="form-group col-md-4">
                                <label for="addressInput"> العنوان  </label>
                            <input type="text" class="form-control" id="addressInput" name="address" value="{{old('address')}}"
                                 placeholder="" autocomplete="disabled" autofocus>
                                @error('address')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                      <div class="form-group col-md-4">
                                <label for="latInput"> احداثيات الطول  </label>
                            <input type="text" class="form-control" id="" name="lat" value="{{old('lat')}}"
                                 placeholder="" autocomplete="disabled" autofocus>
                                @error('lat')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                     <div class="form-group col-md-4">
                                <label for="langInput"> احداثيات العرض  </label>
                            <input type="text" class="form-control" id="" name="lang" value="{{old('lang')}}"
                                 placeholder="" autocomplete="disabled" autofocus>
                                @error('lang')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                        </div>
                       </div>
                       <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary mt-3">أضافة</button>
                                <a href="{{route('adminunit.index')}}" class="btn btn-danger mt-3">إلغاء / عودة</a>
                            </div>
                       </div>
                    </form>
            </div>

        </div>
    </div>
</div>
@endsection
