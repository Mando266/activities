@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                          <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                  <li class="breadcrumb-item active"><a href="javascript:void(0);">  حالة النشاط فى اسبوع من الخطة
                                        </a></li>
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>

                        @permission('ActivityFollowUp-Create')
                            <div class="row">
                                <div class="col-md-12 text-right mb-5">
                                   
                                  
                                    <button style="color:black !important; font-weight:bold;" id="update"  class="btn btn-outline-success">   تفعيل حالة النشاط </button>
                                  
                                </div>

                            </div>
                        @endpermission
                    </div>
      
      
                    <div class="form-row">
                      <div class="form-group col-md-4">
                      <label for="nameInput">         اسم النشاط   :  </label>
                 
                     
                      <input type="text"class=" form-control" value="{{$activity_name->activity_name}}">
                      </div>
                      <div class="form-group col-md-3">
                      <label for="nameInput">            التاريخ من   :  </label>
                      <input type="text"class=" form-control" id="planned_from" value="{{$planned_from->planned_from}}">
                      </div>
                      <div class="form-group col-md-3">
                      <label for="nameInput">        التاريخ الى    :  </label>
                      <input type="text" class=" form-control" id="planned_to" value="{{$planned_to->planned_to}} ">
                  </div>
                   
                        </div>


                    <div class="widget-content widget-content-area">
                        <div class="table-responsive">
                              <table class="table table-success table-striped">
                                <thead>
                                    <tr>

                                       <th> رقم الاسبوع فى الخطة</th>

                                          <th>تاريخ بدا الاسبوع  </th>
                                        <th>تاريخ نهاية الاسبوع </th>

                                         <th>  حالة النشاط</th>
                                        <th>   النسبة المؤية المحققة </th>






                                        <th class='text-center' style='width:26px;'>تعديل</th>
                                        {{-- <th class='text-center' style='width:26px;'>أظهار</th> --}}
                                        <th class='text-center' style='width:26px;'>مسح</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($activityfollowupview as $item)
                                        <tr >


                                         <td>{{ $item->plan_week_no}}</td>

                                         <td>{{ $item->start_date_week}}</td>
                                        <td>{{ $item->end_date_week}}</td>
                                                  <td class="text-left">
                                                        @if($item->activity_status =='1')
                                                            <span    style='width:90px; 'class="badge badge-info"> اعمال جارية </span>
                                                            @elseif($item->activity_status =='2')
                                                            <span   style='width:90px; 'class="badge badge-info"> اعمال انتهت  </span>
                                                            @elseif($item->activity_status =='3')
                                                            <span   style='width:90px; 'class="badge badge-danger"> اعمال متأخرة </span>

                                                           @endif
                                                    </td>

                                             <td>{{ $item->achieved_percentage}}</td>




                                            {{--  <td>{{ $item->unit}}</td> --}}



                                            {{-- @if(Auth::user()->company_id > 45)
                                            @else --}}
                                            <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('ActivityFollowUp-Edit')
                                                     <li>
                                                            <a href="{{ route('activityfollowup.edit', ['activityfollowup' => $item->id,'activity_id'=>request()->input('activity_id'),'activity_name'=>request()->input('activity_name'),'planned_from'=>request()->input('planned_from'),'planned_to'=>request()->input('planned_to')]) }}"
                                                                data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="edit">
                                                                <i class="far fa-edit text-primary"></i>
                                                            </a>
                                                            {{-- @endif --}}
                                                        </li>
                                                    @endpermission
                                            </td>
{{--
                                            @if(Auth::user()->company_id > 45)
                                            @else --}}
                                            <td class="text-center">
                                                <ul class="table-controls">
                                                    @permission('ActivityFollowUp-Delete')
                                                        <li>
                                                            <form
                                                                action="{{ route('activityfollowup.destroy', ['activityfollowup' => $item->id]) }}"
                                                                method="post">
                                                                @method('DELETE')
                                                                @csrf
                                                                <button type="submit" class="fa fa-trash text-primary"></button>
                                                            </form>
                                           {{-- @endif --}}
                                                        </li>
                                                    @endpermission

                                                </ul>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr class="text-center">
                                            <td colspan="7">{{ trans('home.no_data_found') }}</td>
                                        </tr>
                                    @endforelse

                                </tbody>

                            </table>
                        </div>
                        <div class="paginating-container">
                           {{ $activityfollowupview->appends($_GET)->links() }}
                       
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@push('scripts')

<script>
function put(url, data) {
  return fetch(url, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    return response.json();
  });
}



document.getElementById('update').addEventListener('click', function() {
  let activity_id = {{ request()->input('activity_id') }};
  let planned_from = document.getElementById('planned_from').value;
  let planned_to = document.getElementById('planned_to').value;
  
  put(`/api/updatefollow/${activity_id}/${planned_from}/${planned_to}`).then(function(response) {
    alert("تم تعديل علي موقف النشاط انتهي");
  }).catch(function(error) {
    alert("تم التفعيل!");
  });
});

</script>


   <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
@endpush