@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                        <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                {{-- <li class="breadcrumb-item"><a href="javascript:void(0);"> الاكواد</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('activity.index') }}"> تعديل نشاط</a></li>
                                <li class="breadcrumb-item active"><a href="javascript:void(0);"> تعديل اسم النشاط</a></li> --}}
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                    </div>
                    <div class="widget-content widget-content-area">
                        <form id="createForm" action="{{ route('activityfollowup.update', ['activityfollowup' => $activityfollowup]) }}" method="POST">
                            @csrf
                            @method('put')
                             <input type="text" value="{{request()->input('activity_id')}}" name="activity_id" hidden>
                            <div class="form-row">
                               <div class="form-group col-md-2">
                                        <label for="nameInput">   رقم الاسبوع </label>
                                         <select class=" form-control" id="stationNamesInput" data-live-search="true"
                                        name="week_no" data-size="10" title="" disabled>
                                     
                                        @foreach ($planimplweeks as $item)
                                            <option value="{{ $item->id }}"
                                                {{ $item->id == old('week_no', $activityfollowup->week_no) ? 'selected' : '' }}>{{ $item->plan_week_no }}  
                                            </option>
                                        @endforeach
                                    </select>

                                    @error('week_no')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>




                                <div class="form-group col-md-2">
                                        <label for="nameInput">     رقم الشهر  </label>
                                         <select class=" form-control" id="stationNamesInput" data-live-search="true"
                                        name="month_no" data-size="10" title="" disabled>
                                      
                                        @foreach ($planimplweeks as $item)
                                            <option value="{{ $item->id }}"
                                                {{ $item->id == old('month_no', $activityfollowup->month_no) ? 'selected' : '' }}>{{ $item->month_no }}
                                            </option>
                                        @endforeach
                                    </select>

                                    @error('month_no')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>



                                <div class="form-group col-md-2">
                                        <label for="nameInput">    السنة </label>
                                         <select class=" form-control" id="stationNamesInput" data-live-search="true"
                                        name="year" data-size="10" title=""disabled>
                                        
                                        @foreach ($planimplweeks as $item)
                                            <option value="{{ $item->id }}"
                                                {{ $item->id == old('year', $activityfollowup->year) ? 'selected' : '' }}> {{ $item->year }}
                                            </option>
                                        @endforeach
                                    </select>

                                    @error('year')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="liquidInput"> حالة النشاط فى هذا الاسبوع     </label>
                                        <select class="form-control" name="activity_status">
                                       
                                        <option value="1" {{ old('activity_status',$activityfollowup->activity_status) == "1" ? 'selected':'' }}>  اعمال جارية   </option>
                                        <option value="2" {{ old('activity_status',$activityfollowup->activity_status) == "2" ? 'selected':'' }}> اعمال انتهت     </option>
                                        <option value="3" {{ old('activity_status',$activityfollowup->activity_status) == "3" ? 'selected':'' }}> اعمال متأخرة     </option>
                                        </select>

                                        @error('activity_status')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>





                                               <!-- <div class="form-group col-md-3">
                                    <label for="nameInput">   حالة النشاط فى هذا الاسبوع </label>
                                        <select class="form-control" name="activity_status">
                                           <option> اختر</option>
                                    <option value="1"{{ old('activity_status',$activityfollowup->activity_status) == "1" ? 'selected':'' }}>لم ينتهى بعد</option>
                                    <option value="3" {{ old('activity_status',$activityfollowup->activity_status) == "3" ? 'selected':'' }}>انتهى </option>
                                    <option value="2" {{ old('activity_status',$activityfollowup->activity_status) == "2" ? 'selected':'' }}>جارى</option>


                                    </select>

                                    @error('activity_status')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div> -->

                                  <div class="form-group col-md-3">
                                    <label for="nameInput">      النسبة المئوية المحققة </label>
                                     <input type="text" class="form-control" id="emailInput" name="achieved_percentage"
                                        value="{{ old('achieved_percentage', $activityfollowup->achieved_percentage) }}" placeholder="  " autocomplete="off">

                                    @error('achieved_percentage')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                    <div class="form-group col-md-3">
                                    <label for="nameInput">        تاريخ الموقف </label>
                                     <input type="date" class="form-control" id="emailInput"  value="{{ old('status_date', $activityfollowup->status_date) }}" name="status_date"
                                         placeholder="  " autocomplete="off">

                                    @error('achieved_percentage')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>


                      </div>


</div>

                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary mt-3">تعديل</button>
                                    <a href="{{ route('activityfollowup.index',['activity_id'=>request()->input('activity_id')])}}" class="btn btn-danger mt-3">إلغاء / عودة </a>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
