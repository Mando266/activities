@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                        <nav class="breadcrumb-two" aria-label="breadcrumb">
                             <ol class="breadcrumb">
                               {{-- <li class="breadcrumb-item"><a href="javascript:void(0);">البيانات الأساسية</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('activity.index') }}"> اضافة نشاط</a></li>--}}
                                <li class="breadcrumb-item active"><a href="javascript:void(0);"> أضف    متابعة جديدة </a></li>
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                    </div>

                    <br>
                    <div class="widget-content widget-content-area">
                        <form id="createForm" action="{{ route('activityfollowup.store') }}" method="POST">
                            @csrf
                            <input type="text" value="{{request()->input('activity_id')}}" name="activity_id" hidden>
                            <div class="form-row">
                                              <div class="form-group col-md-3">
                                              <label for="nameInput">    الاسبوع  / الشهر  /  السنة</label>
                                         <select class=" form-control" id="stationNamesInput" data-live-search="true"
                                        name="plan_impl_weeks_id" data-size="10" title="">
                                        <option value="">اختر</option>
                                        @foreach ($planimplweeks as $item)
                                            <option value="{{ $item->id }}"
                                                {{ $item->id == old('plan_impl_weeks_id') ? 'selected' : '' }}>{{ $item->plan_week_no }} / {{ $item->month_no }} / {{ $item->year }}
                                            </option>
                                        @endforeach
                                    </select>

                                    @error('plan_impl_weeks_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>


<!-- 
                                <div class="form-group col-md-3">
                                    <label for="nameInput">   اسبوع</label>
                                     <input type="text" class="form-control" name="week_no"
                                        value="{{ old('week_no') }}"   " autocomplete="off">

                                    @error('week_no')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>



                                <div class="form-group col-md-3">
                                    <label for="nameInput">   شهر</label>
                                     <input type="text" class="form-control" name="month_no"
                                        value="{{ old('month_no') }}"    >

                                    @error('month_no')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>



                                
                                <div class="form-group col-md-3">
                                    <label for="nameInput">   سنه</label>
                                     <input type="text" class="form-control" name="year"
                                        value="{{ old('year') }}"    >

                                    @error('year')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div> -->
                                <!-- <div class="form-group col-md-3">
                                    <label for="nameInput">   حالة النشاط فى هذا الاسبوع </label>
                                        <select class="form-control" name="activity_status">
                                           <option> اختر</option>
                                    <option value="1">لم ينتهى بعد</option>
                                    <option value="3">انتهى </option>
                                    <option value="2">جارى</option>


                                    </select>

                                    @error('activity_status')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div> -->



                                <div class="form-group col-md-3">
                                    <label for="liquidInput"> حالة النشاط فى هذا الاسبوع     </label>
                                        <select class="form-control" name="activity_status">
                                       
                                        <option value="1" {{ old('activity_status') == "1" ? 'selected':'' }}>  اعمال جارية   </option>
                                        <option value="2" {{ old('activity_status') == "2" ? 'selected':'' }}> اعمال انتهت     </option>
                                        <option value="3" {{ old('activity_status') == "3" ? 'selected':'' }}>  اعمال متأخرة     </option>
                                        </select>

                                        @error('activity_status')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>

                                <div class="form-group col-md-3">
                                    <label for="nameInput">      النسبة المئوية </label>
                                     <input type="text" class="form-control" id="emailInput" name="achieved_percentage"
                                        value="{{ old('achieved_percentage') }}" placeholder=" النسبة المئوية المحققة " autocomplete="off">

                                    @error('achieved_percentage')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>

                              <div class="form-group col-md-3">
                                    <label for="nameInput">        تاريخ الموقف </label>
                                     <input type="date" class="form-control" id="emailInput" name="status_date" value="{{request()->input('status_date', date('Y-m-d'))}}"
                                         placeholder="  " autocomplete="off">

                                    @error('status_date')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>

                      </div>

                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary mt-3">أضف</button>
                                        <a href="{{ route('activityfollowup.index',['activity_id'=>request()->input('activity_id')])}}" class="btn btn-danger mt-3">إلغاء / عودة
                                        </a>
                                    </div>
                                </div>




                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
