@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                        <nav class="breadcrumb-two" aria-label="breadcrumb">
                             <ol class="breadcrumb">
                               {{-- <li class="breadcrumb-item"><a href="javascript:void(0);">البيانات الأساسية</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('activity.index') }}"> اضافة نشاط</a></li>--}}
                                {{-- <li class="breadcrumb-item active"><a href="javascript:void(0);"> أضف   نشاط جديد </a></li> --}}
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                    </div>
                    <div class="widget-content widget-content-area">
                           <form action="{{ route('planWeeks.index') }}" method="GET">
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="line_codeInput">  الهدف الاستراتيجي</label>

                                         <select class=" form-control" id="strategicPlan" data-live-search="true"
                                        name="strategic_plan_name" data-size="10" title="">
                                        <option value="">اختر</option>
                                        @foreach ($strategicplan as $item)
                                        <option value="{{ $item->strategic_plan_name }}"
                                                {{ $item->id == old('strategic_plan_name') ? 'selected' : '' }}>{{ $item->strategic_plan_name }}
                                        @endforeach
                                    </select>
                            </div>
                             <div class="form-group col-md-4">
                                <label for="line_codeInput">  الفترة المخططة للهدف الاستراتيجي من</label>
                                <input type="text" id="target_from" class="form-control" name="target_from"
                                    placeholder="  " autocomplete="off" value="{{ request()->input('target_from') }}">
                            </div>
                               <div class="form-group col-md-4">
                                <label for="line_codeInput">  الفترة المخططة للهدف الاستراتيجي الى</label>
                                <input type="text" id="target_to" class="form-control" name="target_to"
                                    placeholder="  "autocomplete="off" value="{{ request()->input('target_to') }}">
                            </div>
                             </div>
                             <div class="form-row">
                                    <div class="form-group col-md-3">
                                <label for="line_codeInput">  الهدف التنفيذى</label>
                               <select class=" form-control" id="executiveTarget" data-live-search="true"
                                        name="executive_target_name" data-size="10" title="">
                                        <option value="">اختر</option>
                                        @foreach ($executivetarget as $item)
                                        <option value="{{ $item->executive_target_name }}"
                                                {{ $item->id == old('executive_target_name') ? 'selected' : '' }}>{{ $item->executive_target_name }}
                                        @endforeach
                                    </select>
                            </div>
                             <div class="form-group col-md-4">
                                <label for="line_codeInput">  الفترة المخططة للهدف التنفيذى من</label>
                                <input type="text" id="actualFrom" class="form-control" name="actual_from"
                                    placeholder="" autocomplete="off" value="{{ request()->input('actual_from') }}">
                            </div>
                               <div class="form-group col-md-4">
                                <label for="line_codeInput">  الفترة المخططة للهدف التنفيذى الى</label>
                                <input type="text" id="actualTo" class="form-control" name="actual_to"
                                    placeholder="" autocomplete="off" value="{{ request()->input('actual_to') }}">
                            </div>
                             </div>
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary mt-3">بحث</button>
                                <a href="{{ route('planWeeks.index') }}" class="btn btn-danger mt-3">إلغاء البحث</a>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
<script>
    $('.date').datepicker({
        autoclose: true,
        dateFormat: "yy-mm-dd"
    });

    $('#strategicPlan').on('change' ,function(){

        let strategic_plan_name = $(this).val();
        // console.log(strategic_plan_name);

          let response = $.get(`/api/strat/${strategic_plan_name}`).then(function(data) {
         // console.log(data);
          let startegy = data.strat[0];
         // console.log(startegy);
        let target_from = startegy.start_plan_date ;
       // console.log(target_from);
          $('#target_from').val(target_from);
          $('#target_to').val(startegy.end_plan_date);

          });
    });


        $('#executiveTarget').on('change' ,function(){
        // console.log(12);
        let executive_target_name = $(this).val();
        // console.log(strategic_plan_name);

        let response = $.get(`/api/execut/${executive_target_name}`).then(function(data) {
         // console.log(data);
          let executive = data.execut[0];
          console.log(executive);
        let start_plan = executive.target_from ;
           console.log(start_plan);
          $('#actualFrom').val(start_plan);
          $('#actualTo').val(executive.target_to);

          });
    });
</script>
@endpush
