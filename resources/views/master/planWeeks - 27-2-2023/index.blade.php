@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget widget-one">
                    <div class="widget-heading">
                          <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                  {{-- <li class="breadcrumb-item active"><a href="javascript:void(0);">    متابعةالتنفيذ الفعلى للنشاط --}}
                                        </a></li>
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>

                         <div class="widget-content widget-content-area">

                           
                    </br>


                         <div class="table-responsive">
                         <table class="table table-bordered table-hover table-condensed mb-" style="background-color: white;">
                            <tr>
                            <tr>

                                     <th>النشاط </th>
                                        @foreach ($month as $key => $value)
                                    <th colspan="4" value=""style="padding:2px; text-align:center;"> شهر :{{$value->month_no}}</th>
                                       @endforeach
                            </tr>
                            <th></th>
            @foreach ($column_headers as $column_header)
                <th> الاسبوع :{{ $column_header }} </th>
            @endforeach
           </tr>
            </thead>
            <tbody>

        @foreach ($row_headers as $row_header)
            <tr>
                {{-- <td>{{ $excutive_plan}}</td> --}}
                <td>{{ $row_header }}</td>

                         @foreach ($column_headers as $column_header)
                        <?php
                        $activity = $data->where('activity_name', $row_header)->where('plan_week_no', $column_header)->pluck('activity_status1')->first();
                        ?>
                     @if($activity == 'لم ينتهى بعد')
                    <td  style="background: yellow;">لم ينتهى بعد</td>
                    @elseif($activity == 'انتهى')
                     <td  style="background: red;"> انتهى</td>
                     @elseif($activity == 'جارى')
                       <td  style="background: green;">جارى</td>
                       @else
                       <td></td>
                    @endif
                @endforeach
            </tr>
        @endforeach
    </tbody>

                        </table>

                    </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@push('styles')
    <style>
table, th, td {
  border: 1px solid;
}
</style>
@endpush

