@extends('layouts.app')
@section('content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="center" >
                     </br>
                    @if(is_null($company_id))
<h2></h2>
@else
                     <h2 style="text-align:center;text-decoration:underline;font-weight:bold;">  الهدف الاستراتيجي ({{optional($strategic_target)->strategic_target_name}}) <br>  لشركة ({{(Auth::user())->company->name}} )  خلال المراحل التالية   </h2>
                             </div>
                             @endif
                            </br>
                <div class="widget widget-one">
                    <div class="widget-heading">
                        <nav class="breadcrumb-two" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"></li>
                            </ol>
                        </nav>
                    </div>
                    
                    <div class="widget-content widget-content-area">
                           @if(is_null($company_id))
                        <form action="{{ route('planWeeks.index') }}" method="GET">
                            <div class="form-row">

                                <div class="form-group col-md-4">
                                    <label for="line_codeInput"> الشركات </label>
                                    <select class="form-control" id="companyId" data-live-search="true"
                                        name="company_id" data-size="10" title="">
                                        <option value="">اختر</option>
                                        @foreach ($company as $item)
                                            <option value="{{ $item->id }}"
                                                {{ $item->id == old('name') ? 'selected' : '' }}>
                                           {{ $item->name }}
                                        @endforeach
                                    </select>
                                </div>



                                <div class="form-group col-md-4">
                                    <label for="line_codeInput">  مراحل تحقيق الهدف الاسترتيجي </label>
                                    <select class="form-control" id="strategicP" data-live-search="true"
                                        name="" data-size="10" title="">
                                         <option value="">اختر</option>
                                      {{--  @foreach ($strategicplan as $item)
                                            <option value="{{ $item->id }}"
                                                {{ $item->id == old('id') ? 'selected' : '' }}>
                                           {{ $item->strategic_plan_name }}   لتحقيق ايراد قدرة      {{ $item->revenue_achieved }} مليون جنيه
                                        @endforeach --}}
                                    </select>
                                </div>


                                    <input type="text" id="target_from" class="form-control" name="start_plan_date"
                                            placeholder="" autocomplete="off" value="{{ request()->input('start_plan_date') }}" hidden>

                                    <input type="text" id="target_to" class="form-control" name="end_plan_date"
                                        placeholder="  "autocomplete="off" value="{{ request()->input('end_plan_date') }}" hidden >
                                          <input type="text" id="strategic_plan_id" class="form-control" name="strategic_plan_id"
                                        placeholder="  "autocomplete="off"hidden >

                            </div>

                            <input type="text" id="revenue" name="revenue" hidden>
                            <div class="col-md-12 text-center">
                                <button type="submit" id="button" class="btn btn-primary mt-3" disabled>بحث</button>
                                <a href="{{ route('planWeeks.create')}}" class="btn btn-danger mt-3">إلغاء البحث</a>
                            </div>
                    </div>
                    </form>
                                @else
                                <form action="{{ route('planWeeks.index')}}" method="GET">
                            <div class="form-row">
                                   <input type="text" id="" name="company_id" value="{{Auth::user()->company_id}}" hidden>

                                <div class="form-group col-md-4">
                                    <label for="line_codeInput">  مراحل تحقيق الهدف الاسترتيجي </label>
                                    <select class="form-control" id="strategicP" data-live-search="true"
                                        name="strategic_plan_id" data-size="10" title="">
                                         <option value="">اختر</option>
                                       @foreach ($strategicplan as $item)
                                            <option value="{{ $item->id }}"
                                                {{ $item->id == old('id') ? 'selected' : '' }}>
                                           {{ $item->strategic_plan_name }}   لتحقيق ايراد قدرة      {{ $item->revenue_achieved }} مليون جنيه
                                        @endforeach
                                    </select>
                                </div>


                                        <input type="text" id="target_from" class="form-control" name="start_plan_date"
                                            placeholder="" autocomplete="off" value="{{ request()->input('start_plan_date') }}" hidden>

                                    <input type="text" id="target_to" class="form-control" name="end_plan_date"
                                        placeholder="  "autocomplete="off" value="{{ request()->input('end_plan_date') }}" hidden >


                            </div>

                              <input type="text" id="revenue" name="revenue" hidden>
                            <div class="col-md-12 text-center">
                                <button type="submit" id="button" class="btn btn-primary mt-3" disabled>بحث</button>
                                <a href="{{ route('planWeeks.create') }}" class="btn btn-danger mt-3">إلغاء البحث</a>
                            </div>
                    </div>
                    </form>
                         @endif
                </div>
            </div>

        </div>
    </div>
@endsection
@if (is_null($company_id))
@push('scripts')
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
    <script>
           $('.date').datepicker({
            autoclose: true,
            dateFormat: "yy-mm-dd"
        });


        $('#companyId').on('change', function(){
            console.log(companyId);
           $('#button').prop('disabled', false);
              let id = $('#companyId').val();
             // let self = $(this).val();
             // console.log(self);
              let response = $.get(`/api/comp/${id}`).then(function(data) {
              let codes = data.comp;
              let list = ['<option value=""> اختر</option>'];

                 for (let i = 0; i < codes.length; i++) {
                    //  list2.push(`<option value="${staTions[i].station_id}"'>${staTions[i].station_name} </option>`);
                       list.push(`<option value='${codes[i].company_id}' data-rev='${codes[i].revenue_achieved}' data-strat='${codes[i].strategic_plan_id}' data-str='${codes[i].start_plan_date}' data-end='${codes[i].end_plan_date}'>${codes[i].strategic_plan_name} </option>`);
                }
                 let strategicP = $('#strategicP');
                 strategicP.html(list.join(''));
              $('#strategicP').on('change', function(){
                    //  console.log(12);
                      let self = $(this);
                      let renvnueInput = self.find(':selected');
                      let revenData = renvnueInput.data('rev');
                      let strtDate = renvnueInput.data('str');
                      let endDate = renvnueInput.data('end');
                     let strat = renvnueInput.data('strat');
                    $('#revenue').val(revenData);
                    $('#target_from').val(strtDate);
                    $('#target_to').val(endDate);
                     $('#strategic_plan_id').val(strat);
                  });
        });

            });



    </script>
     <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
@endpush
@else
@push('scripts')
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
    <script>
           $('.date').datepicker({
            autoclose: true,
            dateFormat: "yy-mm-dd"
        });





        $('#strategicP').on('change', function() {
            $('#button').prop('disabled', false);
            let id = $('#strategicP').val();
           // console.log(id);
            let response = $.get(`/api/strat/${id}`).then(function(data) {
                let strat = data.strat || '';
                let target_from = strat.start_plan_date;
                let target_to = strat.end_plan_date;
                 let revenue = strat.revenue_achieved;
                $('#target_from').val(target_from);
                $('#target_to').val(target_to);
                $('#revenue').val(revenue);
            });
        });

        // $('#executiveTarget').on('change', function() {
        //     let id = $('#executiveTarget').val();
        //     let response = $.get(`/api/execut/${id}`).then(function(data) {
        //         let execut = data.execut || '';
        //         let actual_from = execut.target_from;
        //         let actual_to = execut.target_to;
        //         $('#actualFrom').val(actual_from);
        //         $('#actualTo').val(actual_to);
        //     });
        // });

    </script>
     <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
@endpush
@endif

