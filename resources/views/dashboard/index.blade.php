@extends('layouts.app')
@section('content')


@endsection
@push('styles')
    <link href="{{asset('plugins/apex/apexcharts.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/dashboard/dash_1.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
  <script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
  
     <script src="plugins/highlight/highlight.pack.js"></script>
    <script src="assets/js/custom.js"></script>
    <script src="{{asset('plugins/apex/apexcharts.min.js')}}"></script>
    <script src="{{asset('assets/js/dashboard/dash_1.js')}}"></script>
@endpush