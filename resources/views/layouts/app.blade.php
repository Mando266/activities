<!DOCTYPE html>
<html lang="en">
<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-R2T63KPHJ0"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-R2T63KPHJ0');
</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>Hcmlt</title>
    <!-- <link rel="icon" type="image/x-icon" href="{{asset('assets/img/fav.ico')}}"/> -->
    <link href="{{asset('assets/css/loader.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{asset('assets/js/loader.js')}}"></script>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Almarai:wght@300&display=swap" rel="stylesheet">

    <link href="{{asset('fontawesome/css/all.min.css')}}" rel="stylesheet" type="text/css">

    <link href="{{asset($dir.'bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset($dir.'assets/css/plugins.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset($dir.'assets/css/structure.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset($dir.'assets/css/dashboard/dash_1.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset($dir.'assets/css/tables/table-basic.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset($dir.'assets/css/forms/theme-checkbox-radio.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset($dir.'assets/css/elements/alert.css')}}" rel="stylesheet" type="text/css" >
    <link href="{{asset($dir.'plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" >
    <link href="{{asset($dir.'assets/css/elements/breadcrumb.css')}}" rel="stylesheet" type="text/css" />

    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <style>
        .alert-light-success span{
            color: #0e1726;
        }
        .paginating-container {
            display: flex;
            justify-content: center;
            margin-bottom: 0;
        }
        .bootstrap-select.btn-group > .dropdown-toggle{
            max-height: 100%;
        }
        .list li{
            color: #212529;
        }
    </style>
    @stack('styles')
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->

</head>
<body>
    <!-- BEGIN LOADER -->
    <div id="load_screen"> <div class="loader"> <div class="loader-content">
        <div class="spinner-grow align-self-center"></div>
    </div></div></div>
    <!--  END LOADER -->

    <!--  BEGIN NAVBAR  -->
    <div class="header-container fixed-top">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-item flex-row" style="margin-right: 15px;">
                <li class="nav-item align-self-center page-heading">
                    <div class="page-header">
                        <div class="page-title">
                              @if(Auth::user()->is_super_admin || is_null(Auth::user()->company_id))
                            <h5></h5>
                            <h5> {{(Auth::user())->company->name}}</h5>
                             @endif
                            </div>
                    </div>
                </li>
            </ul>
            <ul class="navbar-item flex-row search-ul">

            </ul>

            <ul class="navbar-item flex-row">
                <li class="nav-item align-self-center page-heading navbar-profile">
                    <div class="page-header">
                        <div class="user-title">
                        <div class="media mx-auto">
                            <img src="{{optional(Auth::user())->getAvatarUrl()}}" class="img-fluid mr-2" alt="avatar" style="width:33px;">
                            <div class="media-body">
                                <h5>{{optional(Auth::user())->name}}</h5>
                            </div>
                        </div>
                        </div>
                    </div>
                </li>
            </ul>
            <ul class="navbar-item flex-row navbar-dropdown">
                <li class="nav-item dropdown language-dropdown more-dropdown mx-2">

                </li>

                </li>
                @include('layouts._profile_menu')


            </ul>

        </header>
    </div>
    <!--  END NAVBAR  -->

    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

        <!--  BEGIN SIDEBAR  -->
        @include('layouts._sidebar')
        <!--  END SIDEBAR  -->

        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            @if(session()->has('success'))
            <div class="alert alert-arrow-left alert-icon-left alert-light-success  m-4 d-block" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" data-dismiss="alert" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                </button>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell"><path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path><path d="M13.73 21a2 2 0 0 1-3.46 0"></path></svg>
                <strong>Success!</strong><span> {{ session()->get('success')}}</span>
            </div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-arrow-left alert-icon-left alert-light-danger  m-4 d-block"
                role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" data-dismiss="alert" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell"><path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path><path d="M13.73 21a2 2 0 0 1-3.46 0"></path></svg>
                    <strong>Error!</strong><span> {{ session()->get('error')}} </span>
                </div>
            @endif
            @yield('content')

            <div class="footer-wrapper">
                <div class="footer-section f-section-1">
                    <p class="">جميع الحقوق محفوظة لدى الشركة القابضة للنقل البحرى والبرى © 2023</p>
                </div>

            </div>
        </div>
        <!--  END CONTENT AREA  -->


    </div>
    <!-- END MAIN CONTAINER -->

    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="{{asset($dir.'assets/js/libs/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset($dir.'bootstrap/js/popper.min.js')}}"></script>
    <script src="{{asset($dir.'bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset($dir.'plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script src="{{asset($dir.'assets/js/app.js')}}"></script>
    <script src="{{asset($dir.'plugins/bootstrap-select/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="Stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="/jquery.js"></script>
    <script src="/build/jquery.datetimepicker.full.min.js"></script>
    <script>
    $(function () {
            $("#reporDateFrom").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
            });
            $("#DateFrom").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
            });
        });
</script>

    <script>
        $(document).ready(function() {
            App.init();
            function hasArabicCharacters(text){

            var regex = new RegExp("[\u0600-\u06FF]|[\u0750-\u077f]|[\ufb50-\ufdff]|[\ufe70-\ufeff]|[\u0660-\u0669]|[\u08A0-\u08FF]","gmu");

            return regex.test(text);
            }

            $('.no_arabic').on('keyup',function (e) {
                var value = (e.target.value);
                if(value === '.')return true;
                var self = $(this);
                var old = "";
                if(value.length > 1){
                    old = value.substring(0,value.length-1);
                    if(hasArabicCharacters(value[value.length-1])){
                        self.val(old);
                    }
                }else{
                    if(hasArabicCharacters(value)){
                        self.val(old);
                    }
                    old = value.substring(0,1);
                }

                return true;
            });
            $('.clone-table').on('keyup','.no_arabic',function (e) {
                var value = (e.target.value);
                if(value === '.')return true;
                var self = $(this);
                var old = "";
                if(value.length > 1){
                    old = value.substring(0,value.length-1);
                    if(hasArabicCharacters(value[value.length-1])){
                        self.val(old);
                    }
                }else{
                    if(hasArabicCharacters(value)){
                        self.val(old);
                    }
                    old = value.substring(0,1);
                }

                return true;
            });
            let numberOnlyInputs = $('.numbers-only');
            numberOnlyInputs.on('keydown',function (e) {
                if(e.target.value === '.')return true;
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    return;
                }
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
            $('.clone-table').on('keydown','.numbers-only',function (e) {
                if(e.target.value === '.')return true;
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    return;
                }
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
    </script>
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
    @stack('scripts')
    @if(App::isLocale('ar'))
    <script>
     $('.menu a').on('click',function(e){
        window.location.href = $(this).attr('href');
    });
    </script>
    @endif
</body>
</html>
