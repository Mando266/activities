<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>Hcmlt  Login </title>
    <!-- <link rel="icon" type="image/x-icon" href="{{asset('assets/img/fav.ico')}}"/> -->
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="{{asset($dir.'bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset($dir.'assets/css/plugins.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset($dir.'assets/css/authentication/form-1.css')}}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="{{asset($dir.'assets/css/forms/theme-checkbox-radio.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset($dir.'assets/css/forms/switches.css')}}">

    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Almarai:wght@800&display=swap" rel="stylesheet">
    <style>
    body {
    height: 100%;
    overflow: auto;
    margin: 0;
    padding: 0;
    background: url(/assets/img/1.jpg) no-repeat;
    /* background: #000; */

    background-size: 100% ;
    object-fit: cover;
    justify-content: center;
    flex-direction: column;
    display: flex;
    }
    .banner{
        position: fixed;
        top: 0;
        width: 100%;
        margin:0;
        height:65px;
        font-size:25px;
        text-align:center;
        color:#182930;
        /* text-shadow: 2px 4px solid red; */
    
    }
    
        
    
 
.form-container {
    display: flex;
    justify-content: center;
}

.form-form {
    width: 100%;
    display: flex;
    flex-direction: column;
}
.form-form .form-form-wrap {
    max-width: 460px;
    margin: 0 auto;
    min-width: 311px;
    min-height: 120%;
    height: 50%;
    align-items: center;
    justify-content: center;
    background: #ffffff;
    opacity: .9;
    border-radius: 22px;
}
    </style>
</head>

<body class="form">
<div class="row banner">
<div class="col-md-12  " style="background:#2C2F51;">

<p style="color:#F9F9FB;font-family: 'Almarai', sans-serif;">  متابعة تنفيذ انشطة الاهداف للخطه الاستراتيجيه</p>
</div>
</div>
    <div class="form-container">

        <div class="form-form">
            <div class="form-form-wrap">
                <div class="form-container">
                    <div class="form-content">
                        @yield('content')                   
                        <p class="terms-conditions">جميع الحقوق محفوظة لدى الشركة القابضة للنقل البحرى والبرى © 2023 </p>

                    </div>                    
                </div>
            </div>
        </div>
        
    </div>

    
    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="{{asset($dir.'assets/js/libs/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset($dir.'bootstrap/js/popper.min.js')}}"></script>
    <script src="{{asset($dir.'bootstrap/js/bootstrap.min.js')}}"></script>
    
    <!-- END GLOBAL MANDATORY SCRIPTS -->
    <script src="{{asset($dir.'assets/js/authentication/form-1.js')}}"></script>

</body>
</html>