<div class="sidebar-wrapper sidebar-theme">

    <nav id="sidebar">

<ul class="navbar-nav theme-brand flex-row  text-center">
                     <li class="nav-item theme-logo" style="height: 60px;">
                <a href="{{route('home')}}">
                    <img src="{{asset('assets/img/logo.png')}}" class="navbar-logo" alt="logo" style="height: 55px;">
                </a>
            </li>
            <li class="nav-item theme-text">
                <a href="{{route('home')}}" class="nav-link"> HCMLT </a>
            </li>
            <li class="nav-item toggle-sidebar">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                    class="feather feather-arrow-left sidebarCollapse">
                    <line x1="19" y1="12" x2="5" y2="12"></line>
                    <polyline points="12 19 5 12 12 5"></polyline>
                </svg>
            </li>
                </ul>



                

                <div></div>
                      <ul class="list-unstyled menu-categories" id="accordionExample">
                @if(is_null(Auth::user()->company_id))
                @permission('Role-List')
                <li class="menu">
                <a href="{{route('planWeeks.create')}}" data-toggle="collapse" data-link="true" aria-expanded="true" class="dropdown-toggle">
                    <div class="">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shield">
                      <path d="M12 22s8-4 8-10V5l-8-3-8 3v7c0 6 8 10 8 10z"></path></svg>
                        <span> متابعة الاهداف </span>
                    </div>
                    <div>
                    </div>
                </a>
            </li>
             @endpermission
                @endif
                @if(!is_null(Auth::user()->company_id))
                      @permission('Linecompany-List')
                    <li class="menu active">
                        <a href="#component3" data-toggle="collapse" data-link="true" aria-expanded="true"
                            class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24"tColor" stroke-width="2"
                                    stroke-linecap="round" stroke-linejoin="round" class="feather feather-database">
                                    <ellipse cx="12" cy="5" rx="9" ry="3"></ellipse>
                                    <path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path>
                                    <path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3 height="24"
                                    viewBox="0 0 24 24" fill="none" stroke="currenV5"></path>
                                </svg>
                                <span>  الاكواد </span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                    viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                    stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled" id="component3" data-parent="#accordionExample">
                            <li>
                                <a href="{{ route('perfornanceindex.index') }}">  مؤشر الاداء </a>
                            </li>

                            <li>
                                <a href="{{ route('adminunit.index') }}">   الوحدة الادارية </a>
                            </li>
                          

                            <!-- <li>
                                <a href="{{ route('employee.index') }}">  الموظف </a>
                            </li> -->
                           

                        </ul>
                    </li>
                @endpermission
                 
               
                     @permission('Linecompany-List')
                    <li class="menu active">
                        <a href="#element1" data-toggle="collapse" data-link="true" aria-expanded="true"
                            class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round" class="feather feather-user">
                                    <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                    <circle cx="12" cy="7" r="4"></circle>
                                </svg>
                                <span> البيانات الأساسية </span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>

                    <ul class="collapse submenu list-unstyled" id="element1" data-parent="#accordionExample">



                        @permission('PlanYearMonthWeeksView-List')
                    <li>
                        <a href="{{route('strategicplan.index')}}">    الخطة الاستراتيجية للشركة </a>
                    </li>
                @endpermission

                


                            @permission('TargetIndexPlanExecutiveView-List')
                    <li>
                        <a href="{{route('targetindex.index')}}">    مؤشر اداء الهدف </a>
                    </li>
                @endpermission
                <!-- @permission('TargetIndexPlanExecutiveView-List') -->
                    <li>
                        <a href="{{route('employee.index')}}">      الموظفين </a>
                    </li>
                <!-- @endpermission -->

                        </ul>


         <li class="menu">
         <a href="#dashboard_252" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">
        <div class="">
            <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="7"></circle><polyline points="12 9 12 12 13.5 13.5"></polyline><path d="M16.51 17.35l-.35 3.83a2 2 0 0 1-2 1.82H9.83a2 2 0 0 1-2-1.82l-.35-3.83m.01-10.7l.35-3.83A2 2 0 0 1 9.83 1h4.35a2 2 0 0 1 2 1.82l.35 3.83"></path></svg>
            <span>بيانات الحركة </span>
        </div>

        <div>
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
        </div>
    </a>

        <ul class="collapse submenu list-unstyled" id="dashboard_252" data-parent="#accordionExample">


        @permission('Strategictarget-List')
                                <li>
                                    <a href="{{ route('strategictarget.index') }}">  الهدف الاستراتيجي   </a>
                                </li>
                            @endpermission




                @permission('EexecutiveTargetCompView-List')
                    <li>
                        <a href="{{route('executivetarget.index')}}">   الاهداف التنفيذية </a>
                    </li>
                @endpermission



              


                <!-- @permission('ActivityFollowExecutiveView-List')
                                <li>
                                    <a href="{{ route('activity.index') }}"> متابعة التنفيذ الفعلي للنشاط </a>
                                </li>
                            @endpermission -->
        </ul>

    </li>

                </a>
            </li>
            @if((Auth::user()->company_id) == 4)
            
                     @permission('User-List')
            <li class="menu">
                <a href="{{route('users.index')}}" data-toggle="collapse" data-link="true" aria-expanded="true" class="dropdown-toggle">
                    <div class="">
                   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                        <span>المستخدمين</span>
                    </div>
                    <div>
                    </div>
                </a>
            </li>
                @endpermission
                @permission('Role-List')
                <li class="menu">
                <a href="{{route('roles.index')}}" data-toggle="collapse" data-link="true" aria-expanded="true" class="dropdown-toggle">
                    <div class="">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shield">
                      <path d="M12 22s8-4 8-10V5l-8-3-8 3v7c0 6 8 10 8 10z"></path></svg>
                        <span>الأدوار / الصلاحيات</span>
                    </div>
                    <div>
                    </div>
                </a>
            </li>
             @endpermission
             @endif
               @permission('Role-List')
                <li class="menu">
                <a href="{{route('planWeeks.create')}}" data-toggle="collapse" data-link="true" aria-expanded="true" class="dropdown-toggle">
                    <div class="">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shield">
                      <path d="M12 22s8-4 8-10V5l-8-3-8 3v7c0 6 8 10 8 10z"></path></svg>
                        <span> متابعة الاهداف </span>
                    </div>
                    <div>
                    </div>
                </a>
            </li>
             @endpermission
       @endif
 @endpermission
        </ul>
    </nav>
</div>
