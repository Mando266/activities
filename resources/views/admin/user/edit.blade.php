@extends('layouts.app')
@section('content')
<div class="layout-px-spacing">
    <div class="row layout-top-spacing">

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-one">
                <div class="widget-heading">
                    <nav class="breadcrumb-two" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);"></a></li>
                            <li class="breadcrumb-item"><a href="{{route('users.index')}}">اعضاء مجلس الادارة</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="javascript:void(0);">تعديل</a></li>
                            <li class="breadcrumb-item"></li>
                        </ol>
                    </nav>
                </div>
                <div class="widget-content widget-content-area">
                <form id="createForm" action="{{route('users.update',['user'=>$user->id])}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')


                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="userName">أسم عضو مجلس الادارة <span class="text-warning">  </span></label>
                            <input type="text" class="form-control" id="userName" name="name" value="{{old('name',$user->name)}}"
                                 placeholder="أسم المستخدم" autocomplete="off" autofocus maxlength="30">
                                @error('name')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="fullName">الأسم كامل</label>
                            <input type="text" class="form-control" id="fullName" name="full_name" value="{{old('full_name',$user->full_name)}}"
                                 placeholder="الأسم كامل" autocomplete="off" maxlength="128">
                                @error('full_name')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="passwordInput">كلمة المرور<span class="text-warning"></span></label>
                                <input type="password" class="form-control" id="passwordInput" name="password" maxlength="30"
                                    placeholder="كلمة المرور" autocomplete="off" >
                                @error('password')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="passwordConfirmInput">أعد إدخال كلمة المرور</label>
                                <input type="password" class="form-control" id="passwordConfirmInput" name="password_confirmation" maxlength="30"
                                    placeholder="أعد إدخال كلمة المرور" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-row mb-4">
                            <div class="form-group col-md-6">
                                <label for="email">البريد الألكترونى</label>
                            <input type="text" class="form-control" id="email" name="email" value="{{old('email',$user->email)}}" maxlength="128"
                                 placeholder="البريد الألكترونى" autocomplete="off" >
                                @error('email')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <hr/>

                        <div class="form-row">

                            <div class="form-group col-md-6">
                                <label for="role">الدور الوظيفى<span class="text-warning"> </span></label>
                                <select class="selectpicker show-tick form-control" id="role" data-live-search="true" name="role" title="أختر">
                                    @foreach ($roles as $item)
                                    <option value="{{$item->id}}" {{$item->id == old('role',optional($user->roles->first())->id) ? 'selected' :''}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                                @error('role')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="status">حالة المستخدم</label>
                                <select class="selectpicker form-control"  name="is_active">
                                    <option value="1" {{ old('status',$user->is_active) == "1" ? 'selected':'' }}>Enabled</option>
                                    <option value="0" {{ old('status',$user->is_active) == "0" ? 'selected':'' }}>Disabled</option>
                                </select>
                                @error('status')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                        </div>
                        <hr/>

                       <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary mt-3">تعديل</button>
                                <a href="{{route('users.index')}}" class="btn btn-danger mt-3">إلغاء</a>
                            </div>
                       </div>


                    </form>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
@push('styles')
<link href="{{asset('plugins/sweetalerts/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/sweetalerts/sweetalert.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/css/components/custom-sweetalert.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/css/elements/avatar.css')}}" rel="stylesheet" type="text/css" />
<style>
    .custom-file-container label {
    color: #3b3f5c;
}
.custom-file-container__image-preview{
    display: none;
}
.avatar img {
    object-fit: scale-down;
}
</style>
@endpush
@push('scripts')
    <script src="{{asset('plugins/sweetalerts/sweetalert2.min.js')}}"></script>
    <script src="{{asset('plugins/sweetalerts/custom-sweetalert.js')}}"></script>
    <script src="{{ asset('plugins/file-upload/file-upload-with-preview.min.js')}}"></script>
    <script src="{{ asset('app/admin/user.js') }}"></script>
    <script>
        var firstUpload = new FileUploadWithPreview('avatar',{
            images: {
                    baseImage: '{{asset('assets/img/profile.png')}}',
                }
        })
    </script>
@endpush

