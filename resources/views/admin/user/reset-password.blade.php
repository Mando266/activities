@extends('layouts.app')
@section('content')
<div class="layout-px-spacing">
    <div class="row layout-top-spacing">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="widget widget-one">
                <div class="widget-heading">
                <h5 class="">تغير كلمة المرور</h5>
                </div>
                <div class="widget-content widget-content-area">
                <form id="createForm" action="{{route('user.reset-password')}}" method="POST">
                        @csrf
                        @method('put')
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="passwordInput">كلمة المرور </label>
                                <input type="password" class="form-control" id="passwordInput" name="password"
                                    placeholder="كلمة المرور " autocomplete="off" autofocus>
                                @error('password')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="passwordConfirmInput">إعادة إدخال كلمة المرور</label>
                                <input type="password" class="form-control" id="passwordConfirmInput" name="password_confirmation"
                                    placeholder="إعادة إدخال كلمة المرور" autocomplete="off">
                            </div>
                        </div>

                       <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary mt-3">تعديل</button>
                                <a href="{{route('home')}}" class="btn btn-danger mt-3">إلغاء</a>
                            </div>
                       </div>


                    </form>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
