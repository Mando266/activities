<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityFollowExecutiveView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("alter  VIEW activity_follow_executive_view_x
        AS
        SELECT `activity`.id,`responsible_sector`,`planned_from`,`planned_to`,activity_name,
`executive_target_id`,`actual_from`,`actual_to`,`strategic_plan`.`company_id`,
`company`.name as company_name,executive_target.executive_target_name
FROM `activity`
join activity_follow_up
on(activity_follow_up.`activity_id` = `activity`.`id`)
left join `executive_target`
on(activity.`executive_target_id` = `executive_target`.`id`)


left join  `strategic_plan`
on(`executive_target`.`strategic_plan_id` = `strategic_plan`.`id`)
left join  `company`
on(`strategic_plan`.`company_id` = `company`.`id`)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_follow_executive_view');
    }
}
