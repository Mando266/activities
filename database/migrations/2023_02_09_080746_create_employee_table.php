<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeTable extends Migration
{
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedbigInteger('admin_unit_id')->nullable();
            $table->unsignedbigInteger('company_id')->nullable();
            $table->string('name',100);
            $table->string('address',100)->nullable();
            $table->integer('is_active')->nullable();
            $table->bigInteger('nat_id')->nullable();
            $table->string('email',300)->nullable();
            $table->string('code',200)->nullable();
            $table->string('job',300)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
