<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddEmployeeFk extends Migration
{
    public function up()
    {
        Schema::table('employee', function (Blueprint $table) {
            DB::statement("ALTER TABLE employee
                                ADD FOREIGN KEY  (admin_unit_id)
                                            REFERENCES admin_unit(id);");
            DB::statement("ALTER TABLE employee
                                ADD FOREIGN KEY  (company_id)
                                            REFERENCES company(id);");
        });
    }

    public function down()
    {
        Schema::table('employee', function (Blueprint $table) {
            //
        });
    }
}
