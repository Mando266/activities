<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityFollowUpTable extends Migration
{
    public function up()
    {
        Schema::create('activity_follow_up', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('activity_id');
            $table->unsignedBigInteger('plan_impl_weeks_id');
            $table->Integer('activity_status')->nullable();
            $table->Integer('achieved_percentage')->nullable();
            $table->bigInteger('week_no')->nullable();
          
            $table->Date('status_date')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('activity_follow_up');
    }
}
