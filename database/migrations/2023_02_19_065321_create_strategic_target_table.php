<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStrategicTargetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('strategic_target', function (Blueprint $table) {
           
            $table->bigIncrements('id');
            $table->string("strategic_target_name");
            $table->Date('target_from')->nullable();
            $table->Date('target_to')->nullable();
            $table->bigInteger('target_estimated_budget')->nullable();
            $table->unsignedbigInteger('strategic_plan_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('strategic_target');
    }
}
