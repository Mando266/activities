<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFundingSourcesTable extends Migration
{
    public function up()
    {
        Schema::create('funding_sources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->BigInteger('funding_entity')->nullable();
            $table->BigInteger('funding_value')->nullable();
            $table->unsignedBigInteger('company_id');
            $table->BigInteger('executive_target_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('funding_sources');
    }
}
