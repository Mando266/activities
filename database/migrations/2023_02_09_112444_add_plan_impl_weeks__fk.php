<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddPlanImplWeeksFk extends Migration
{
    public function up()
    {
        Schema::table('plan_impl_weeks', function (Blueprint $table) {
            DB::statement("ALTER TABLE plan_impl_weeks
                                    ADD FOREIGN KEY  (strategic_plan_id)
                                             REFERENCES strategic_plan(id);");
        });
    }

    public function down()
    {
        Schema::table('plan_impl_weeks', function (Blueprint $table) {
            //
        });
    }
}
