<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddExecutiveTargetFk extends Migration
{
    public function up()
    {
        Schema::table('executive_target', function (Blueprint $table) {
            DB::statement("ALTER TABLE executive_target
                                    ADD FOREIGN KEY  (strategic_plan_id)
                                             REFERENCES strategic_plan(id);");
            DB::statement("ALTER TABLE executive_target
                                             ADD FOREIGN KEY  (funding_source_id)
                                                      REFERENCES funding_sources(id);");
        });
    }

    public function down()
    {
        Schema::table('executive_target', function (Blueprint $table) {
            //
        });
    }
}
