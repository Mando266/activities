<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStrategicPlanTable extends Migration
{
    public function up()
    {
        Schema::create('strategic_plan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('strategic_plan_name',100)->nullable();
            $table->integer('plan_years_no')->nullable();
            $table->Date('start_plan_date')->nullable();
            $table->Date('end_plan_date')->nullable();
            $table->unsignedbigInteger('company_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('strategic_plan');
    }
}
