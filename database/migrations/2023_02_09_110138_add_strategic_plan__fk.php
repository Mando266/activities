<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddStrategicPlanFk extends Migration
{
    public function up()
    {
        Schema::table('strategic_plan', function (Blueprint $table) {
            DB::statement("ALTER TABLE strategic_plan
                                    ADD FOREIGN KEY  (company_id)
                                             REFERENCES company(id);");
        });
    }

    public function down()
    {
        Schema::table('strategic_plan', function (Blueprint $table) {
            //
        });
    }
}
