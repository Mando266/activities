<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddEmployeeUq extends Migration
{
    public function up()
    {
        Schema::table('employee', function (Blueprint $table) {
            DB::statement("ALTER TABLE  employee
                                 ADD CONSTRAINT  employee_uq
                                        UNIQUE KEY(company_id,code);");
        });
    }

    public function down()
    {
        Schema::table('employee', function (Blueprint $table) {
            //
        });
    }
}
