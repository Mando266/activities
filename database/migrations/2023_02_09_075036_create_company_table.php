<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyTable extends Migration
{
    public function up()
    {
        {
            Schema::create('company', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name',100);
                $table->string('address',100)->nullable();
                $table->string('email',100)->unique();
                $table->string('telephone',14)->unique();
                $table->string('code',20);
                $table->string('web_site',100)->nullable();
                $table->string('commercial_register',100)->nullable();
                $table->bigInteger('tax_card')->nullable();
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('company');
    }
}
