<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityTable extends Migration
{
    public function up()
    {
        Schema::create('activity', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedbigInteger('admin_unit_id')->nullable();
            $table->string('activity_name',100)->nullable();
            $table->string('responsible_sector',100)->nullable();
            $table->Date('planned_from')->nullable();
            $table->Date('planned_to')->nullable();
            $table->unsignedbigInteger('executive_target_id')->nullable();
            $table->DateTime('actual_from');
            $table->DateTime('actual_to');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('activity');
    }
}
