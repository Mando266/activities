<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddEmpInActivityFk extends Migration
{
    public function up()
    {
        Schema::table('emp_in_activity', function (Blueprint $table) {
            DB::statement("ALTER TABLE emp_in_activity
                                    ADD FOREIGN KEY  (admin_unit_id)
                                             REFERENCES admin_unit(id);");
            DB::statement("ALTER TABLE emp_in_activity
                                    ADD FOREIGN KEY  (employee_id)
                                                       REFERENCES employee(id);");
            DB::statement("ALTER TABLE emp_in_activity
                                    ADD FOREIGN KEY  (activity_id)
                                                        REFERENCES activity(id);");
        });
    }

    public function down()
    {
        Schema::table('emp_in_activity', function (Blueprint $table) {
            //
        });
    }
}
