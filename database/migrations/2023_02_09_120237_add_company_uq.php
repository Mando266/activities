<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddCompanyUq extends Migration
{
    public function up()
    {
        Schema::table('company', function (Blueprint $table) {
            DB::statement("ALTER TABLE  company
                                        ADD CONSTRAINT  company_uq
                                                UNIQUE KEY(code);");
        });
    }

    public function down()
    {
        Schema::table('company', function (Blueprint $table) {
            //
        });
    }
}
