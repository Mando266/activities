<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddPlanImplWeeksUq extends Migration
{
    public function up()
    {
        Schema::table('plan_impl_weeks', function (Blueprint $table) {
            DB::statement("ALTER TABLE  plan_impl_weeks
                                    ADD CONSTRAINT  plan_impl_weeks_uq
                                            UNIQUE KEY(strategic_plan_id,month_week_no);");
        });
    }

    public function down()
    {
        Schema::table('plan_impl_weeks', function (Blueprint $table) {
            //
        });
    }
}
