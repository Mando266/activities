<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddTargetOutputFk extends Migration
{
    public function up()
    {
        Schema::table('target_output', function (Blueprint $table) {
            DB::statement("ALTER TABLE target_output
                                    ADD FOREIGN KEY  (executive_target_id)
                                                REFERENCES executive_target(id);");
        });
    }

    public function down()
    {
        Schema::table('target_output', function (Blueprint $table) {
            //
        });
    }
}
