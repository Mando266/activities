<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdminInActivityFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_in_activity', function (Blueprint $table) {
            DB::statement("ALTER TABLE admin_in_activity
                                 ADD FOREIGN KEY  (activity_id)
                                            REFERENCES activity(id);");
            DB::statement("ALTER TABLE admin_in_activity
                                     ADD  FOREIGN KEY (admin_unit_id)
                                            REFERENCES admin_unit(id);");
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_in_activity', function (Blueprint $table) {
            //
        });
    }
}
