<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddActivityFollowUpFk extends Migration
{
    public function up()
    {
        Schema::table('activity_follow_up', function (Blueprint $table) {
            DB::statement("ALTER TABLE activity_follow_up
                                    ADD FOREIGN KEY  (plan_impl_weeks_id)
                                             REFERENCES plan_impl_weeks(id);");
            DB::statement("ALTER TABLE activity_follow_up
                                    ADD FOREIGN KEY  (activity_id)
                                                       REFERENCES activity(id);");
        });
    }

    public function down()
    {
        Schema::table('activity_follow_up', function (Blueprint $table) {
            //
        });
    }
}
