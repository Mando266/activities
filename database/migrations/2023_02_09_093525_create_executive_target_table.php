<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExecutiveTargetTable extends Migration
{
    public function up()
    {
        Schema::create('executive_target', function (Blueprint $table) {
            $table->unsignedbigInteger('executive_target_id')->nullable();
            $table->unsignedbigInteger('funding_source_id');
            $table->unsignedbigInteger('strategic_plan_id')->nullable();
            $table->string('executive_target_name',100)->nullable();
            $table->Date('target_from')->nullable();
            $table->Date('target_to')->nullable();
            $table->bigInteger('target_estimated_budget')->nullable();
            $table->unsignedbigInteger('strategic_target_id')->nullable();
         
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('executive_target');
    }
}
