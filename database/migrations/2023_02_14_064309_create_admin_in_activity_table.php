<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminInActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_in_activity', function (Blueprint $table) {
            $table->id();
           
            $table->unsignedbigInteger('admin_unit_id');
            $table->unsignedbigInteger('activity_id');
            $table->Date('planned_from')->nullable();
            $table->Date('planned_to')->nullable();
            $table->DateTime('actual_from');
            $table->DateTime('actual_to');
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_in_activity');
    }
}
