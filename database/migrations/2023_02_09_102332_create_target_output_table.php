<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTargetOutputTable extends Migration
{
    public function up()
    {
        Schema::create('target_output', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('target_output_name',300);
            $table->unsignedbigInteger('executive_target_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('target_output');
    }
}
