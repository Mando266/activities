<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanImplWeeksTable extends Migration
{
    public function up()
    {
        Schema::create('plan_impl_weeks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('month_no')->nullable();
            $table->bigInteger('plan_week_no');
            $table->unsignedbigInteger('strategic_plan_id')->nullable();
            $table->bigInteger('year')->nullable();
            $table->bigInteger('month_week_no')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('plan_impl_weeks');
    }
}
