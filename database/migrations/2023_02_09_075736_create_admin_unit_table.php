<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminUnitTable extends Migration
{
    public function up()
    {
        Schema::create('admin_unit', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id')->nullable();
            $table->unsignedBigInteger('admin_unit_typ_id')->nullable();
            $table->string('code',20);
            $table->string('name',100);
            $table->string('address',100)->nullable();
            $table->string('lat',300)->nullable();
            $table->string('lang',300)->nullable();
            $table->string('tel',14)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('admin_unit');
    }
}
