<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpInActivityTable extends Migration
{
    public function up()
    {
        Schema::create('emp_in_activity', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedbigInteger('employee_id');
            $table->unsignedbigInteger('admin_unit_id');
            $table->unsignedbigInteger('activity_id');
            $table->string('notes',200);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('emp_in_activity');
    }
}
