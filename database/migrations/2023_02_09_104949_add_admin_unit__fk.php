<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddAdminUnitFk extends Migration
{
    public function up()
    {
        Schema::table('admin_unit', function (Blueprint $table) {
            DB::statement("ALTER TABLE admin_unit
                                 ADD FOREIGN KEY  (company_id)
                                            REFERENCES company(id);");
            DB::statement("ALTER TABLE admin_unit
                                     ADD  FOREIGN KEY (admin_unit_typ_id)
                                            REFERENCES admin_unit_typ(id);");
        });
    }

    public function down()
    {
        Schema::table('admin_unit', function (Blueprint $table) {
            //
        });
    }
}
