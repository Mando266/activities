<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddFundingSourcesFk extends Migration
{
    public function up()
    {
        Schema::table('funding_sources', function (Blueprint $table) {
            DB::statement("ALTER TABLE funding_sources
                                    ADD FOREIGN KEY  (company_id)
                                             REFERENCES company(id);");
        });
    }

    public function down()
    {
        Schema::table('funding_sources', function (Blueprint $table) {
            //
        });
    }
}
