<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddActivityFk extends Migration
{
    public function up()
    {
        Schema::table('activity', function (Blueprint $table) {
          
            DB::statement("ALTER TABLE activity
                                            ADD FOREIGN KEY  (executive_target_id)
                                                       REFERENCES executive_target(id);");
                   });
    }

    public function down()
    {
        Schema::table('activity', function (Blueprint $table) {
            //
        });
    }
}
