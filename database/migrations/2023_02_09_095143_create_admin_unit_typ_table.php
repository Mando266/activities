<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminUnitTypTable extends Migration
{
    public function up()
    {
        Schema::create('admin_unit_typ', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('admin_unit_typ_name',300);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('admin_unit_typ');
    }
}
