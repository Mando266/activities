<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\EexecutiveTargetCompView ;
use Illuminate\Support\Facades\Auth;
use App\Models\Master\ExecutiveTarget;
use App\Models\Master\StrategicPlan;
use App\Models\Master\FundingSources;
use App\Models\Master\PlanYearMonthWeeksView;
use App\Models\Master\PlanImplWeeks;
use DB;

class EexecutiveTargetController extends Controller
{
    public function index()
    {
        $this->authorize(__FUNCTION__,EexecutiveTargetCompView::class);
        if(Auth::user()->is_super_admin || is_null(Auth::user()->company_id)){
            $executivetargetcompview = EexecutiveTargetCompView::
            where('company_id',Auth::user()
            ->company_id)->
            orderBy('id')
          ->paginate(10);


        }else
        {
            $executivetargetcompview = executivetargetcompview::
            where('company_id',Auth::user()->company_id)
                                               ->paginate(10);


        }
        return view('master.executivetarget.index',[
            'executivetargetcompview'=>$executivetargetcompview,


      ]);

    }

    public function create()
    {
        $this->authorize(__FUNCTION__,ExecutiveTarget::class);

        $strategicplan = DB::table('strategic_plan')->select('id', 'strategic_plan_name')->distinct('id')
       ->where('company_id',Auth::user()->company_id) ->get();

       $targetexecutive = DB::table('strategic_target_executive_view')->select('id', 'strategic_target_name', 'target_from', 'target_to')->distinct('id')
       ->where('company_id',Auth::user()->company_id)
       ->get();
       // dd($targetexecutive);

        return view('master.executivetarget.create',[
            'strategicplan'=>$strategicplan,
            'targetexecutive'=>$targetexecutive,


            ]);

    }


    public function store(Request $request)
    {

      $request->validate([

        'target_from' => 'required',
        'target_to' => ['required', 'after_or_equal:target_from'],


    ],[
       'target_to.after_or_equal' => ' يجب تاريخ نهاية الخطة اكبر من تاريخ بداية الخطة',
       ]);

        $executivetarget = ExecutiveTarget::create([



            'strategic_plan_id'=> $request->input('strategic_plan_id'),
            'executive_target_name'=> $request->input('executive_target_name'),
            'target_from'=> $request->input('target_from'),
            'target_to'=> $request->input('target_to'),
            'target_estimated_budget'=> $request->input('target_estimated_budget'),
            'strategic_target_id' => $request->input('strategic_target_id'),



]);

        Auth::user()->clearCache();
      //  return response()->JSON(['message'=>$message,],200);
       return redirect()->route('executivetarget.index' ,['executivetarget'=> $executivetarget])->with('success',trans('executivetarget.created'));
    }



    public function edit(ExecutiveTarget $executivetarget)
    {

        $strategicplan = DB::table('strategic_plan')->select('id', 'strategic_plan_name')->distinct('id')
        ->where('company_id',Auth::user()->company_id)  ->get();
        $targetexecutive = DB::table('strategic_target_executive_view')->select('id', 'strategic_target_name', 'target_from', 'target_to')->distinct('id')
       ->where('company_id',Auth::user()->company_id) ->get();
        $this->authorize(__FUNCTION__,ExecutiveTarget::class);

        return view('master.executivetarget.edit',[
            'executivetarget'=>$executivetarget,
            'strategicplan'=>$strategicplan,
            'targetexecutive'=>$targetexecutive,

        ]);
    }


    public function update(Request $request, ExecutiveTarget $executivetarget)
    {


        $this->authorize(__FUNCTION__,ExecutiveTarget::class);
    $executivetarget->update($request->except('_token'));
    return redirect()->route('executivetarget.index')->with('success',trans('executivetarget.updated'));
    }


    public function destroy($id)
    {
        $executivetarget =ExecutiveTarget::Find($id);
        $executivetarget->delete();

        return redirect()->route('executivetarget.index')->with('success',trans('executivetarget.deleted'));
    }
    public function plan(Request $request)
    {
        $planyear = PlanYearMonthWeeksView ::first();
     //   $notbooksNo = $request->notbook_no;
       // $notbooksNo1 = $request->book_pages;
        $year = $planyear->year;
     //   $MONTH_NO = $planyear->MONTH_NO;
        $MONTH = 1;

        $v_month=0;
        $v_week=0;
        $week=1;
      //  dd( $year);
        //// new procedure to insert data in table 29/01/2023
        /// to insert no of books in table
        for ($i = 0; $i < $planyear->count_year; $i++) {
          $v_year=2020;
            for ($v_month= 0; $v_month <$planyear->month12 && $year=$year++  ;   $v_month++) {
              for ($v_week= 0; $v_week <4  ;   $v_week++) {
                $planimplweeks = PlanImplWeeks::create([
                    'year'                            => $year,
                   'month_no'                    => $MONTH,
                  //  'book_pages'                 => $noPages,
                    'strategic_plan_id'            => $planyear->strategic_plan_id,
                  //  'admin_unit_id'              =>  $adminUnit->id,
                    'plan_week_no'                =>$week,
                  //  'serl_from_after_transaction' => $request->input('serl_from'),
                  //  'notbook_add_date'           => $request->input('move_date')
                ]);

                $week=  $week + 1;
              }

                $MONTH=  $MONTH + 1;
            }

            $year = $year + 1;
          }
               // Auth::user()->clearCache();
               return response()->json($planimplweeks, 201);


    }


    public function ss(Request $request)
    {
        $planyear = PlanYearMonthWeeksView ::first();
     //   $notbooksNo = $request->notbook_no;
       // $notbooksNo1 = $request->book_pages;
        $year = $planyear->year;
     //   $MONTH_NO = $planyear->MONTH_NO;
     $month = 1;
     $year  = 2017;
     $count = 12;

     $week = 1;
      //  dd( $year);
        //// new procedure to insert data in table 29/01/2023
        /// to insert no of books in table
        for ($i = 0; $i <= $count; $i++) {
          $week++;
          if ( $month > 12  )
            {

                $planimplweeks = PlanImplWeeks::create([
                    'year'                            => $year,
              'month_no'                    => $month,
                  //  'book_pages'                 => $noPages,
                    'strategic_plan_id'            => $planyear->strategic_plan_id,
                  //  'admin_unit_id'              =>  $adminUnit->id,
                    'plan_week_no'                =>$week,
                  //  'serl_from_after_transaction' => $request->input('serl_from'),
                  //  'notbook_add_date'           => $request->input('move_date')
                ]);

                $month = $month+1;
              $week = $week+1;
          //  $year++;

            }

         // $year = $year + 1;
          }
               // Auth::user()->clearCache();
               return response()->json($planimplweeks, 201);


    }

    public function bb(Request $request)
    {
        $planyear = PlanYearMonthWeeksView ::first();
     //   $notbooksNo = $request->notbook_no;
       // $notbooksNo1 = $request->book_pages;
        $year = $planyear->year;
     //   $MONTH_NO = $planyear->MONTH_NO;
     $month = 11;
     $year  = 2017;
     $count = 15;
        $week=1;
      //  dd( $year);
        //// new procedure to insert data in table 29/01/2023
        /// to insert no of books in table
        for ($i = 1; $i <= $count; $i++)  {
          $month++;
          if ($month > 12) {

                $planimplweeks = PlanImplWeeks::create([
                    'year'                            => $year,
                   'month_no'                    => $month,
                  //  'book_pages'                 => $noPages,
                  //  'strategic_plan_id'            => $planyear->strategic_plan_id,
                  //  'admin_unit_id'              =>  $adminUnit->id,
                    'plan_week_no'                =>$week,
                  //  'serl_from_after_transaction' => $request->input('serl_from'),
                  //  'notbook_add_date'           => $request->input('move_date')
                ]);



            }

            $month = 1;
            $year++;
          }
               // Auth::user()->clearCache();
               return response()->json($planimplweeks, 201);




    }

}
