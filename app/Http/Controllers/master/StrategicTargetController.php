<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

use App\Models\Master\StrategicTargetCompView;
use App\Models\Master\StrategicTarget;

class StrategicTargetController extends Controller
{
    public function index(){
        $this->authorize(__FUNCTION__,StrategicTargetCompView::class);
        $strategictarget = StrategicTargetCompView::where('company_id',Auth::user()
        ->company_id)->orderBy('company_id')->orderBy('id')->paginate(30);
        return view('master.strategictarget.index',[
            'items'=>$strategictarget
        ]);
    }
    public function show(StrategicTarget $strategictarget){
        $this->authorize(__FUNCTION__,StrategicTarget::class);
      
        return view('master.strategictarget.show',[
            'strategictarget'=>$strategictarget,
        ]);
    }

    public function create()
    {
        $this->authorize(__FUNCTION__,StrategicTarget::class);
        $strategicplan = DB::table('strategic_plan')->select('id', 'start_plan_date', 'end_plan_date' )->distinct('id')
        ->where('company_id',Auth::user()->company_id)->get();
        return view('master.strategictarget.create',['strategicplan'=>$strategicplan,]);
    }

    public function store(Request $request){
       
      
        $this->authorize(__FUNCTION__,StrategicTarget::class);
        $request->validate([
            'target_from' => 'required',
            'target_to' => ['required', 'after_or_equal:target_from'],
           

        ],[
            'target_to.after_or_equal' => ' يجب ادخال تاريخ    المخطط الزمنى الى اكبر من المخطط الزمنى من  ',
        

        ]);
        StrategicTarget::create($request->except('_token'));
        return redirect()->route('strategictarget.index')->with('success',trans('strategictarget.created'));
    }

    public function edit(StrategicTarget $strategictarget){
        $this->authorize(__FUNCTION__,StrategicTarget::class);
        $strategicplan = DB::table('strategic_plan')->select('id', 'start_plan_date', 'end_plan_date')->distinct('id')
        ->where('company_id',Auth::user()->company_id)->get();
        return view('master.strategictarget.edit',[
            'strategictarget'=>$strategictarget,
            'strategicplan'=>$strategicplan
        ]);
    }

    public function update(Request $request,StrategicTarget $strategictarget){
        $this->authorize(__FUNCTION__,StrategicTarget::class);
        $strategictarget->update($request->except('_token'));
        return redirect()->route('strategictarget.index')->with('success',trans('strategictarget.updated'));
    }

    public function destroy($id)
    {

        $strategictarget =StrategicTarget::Find($id);
        $strategictarget->delete();

        return redirect()->route('strategictarget.index')->with('success',trans('strategictarget.deleted.success'));

    }
}
