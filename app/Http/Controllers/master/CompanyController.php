<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Company;

use Illuminate\Http\Request;


use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{
    public function index(){
        $this->authorize(__FUNCTION__,Company::class);
        $companies = Company::orderBy('id')->paginate(30);
        return view('master.company.index',[
            'items'=>$companies
        ]);
    }
    public function show(Company $company){
        $this->authorize(__FUNCTION__,Company::class);
      
        return view('master.company.show',[
            'company'=>$company,
        ]);
    }

    public function create()
    {
        $this->authorize(__FUNCTION__,Company::class);
        return view('master.company.create');
    }

    public function store(Request $request){
        $request->validate([
            'telephone' => ['required', new ValidTicket],
        ]);
      
        $this->authorize(__FUNCTION__,Company::class);
        Company::create($request->except('_token'));
        return redirect()->route('company.index')->with('success',trans('company.created'));
    }

    public function edit(Company $company){
        $this->authorize(__FUNCTION__,Company::class);
        return view('master.company.edit',[
            'company'=>$company
        ]);
    }

    public function update(Request $request,Company $company){
        $this->authorize(__FUNCTION__,Company::class);
        $company->update($request->except('_token'));
        return redirect()->route('company.index')->with('success',trans('company.updated'));
    }

    public function destroy($id)
    {

        $company =Company::Find($id);
        $company->delete();

        return redirect()->route('company.index')->with('success',trans('company.deleted.success'));

    }

}
