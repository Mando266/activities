<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\DropAdminInCompView;
use App\Models\Master\AdminInActivity;
use Illuminate\Support\Facades\Auth;

use DB;

class AdminInCompController extends Controller
{

   public function index()
   {
       $this->authorize(__FUNCTION__,DropAdminInCompView::class);
       $activity_id = request()->input('activity_id');
       $dropadminincompview=DropAdminInCompView::where('activity_id',$activity_id)
       ->where('company_id',Auth::user()
       ->company_id)
       ->paginate(10);

       return view('master.admininactivity.index',['dropadminincompview'=>$dropadminincompview]);
   }


   public function create()
   {
       $this->authorize(__FUNCTION__,AdminInActivity::class);
      // $planimplweeks = PlanImplWeeks::get();
       $dropadminincompview = DB::table('admin_unit')->select('id', 'name')->distinct('id')
       ->where('company_id',Auth::user()->company_id)->get();

       return view('master.admininactivity.create',['dropadminincompview'=>$dropadminincompview,]);
   }


   public function store(Request $request)
   {
       $activity_id = request()->input('activity_id');
       $this->authorize(__FUNCTION__, AdminInActivity::class);
       $admininactivity= AdminInActivity::create($request->except('_token'));
       Auth::user()->clearCache();
       return redirect()->route('admininactivity.index', ['activity_id'=> $admininactivity->activity_id,])->with('success', trans('admininactivity.created'));
   }


   public function edit(AdminInActivity $admininactivity)
   {
       $this->authorize(__FUNCTION__, AdminInActivity::class);
       $dropadminincompview = DB::table('admin_unit')->select('id', 'name')->distinct('id')
       ->where('company_id',Auth::user()->company_id)->get();

       return view('master.admininactivity.edit', ['dropadminincompview'=> $dropadminincompview, 'admininactivity'=> $admininactivity,]);
   }

   public function update(Request $request, AdminInActivity $admininactivity)

   {
       $this->authorize(__FUNCTION__, AdminInActivity::class);
       $admininactivity->update($request->except('_token'));
       return redirect()->route('admininactivity.index',['activity_id'=> $admininactivity->activity_id])->with('success', trans('admininactivity.updated'));
   }

   public function destroy($id)
   {
       $admininactivity = AdminInActivity::Find($id);
       $admininactivity->delete();
       return redirect()->route('admininactivity.index',['activity_id'=> $admininactivity->activity_id])->with('success', trans('admininactivity.deleted'));
   }
}
