<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\ExecutiveTargetCompView;
use App\Models\Master\TargetOutput;
use App\Models\Master\TargetOutputExecutiveCompView;
use Illuminate\Support\Facades\Auth;
use DB;

class TargetOutputController extends Controller
{
    public function index()
    {
       
        $this->authorize(__FUNCTION__, ExecutiveTargetCompView::class);
        $executive_target_id = request()->input('executive_target_id'); //20143;
        //dd($maniest_id);
        $targetoutputexecutivecompview = TargetOutputExecutiveCompView::where('executive_target_id', $executive_target_id)
       // ->where('company_id',Auth::user()
         //     ->company_id)
        ->get();

        
        // dd($postponedcatmanifesttofareview);

        return view('master.targetoutput.index', ['targetoutputexecutivecompview' => $targetoutputexecutivecompview,]);
    }


    public function create()
    {
        $this->authorize(__FUNCTION__, TargetOutput::class);
       

        return view('master.targetoutput.create', );
    }



    public function store(Request $request)
    {


        foreach ($request->input('targetoutput', []) as $targetoutput) {
            TargetOutput::create([
                'executive_target_id'      => $request->input('executive_target_id'),
                'target_output_name'   => $targetoutput['target_output_name'],
              
            ]);
        }
        return redirect()->route('targetoutput.index', ['executive_target_id' => request()->input('id'), 'executive_target_id' => request()->input('manifest_id')])->with('success', trans('postponedcatmanifest.created'));
    }



    public function edit(TargetOutput $targetoutput)
    {
        $this->authorize(__FUNCTION__, TargetOutput::class);

      


        return view('master.targetoutput.edit', ['targetoutput' => $targetoutput,]);
    }




    public function update(Request $request, TargetOutput $targetoutput)
    {
        $this->authorize(__FUNCTION__, TargetOutput::class);
        $targetoutput->update($request->except('_token'));
        Auth::user()->clearCache();

        return redirect()->route('targetoutput.index', ['executive_target_id' => $targetoutput->executive_target_id,])
            ->with('success', trans('targetoutput.updated'));
    }



    public function destroy($id)
    {
        $targetoutput = TargetOutput::Find($id);
        $targetoutput->delete();
        return redirect()->route('targetoutput.index', ['executive_target_id' => $targetoutput->executive_target_id])->with('success', trans('targetoutput.deleted'));
    }
}
