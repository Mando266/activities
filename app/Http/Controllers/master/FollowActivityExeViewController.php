<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\MonthWeek;
use App\Models\Master\ExecutiveTarget;
use App\Models\Master\StrategicPlan;
use App\Models\Master\StrategicTarget;
use App\Models\Master\FollowActivityExeView;
use Illuminate\Support\Facades\Auth;
use DB;

//تقرير متابعة الاهداف والانشطة الجارية/التى انتهت/المتأخرة
class FollowActivityExeViewController extends Controller
{
    public function index()
    {
        $strategic_plan_id      = request()->input('strategic_plan_id');
        $start_plan_date        = request()->input('start_plan_date');
        $end_plan_date          = request()->input('end_plan_date');
        $company_id             = request()->input('company_id');
        $strategic_target_id    =request()->input('strategic_target_id');
        $activity_status        = request()->input('activity_status');
        $strategicplan = DB::table('strategic_plan')->select('id', 'strategic_plan_name')->distinct('id')
        ->where('id', $strategic_plan_id)
        ->first();
        $strategictarget = DB::table('strategic_target')->select('id', 'strategic_target_name')->distinct('id')
        ->where('id', $strategic_target_id)
        ->first();
//dd($strategictarget);

      
        $follow = FollowActivityExeView::
        
            where('strategic_plan_id', $strategic_plan_id)
            ->where('start_plan_date', $start_plan_date)
            ->where('end_plan_date', $end_plan_date)
            ->where('company_id', $company_id)
            ->where('strategic_target_id', $strategic_target_id)
            ->where('activity_status', $activity_status)
          ->paginate(15);
           
       
        return view('master.follow.index', [
            'strategicplan' => $strategicplan,
            'follow' => $follow,
            'strategictarget' => $strategictarget,
            

        ]);
}
}