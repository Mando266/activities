<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\ActivityFollowUpView;
use App\Models\Master\ActivityFollowUp;
use App\Models\Master\PlanImplWeeks;
use App\Models\Master\PlanImplWeeksCompView;
use App\Models\Master\StatusActivEndView;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use DB;

class ActivityFollowUpController extends Controller
{
    ///  متابعة التنفيذ العفلى للنشاط
    public function index()
    {
        $this->authorize(__FUNCTION__,ActivityFollowUpView::class);
        $activity_id = request()->input('activity_id');
        $activity_name = DB::table('activity_follow_up_view')
        ->where('activity_id',$activity_id)->first();
        //dd($activity_name);
      //  $activity_name = request()->input('activity_name');
        $planned_from = DB::table('activity_follow_up_view')
        ->where('activity_id',$activity_id)->first();
        $planned_to = DB::table('activity_follow_up_view')
        ->where('activity_id',$activity_id)->first();
        // dd($planned_from);
        $activityfollowupview=ActivityFollowUpView::where('activity_id',$activity_id)
        ->whereBetween('start_date_week', [$planned_from->planned_from, $planned_to->planned_to])
        ->paginate(10);
// dd($activityfollowupview);


//dd($activityfollowupview);

        return view('master.activityfollowup.index',['activityfollowupview'=>$activityfollowupview,'activity_name'=>$activity_name,'planned_to'=>$planned_to,'planned_from'=>$planned_from]);
    }


    public function create()
    {
        $this->authorize(__FUNCTION__,ActivityFollowUp::class);
       // $planimplweeks = PlanImplWeeks::get();
        $planimplweeks = DB::table('plan_impl_weeks_comp_view')->select('id', 'plan_week_no','month_no','year')->distinct('id')
        ->where('company_id',Auth::user()->company_id)->get();
         // dd($planimplweeks);
        return view('master.activityfollowup.create',['planimplweeks'=>$planimplweeks,]);
    }


    public function store(Request $request)
    {
        $activity_id = request()->input('activity_id');

        $this->authorize(__FUNCTION__, ActivityFollowUp::class);
        $activityfollowup= ActivityFollowUp::create($request->except('_token'));
        Auth::user()->clearCache();
     //   return redirect()->route('activityfollowup.index', ['activity_id'=> $activity_id->activity_id,])->with('success', trans('activityfollowup.created'));
        return redirect()->route('activityfollowup.index', ['activity_id' => request()->input('activity_id'),'activity_name' => request()->input('activity_name'),'planned_from' => request()->input('planned_from'),'planned_to' => request()->input('planned_to')])->with('success', trans('activityfollowup.created'));
    }


    public function edit(ActivityFollowUp $activityfollowup)
    {
        $this->authorize(__FUNCTION__, ActivityFollowUp::class);
        $planimplweeks = DB::table('plan_impl_weeks_comp_view')->select('id', 'plan_week_no','month_no','year')->distinct('id')
        ->where('company_id',Auth::user()->company_id)->get();

        return view('master.activityfollowup.edit', ['planimplweeks'=> $planimplweeks,'activityfollowup'=> $activityfollowup,]);
        
    }

    public function update(Request $request, ActivityFollowUp $activityfollowup)

    {
        $this->authorize(__FUNCTION__, ActivityFollowUp::class);
     
        $activityfollowup->update($request->except('_token'));
        return redirect()->route('activityfollowup.index', ['activity_id' => $activityfollowup->activity_id])->with('success', trans('activityfollowup.updated'));
    }

    public function destroy($id)
    {
        $activityfollowup = ActivityFollowUp::Find($id);
        $activityfollowup->delete();
        return redirect()->route('activityfollowup.index',['activity_id'=>$activityfollowup->activity_id])->with('success', trans('activityfollowup.deleted'));
    }


    public function followupdate($activity_id , $planned_from,$planned_to)
    {
       
      
            $activityfollowup=StatusActivEndView::
            where('activity_id', $activity_id)
           // ->where('planned_from','=', $planned_from)
           // ->where('planned_to','=', $planned_to)
            ->whereBetween('start_date_week', [$planned_from, $planned_to])
            ->where('id','<' , function($query ) use ($activity_id) {
                                $query->select('id')
                                ->from(with(new StatusActivEndView)->getTable())
                                ->where('activity_id',$activity_id)
                                ->where('activity_status', 2);  })
            //->select('activity_status')
            ->orderBy('id')
            ->get();
        //     $act=ActivityFollowUp::get();
        //   foreach($activityfollowup as $activityfollowups )
        //         $act->update(['activity_status'=> '2' 
        //             ->where('id','=', $activityfollowups->id)
        //             ]);
            foreach($activityfollowup as $activityfollowups ){
                    ActivityFollowUp::where('id','=', $activityfollowups->id)
                                ->update(['activity_status'=> '2' ]);
            }


      
                return response()->json([
                  //  $act,
                   "message" => "تم تعديل علي موقف النشاط انتهي"
                ], 200);
              
          }

          public function activityend()
    {
        
      
        $activityfollowup=StatusActivEndView::
           // where('activity_id', $activity_id)
         //  ->
          where('start_date_week','>=', 'planned_from')
          ->where('start_date_week','<=','planned_to')
       //  whereBetween('start_date_week', [$planned_from, $planned_to])
           ->
            where('id','<' , function($query )  {
                                $query->select('id')
                                ->from(with(new StatusActivEndView)->getTable())
                              //  ->where('activity_id',$activity_id)
                                ->where('activity_status', 2);  })
            //->select('activity_status')
            ->orderBy('id')
            ->get();
        //     $act=ActivityFollowUp::get();
        //   foreach($activityfollowup as $activityfollowups )
        //         $act->update(['activity_status'=> '2' 
        //             ->where('id','=', $activityfollowups->id)
        //             ]);
           foreach($activityfollowup as $activityfollowups ){
                    ActivityFollowUp::
                    //where('id','=', $activityfollowups->id)
                         //       ->
                                update(['activity_status'=> '2' ]);
            }


      
                return response()->json([
                  //  $act,
                  "message" => "تم تعديل علي موقف النشاط انتهي"
                ], 200);
              
          }
          
}
