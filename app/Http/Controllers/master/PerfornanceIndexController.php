<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\PerfornanceIndex;

class PerfornanceIndexController extends Controller
{
    public function index(){
        $this->authorize(__FUNCTION__,PerfornanceIndex::class);
        $perfornanceindex = PerfornanceIndex::
            
            orderBy('id')->paginate(30);
        return view('master.perfornanceindex.index',[
            'perfornanceindex'=>$perfornanceindex
        ]);
    }
    public function show(PerfornanceIndex $perfornanceindex){
        $this->authorize(__FUNCTION__,PerfornanceIndex::class);
      
        return view('master.perfornanceindex.show',[
            'perfornanceindex'=>$perfornanceindex,
        ]);
    }

    public function create()
    {
        $this->authorize(__FUNCTION__,PerfornanceIndex::class);
        return view('master.perfornanceindex.create');
    }

    public function store(Request $request){
       
      
        $this->authorize(__FUNCTION__,PerfornanceIndex::class);
        PerfornanceIndex::create($request->except('_token'));
        return redirect()->route('perfornanceindex.index')->with('success',trans('perfornanceindex.created'));
    }

    public function edit(PerfornanceIndex $perfornanceindex){
        $this->authorize(__FUNCTION__,PerfornanceIndex::class);
        return view('master.perfornanceindex.edit',[
            'perfornanceindex'=>$perfornanceindex
        ]);
    }

    public function update(Request $request,PerfornanceIndex $perfornanceindex){
        $this->authorize(__FUNCTION__,PerfornanceIndex::class);
        $perfornanceindex->update($request->except('_token'));
        return redirect()->route('perfornanceindex.index')->with('success',trans('perfornanceindex.updated'));
    }

    public function destroy($id)
    {

        $perfornanceindex =PerfornanceIndex::Find($id);
        $perfornanceindex->delete();

        return redirect()->route('perfornanceindex.index')->with('success',trans('perfornanceindex.deleted.success'));

    }

}
