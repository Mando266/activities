<?php

namespace App\Http\Controllers\Master;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Master\AdminUnit;
use App\models\Master\Company;

use App\Models\Master\AdminUnitTyp;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class AdminUnitController extends Controller
{   ///dalia at 21/2/2021   كان موجود فى البرجيكت القديم وانا حطيته هنا وعدلت فيه وفقا لنظام الفيو
        public function index()
        {
          $this->authorize(__FUNCTION__,AdminUnit::class);
          if(Auth::user()->is_super_admin || is_null(Auth::user()->company_id)){
              $adminUnit = AdminUnit::where('company_id',Auth::user()
              ->company_id)->
              orderBy('id')->paginate(10);
              return view('master.adminunit.index',['adminUnit'=>$adminUnit]);
  
          }else
          {
              $adminUnit = AdminUnit::where('company_id',Auth::user()
              ->company_id)->orderBy('company_id')->paginate(10);
              return view('master.adminunit.index',['adminUnit'=>$adminUnit]);
        }
      }

        public function create()
        {
          $this->authorize(__FUNCTION__,AdminUnit::class);
          if(Auth::user()->is_super_admin || is_null(Auth::user()->company_id)){
          $company = Company::all();
         $adminunitTyp = AdminUnitTyp::all();
         // $adminUnit = AdminUnit::get();
        }else{
          $company = Company::all();
          $adminunitTyp = AdminUnitTyp::all();
        //  $adminUnit = AdminUnit::where('company_id',Auth::user()
        //  ->company_id)->get();
         
        }
          return view('master.adminunit.create',['company'=>$company,
         'adminunitTyp'=>$adminunitTyp,    ]);
        }

        public function store(Request $request){
          $user = Auth::user();

          $admin = AdminUnit::create([
            'company_id'=> $user->company_id,
            'admin_unit_typ_id'=> $request->input('admin_unit_typ_id'),
            'code'=> $request->input('code'),
            'name'=> $request->input('name'),
            'address'=> $request->input('address'),
            'tel'=> $request->input('tel'),
          
            'lat'=> $request->input('lat'),
            'lang'=> $request->input('lang'),
          ]);

          Auth::user()->clearCache();
          return redirect()->route('adminunit.index')->with('success',trans('adminunit.created'));

        }

           ////////////////////////////////////

        // public function show($id)
        // {

        //    $this->authorize(__FUNCTION__,AdminUnit::class);
        //     $adminunit = AdminUnit::Find($id);
        //     return view('master.adminunit.index',['adminunit'=>$adminunit,]);
        //    // return response()->JSON(['adminunits' => $adminunits],200);
        // }
        ///////////////////////////////////////
        public function edit(AdminUnit $adminunit)
        {
          $this->authorize(__FUNCTION__,AdminUnit::class);
          if(Auth::user()->is_super_admin || is_null(Auth::user()->company_id)){
          $company = Company::all();
         
          $admin = AdminUnit::all();
          $adminunitTyp = AdminUnitTyp::all();
        }else{
          $company = Company::all();
        
          $admin = AdminUnit::where('company_id',Auth::user()
          ->company_id)->get();
          $adminunitTyp = AdminUnitTyp::all();
        }
          return view('master.adminunit.edit',['adminunit'=>$adminunit,
         'adminunitTyp'=>$adminunitTyp,]);
        }

        public function update(Request $request, AdminUnit $adminunit){
            $this->authorize(__FUNCTION__,AdminUnit::class);
            $adminunit->update($request->except('_token'));
            return redirect()->route('adminunit.index')->with('success',trans('adminunit.updated'));
        }



        /////////////////////////////////////////

        public function destroy(AdminUnit $adminunit)
        {
          $adminunit->delete();
            Auth::user()->clearCache();
            return redirect()->route('adminunit.index')->with('success',trans('adminunit.deleted'));
        }
        ///////////////////////////////////////////

        // public function adminUnitChildren()
      //  {
        //    $adminunits = AdminUnit::all();
      //      return response()->JSON(['adminunits' => $adminunits],200);
    //    }
    }
