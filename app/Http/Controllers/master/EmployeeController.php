<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\Employee;
use App\Models\Master\EmpAdminComp;
use App\Models\Master\AdminUnit;
use Illuminate\Support\Facades\Auth;
use App\Filters\Emp\EmpIndexFilter;
use DB  ;

class EmployeeController extends Controller
{
    public function index()
    {

        $this->authorize(__FUNCTION__,EmpAdminComp::class);
       
        if(Auth::user()->is_super_admin || is_null(Auth::user()->company_id)){
            $employe   = EmpAdminComp:: where('company_id',Auth::user()
            ->company_id)->get();
           
          
        }else
        {
            $employe        = EmpAdminComp::where('company_id',Auth::user()->company_id)->get();
           

        }
       // dd($employe);
         return view('master.employee.index',[ 'employe'=>$employe,]);
     // return response()->JSON(['employe' => $employe],200);
    }

    public function create()
    {
        $this->authorize(__FUNCTION__,Employee::class);
        if(Auth::user()->is_super_admin || is_null(Auth::user()->company_id)){
            $adminunit  = AdminUnit::where('company_id',Auth::user()->company_id)->get();
          
        }else{

        $adminunit  = AdminUnit::where('company_id',Auth::user()->company_id)->get();
       
        }
            return view('master.employee.create',['adminunit'=>$adminunit, ]);

}

    public function store(Request $request)
    {   
        $user = Auth::user();
        $code = request()->input('code');
        $empCodeDublicate  = Employee::where('company_id',$user->company_id)->where('code',$code)->first();

        if($empCodeDublicate != null){
            return back()->with('alert', 'كود الموظف موجود بالفعل');
        }
        Employee::create([
                        'company_id'    =>$user->company_id,
                        'admin_unit_id' =>$request->input('admin_unit_id'),
                        'name'          =>$request->input('name'),
                        'address'       =>$request->input('address'),
                        'is_active'     =>$request->input('is_active'),
                        'nat_id'        =>$request->input('nat_id'),
                    
                        'email'         =>$request->input('email'),
                        'code'          =>$request->input('code'),
                        'job'           =>$request->input('job')]);
        Auth::user()->clearCache();
        return redirect()->route('employee.index')->with('success',trans('Employee.created'));
       }

    public function show($id)
    {
        //
    }

    public function edit(Employee $employee)
    {
        $this->authorize(__FUNCTION__,Employee::class);
        if(Auth::user()->is_super_admin || is_null(Auth::user()->company_id)){
            
            $adminunit  = AdminUnit::all();
         
        }else{
            $adminunit  = AdminUnit::where('company_id',Auth::user()->company_id)->get();
        
        }
        return view('master.employee.edit',['adminunit'=>$adminunit,
                                             'employee'=>$employee,
                                             ]);
      }

    public function update(Request $request,Employee $employee)
    {
        $this->authorize(__FUNCTION__,Employee::class);
        $employee->update($request->except('_token'));
        return redirect()->route('employee.index')->with('success',trans('Employee.updated'));   
    }

    public function destroy($id)
    {
        $employee =Employee::Find($id);
        $employee->delete();

        return redirect()->route('employee.index')->with('success',trans('Employee.deleted'));
    }
}
