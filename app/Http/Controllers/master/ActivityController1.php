<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\ActivityFollowExecutiveView ;
use Illuminate\Support\Facades\Auth;
use App\Models\Master\AddFollowActiveViewLast;
use App\Models\Master\Activity;
use App\Models\Master\AddFollowActiveView;

use App\Models\Master\test;
use DB;


class ActivityController extends Controller
{
    public function index()
    {
        $this->authorize(__FUNCTION__,ActivityFollowExecutiveView::class);
        $executive_target_id = request()->input('executive_target_id'); //20143;
        if(Auth::user()->is_super_admin || is_null(Auth::user()->company_id)){
            $activityfollowexecutiveiew = ActivityFollowExecutiveView::
            where('company_id',Auth::user()
            ->company_id)->orderBy('company_id')->
                                
                                   orderBy('id')
                                    ->paginate(10);
            //dd($activityfollowexecutiveiew);

        }else
        {
            $activityfollowexecutiveiew = ActivityFollowExecutiveView::
            where('company_id',Auth::user()->company_id)->
                                               paginate(10);
            //dd($activityfollowexecutiveiew);

        }
        return view('master.activity.index',[
            'activityfollowexecutiveiew'=>$activityfollowexecutiveiew,
        ]);
    }

    public function create()
    {
        $this->authorize(__FUNCTION__,Activity::class);


        $executive = DB::table('executive_target_name_view')->select('id', 'executive_target_name','target_from', 'target_to')->distinct('id')
        ->where('company_id',Auth::user()
        ->company_id)->get();
        return view('master.activity.create',['executive'=> $executive]);
    }


    public function store(Request $request)
    {
       // $strategic_plan_id      = request()->input('strategic_plan_id');
        $request->validate([
            'planned_from' => 'required',
            'planned_to' => ['required', 'after_or_equal:planned_from'],
            // 'actual_from' => 'required',
            // 'actual_to' => ['required', 'after_or_equal:actual_from'],

        ],[
            'planned_to.after_or_equal' => 'يجب ادخال فتره التفيذ  الفعلية الى اكبر من من',
            // 'actual_to.after_or_equal' => 'يجب ادخال فتره التفيذ  الفعلية الى اكبر من من',

        ]);
        $request->validate([

            'actual_from' => 'required',
            'actual_to' => ['required', 'after_or_equal:actual_from'],

        ], [

            'actual_to.after_or_equal' => 'يجب ادخال فتره التفيذ  الفعلية الى اكبر من من',

        ]);
       // $addfollow= AddFollowActiveViewLast ::whereBetween('start_date_week', [$request->input('planned_from'), $request->input('planned_to')]) ->get();
        $addfollow= AddFollowActiveViewLast ::all();
      
//dd($addfollow);
foreach ( $addfollow  as $addfollows){
        DB::statement("insert into  activity_follow_up (activity_id,plan_impl_weeks_id,activity_status)
        values  ($addfollows->activity_id
       ,$addfollows->plan_impl_weeks_id,$addfollows->activity_status)");
}

        $user = Auth::user();
        $activity = Activity::create($request->input());
        //$activity->company_id = $user->company_id;
        $activity->save();
        Auth::user()->clearCache();
        return redirect()->route('activity.index')->with('success',trans('activity.created'));
    }



    public function edit(Activity $activity)
    {
        $this->authorize(__FUNCTION__,Activity::class);
        $executive = DB::table('executive_target_name_view')->select('id','executive_target_name', 'target_from', 'target_to')->distinct('id')
        ->where('company_id',Auth::user()
        ->company_id)->get();
        return view('master.activity.edit',[
            'executive'=> $executive, 'activity' => $activity,

        ]);
    }


    public function update(Request $request, Activity $activity)
    {


        $this->authorize(__FUNCTION__,Activity::class);
    $activity->update($request->except('_token'));
    return redirect()->route('activity.index')->with('success',trans('activity.updated'));
    }


    public function destroy($id)
    {
        $activity =Activity::Find($id);
        $activity->delete();

        return redirect()->route('activity.index')->with('success',trans('activity.deleted'));
    }
}
