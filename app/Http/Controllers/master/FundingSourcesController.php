<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\FundingSourcesCompView;
use App\Models\Master\FundingSources;

use Illuminate\Support\Facades\Auth;
use DB;

class FundingSourcesController extends Controller
{
    public function index()
    {
       
        $this->authorize(__FUNCTION__, FundingSourcesCompView::class);
        $executive_target_id = request()->input('executive_target_id'); //20143;
        //dd($maniest_id);
        $fundingsourcescompview = FundingSourcesCompView::where('executive_target_id', $executive_target_id)
        ->where('company_id',Auth::user()
              ->company_id)
        ->get();
        // dd($postponedcatmanifesttofareview);

        return view('master.fundingsources.index', ['fundingsourcescompview' => $fundingsourcescompview,]);
    }


    public function create()
    {
        $this->authorize(__FUNCTION__, FundingSources::class);
       

        return view('master.fundingsources.create', );
    }



    public function store(Request $request)
    {

//dd($request->input());
        foreach ($request->input('fundingsources', []) as $fundingsources) {
            FundingSources::create([
                'executive_target_id'      => $request->input('executive_target_id'),
                'funding_entity'   => $fundingsources['funding_entity'],
                'funding_value' => $fundingsources['funding_value'],
                'company_id' => Auth::user()->company_id,
            ]);
        }
        return redirect()->route('fundingsources.index', [ 'executive_target_id' => request()->input('executive_target_id')])->with('success', trans('fundingsources.created'));
    }

    public function edit(FundingSources $fundingsource)
    {
        $this->authorize(__FUNCTION__, FundingSources::class);

      


        return view('master.fundingsources.edit', ['fundingsource' => $fundingsource,]);
    }




    public function update(Request $request, FundingSources $fundingsource)
    {
        $this->authorize(__FUNCTION__, FundingSources::class);
        $fundingsource->update($request->except('_token'));
        Auth::user()->clearCache();

        return redirect()->route('fundingsources.index', [ 'executive_target_id' => request()->input('executive_target_id')])->with('success',trans('fundingsources.updated'));
    }



  
       public function destroy($id)
       {
           $fundingsource =FundingSources::Find($id);
           $fundingsource->delete();
   
           return back()->with('success',trans('fundingsources.deleted'));
       }
   
}
