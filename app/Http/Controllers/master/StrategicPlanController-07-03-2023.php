<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\StrategicPlan;
use Illuminate\Support\Facades\Auth;
use App\Models\Master\PlanYearMonthWeeksView;
use App\Models\Master\PlanImplWeeks;
use DB;
  



//الخطة الاستراتيجية
class StrategicPlanController extends Controller
{
    public function index()
    {
        $this->authorize(__FUNCTION__,StrategicPlan::class);
        if(Auth::user()->is_super_admin || is_null(Auth::user()->company_id)){
            $strategicplan = StrategicPlan::orderBy('id')->paginate(10);
           

        }else
        {
            $strategicplan = StrategicPlan::where('company_id',Auth::user()
            ->company_id)->orderBy('company_id')->paginate(10);
          

        }
        return view('master.strategicplan.index',[
            'strategicplan'=>$strategicplan,
          

        ]);
    }

    public function create()
    {
        $this->authorize(__FUNCTION__,StrategicPlan::class);
        return view('master.strategicplan.create');
    }


    public function store(Request $request)
    {

      $request->validate([

        'start_plan_date' => 'required',
        'end_plan_date' => ['required', 'after_or_equal:start_plan_date'],

    ], [

        'end_plan_date.after_or_equal' => 'يجب ادخال  تاريخ بداية الخطة اكبرمن تاريخ نهاية الخطة',

    ]);
        StrategicPlan::create($request->input());

        Auth::user()->clearCache();
        return redirect()->route('strategicplan.index')->with('success',trans('strategicplan.created'));
    }


    public function show(StrategicPlan $route)
    {
        $this->authorize(__FUNCTION__,StrategicPlan::class);
        return view('master.strategicplan.show',[
            'strategicplan'=>$strategicplan,
        ]);
    }


    public function edit(StrategicPlan $strategicplan){
        $this->authorize(__FUNCTION__,StrategicPlan::class);
        return view('master.strategicplan.edit',[
            'strategicplan'=>$strategicplan
        ]);
    }

    public function update(Request $request,StrategicPlan $strategicplan){
        $this->authorize(__FUNCTION__,StrategicPlan::class);
        $strategicplan->update($request->except('_token'));
        return redirect()->route('strategicplan.index')->with('success',trans('strategicplan.updated'));
    }
    
    public function destroy($id)
    {
        $strategicplan =StrategicPlan::Find($id);
        $strategicplan->delete();

        return redirect()->route('strategicplan.index')->with('success',trans('strategicplan.deleted'));
    }

    public function stat_plan(Request $request)
    {
        $planyears = PlanYearMonthWeeksView ::get();      
        $MONTH = 1;     
        $v_month=0;
        $v_week=0;
        $week=1;
      
        //// new procedure to insert data in table  PlanImplWeeks 29/01/2023
          //للدوران على سجلات الاعوام
        foreach($planyears as $planyear){
              $year = $planyear->year;
              $start_plan_date = \Carbon\Carbon::createFromFormat('Y-m-d', $planyear->start_plan_date) ;  
                                                            
                  //الشهر
                  for ($v_month= 0; $v_month <$planyear->MONTH_NO && $year=$year++  ;   $v_month++) {
                            //الاسبوع
                              for ($v_week= 0; $v_week <4  ;   $v_week++) {
                                //insert into table 
                                    $planimplweeks = PlanImplWeeks::create([
                                      'year'                            => $year,
                                      'month_no'                    => $MONTH,
                                      'strategic_plan_id'            => $planyear->strategic_plan_id,
                                      'plan_week_no'                =>$week,
                                      'start_date_week'                =>$start_plan_date
                                    ]);
                                     //اضافة الايام الى تاريخ البداية
                                 $day=7;
                                $start_plan_date=$start_plan_date->addDays($day) ;
                                    $week=  $week + 1; 
                              }
                    $start_plan_date =  $start_plan_date->addMonth()  ; //لاضافة شهر الى تاريخ البداية
                    $MONTH=  $MONTH + 1;     //لعدد الشهور الملحقة
                    $year =  $start_plan_date->format('Y'); // لاستخراج السنة بعد زيادة الشهور
                  }                   
        }
               return response()->json($planimplweeks, 201);
    }



    public function data_found( $strategic_plan_id){

        $no_data_found=PlanImplWeeks::select(DB::raw('count([id]) '))
                                           ->where('strategic_plan_id',$strategic_plan_id)
                                           ->first();
    if(!$no_data_found){
         $message=0;  ///تم تكرار البيانات

     }
     else{
      $message=1; // يوجد بيانات
     }
   return response()->JSON(['message' => $message],200);

              }
}
