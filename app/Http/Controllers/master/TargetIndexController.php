<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Master\TargetIndex;
use App\Models\Master\TargetIndexPlanExecutiveView;
use App\Models\Master\PerfornanceIndex;
use App\Models\Master\ExecutiveTarget;
 
use DB;

class TargetIndexController extends Controller
{
    public function index()
    {

        $this->authorize(__FUNCTION__, TargetIndexPlanExecutiveView::class);
        $executive_target_id = request()->input('executive_target_id'); //20143;
        //dd($maniest_id);
        $targetIndexplanExecutiveview = TargetIndexPlanExecutiveView::where('executive_target_id', $executive_target_id)
        
        ->get();
        return view('master.targetindex.index', ['targetIndexplanExecutiveview' => $targetIndexplanExecutiveview,]);
    }

        

    public function create()
    {
        $this->authorize(__FUNCTION__,TargetIndex::class);


       
      $perfornanceindex = DB::table('perfornance_index')->select('id', 'perfornance_index_name')->distinct('id')
            ->get();
        return view('master.targetindex.create',[
        'perfornanceindex'=> $perfornanceindex]);
    }


    public function store(Request $request)
    {

       
        $user = Auth::user();
        $targetindex = TargetIndex::create($request->input());
      
        $targetindex->save();
        Auth::user()->clearCache();
        return redirect()->route('targetindex.index')->with('success',trans('targetindex.created'));
    }



    public function edit(TargetIndex $targetindex)
    {
        $this->authorize(__FUNCTION__,TargetIndex::class);
       
            $perfornanceindex = DB::table('perfornance_index')->select('id', 'perfornance_index_name')->distinct('id')
            ->get();
        return view('master.targetindex.edit',[
             'targetindex' => $targetindex,
            'perfornanceindex' => $perfornanceindex,
        ]);
    }


    public function update(Request $request, TargetIndex $targetindex)
    {


        $this->authorize(__FUNCTION__,TargetIndex::class);
    $targetindex->update($request->except('_token'));
    return redirect()->route('targetindex.index', [ 'executive_target_id' => request()->input('executive_target_id')])->with('success',trans('targetindex.updated'));
    }


    public function destroy($id)
    {
        $targetindex =TargetIndex::Find($id);
        $targetindex->delete();

        return redirect()->route('targetindex.index')->with('success',trans('targetindex.deleted'));
    }
}
