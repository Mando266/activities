<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\MonthWeek;
use App\Models\Master\ExecutiveTarget;
use App\Models\Master\StrategicPlan;
use Illuminate\Support\Facades\Auth;
//use App\Models\master\Activity;
use DB;
use App\Models\Master\ActivityFollowPlanWeeksView;

class MonthWeekController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $strategic_plan_id      = request()->input('strategic_plan_id');
        $start_plan_date        = request()->input('start_plan_date');
        $end_plan_date          = request()->input('end_plan_date');
        $company_id          = request()->input('company_id');


        $months = DB::table('activity_status_plan_view')
            ->select('month_no', 'year')
            ->distinct('month_no')
            ->where('strategic_plan_id', $strategic_plan_id)
            ->get();


        foreach ($months as $key => $value) {
            $num_days = cal_days_in_month(CAL_GREGORIAN, $value->month_no, $value->year);
            $months[$key]->num_days = $num_days;
        }


        $data = DB::table('activity_follow_up_plan_weeks_comp_view')
            ->where('strategic_plan_id', $strategic_plan_id)
            ->where('start_plan_date', $start_plan_date)
            ->where('end_plan_date', $end_plan_date)
            ->where('company_id', $company_id)
            ->orderBy('executive_target_name', 'desc')
            ->get();
        $rowspans
            =
            DB::table(DB::raw('(SELECT DISTINCT executive_target_name, activity_name FROM activity_follow_up_plan_weeks_comp_view
            WHERE strategic_plan_id = ' . $strategic_plan_id . '
            and company_id = ' . $company_id . '
            and start_plan_date = "' . $start_plan_date . '"
            and end_plan_date = "' . $end_plan_date . '") AS subquery_name'))
            ->select(DB::raw('COUNT(1)'), 'executive_target_name')
            ->orderBy('executive_target_name', 'desc')

            // ->orderBy('id')
            ->groupBy('executive_target_name')
            ->get();

        $strategic_target = DB::table('activity_follow_up_plan_weeks_comp_view')
            ->select('strategic_target_id', 'strategic_target_name')
            ->where('company_id', $company_id)
            ->first();
        //dd($strategic_target);
        $get_strategic_target_id = DB::table('activity_follow_up_plan_weeks_comp_view')
            ->select('strategic_target_id')
            ->where('strategic_plan_id', $strategic_plan_id)
            ->where('start_plan_date', $start_plan_date)
            ->where('end_plan_date', $end_plan_date)
            ->where('company_id', $company_id)->first();

        $strategicplan = DB::table('strategic_plan')->select('id', 'strategic_plan_name')->distinct('id')
            ->where('id', $strategic_plan_id)
            ->first();

        $column_headers = DB::table('activity_status_plan_view')
        ->select('plan_week_no', 'month_no')
        ->distinct('plan_week_no')
        ->where('strategic_plan_id', $strategic_plan_id)
            ->get();
        //Determine the row headers and column headers
        $row_headers = $data->pluck('activity_name', 'activity_id')->unique();
        $columns = $data->pluck('plan_week_no')->unique();
        // $column_headers = $data->plan_week_no;
        // foreach ($data->plan_week_no as $header) {
        //     echo $header;
        // }
        // dd($header);
        $executive_target_names = $data->pluck('executive_target_name', 'executive_target_id')->unique();



        return view('master.planWeeks.index', [
            'row_headers' => $row_headers,
            'column_headers' => $column_headers,
            'data' => $data,
            'columns' => $columns,
            'months' => $months,
            'strategicplan' => $strategicplan,
            'executive_target_names' => $executive_target_names,
            'get_strategic_target_id' => $get_strategic_target_id,
            'strategic_target' => $strategic_target,
            'rowspans' => $rowspans
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company_id          =
            Auth::user()->company_id;

        $this->authorize(__FUNCTION__, ActivityFollowPlanWeeksView::class);

        $strategic_target = DB::table('activity_follow_up_plan_weeks_comp_view')
            ->select('strategic_target_id', 'strategic_target_name')
            ->where('company_id', $company_id)
            ->first();
        // dd($strategic_target);
        if (Auth::user()->is_super_admin && is_null(Auth::user()->company_id)) {

            $executivetarget = DB::table('executive_target')->select('id', 'executive_target_name')->distinct('id')
                ->get();
            $company = DB::table('company')->select('id', 'name')
                ->where('id', '!=', '4')
                ->get();
            $strategicplan = DB::table('strategic_plan')->select('id', 'strategic_plan_name', 'revenue_achieved')->distinct('id')
                ->get();
            $activityfollowplanview = ActivityFollowPlanWeeksView::get();
        } else {
            $activityfollowplanview = ActivityFollowPlanWeeksView::get();
            $executivetarget = DB::table('executive_target')->select('id', 'executive_target_name')->distinct('id')
                ->get();
            $strategicplan = DB::table('strategic_plan')->where('company_id', Auth::user()->company_id)->select('id', 'strategic_plan_name', 'revenue_achieved')->distinct('id')
                ->get();
            $company = DB::table('company')->select('id', 'name')

                ->get();
        }
        // dd($executivetarget);
        return view('master.planWeeks.create', [
            'activityfollowplanview' => $activityfollowplanview,
            'executivetarget' => $executivetarget,
            'strategicplan' => $strategicplan,
            'company' => $company,
            'company_id' => $company_id,
            'strategic_target' => $strategic_target,

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $monthWeek = new MonthWeek([
        //     'month' => $request->get('month'),
        //     'week' => $request->get('week')
        // ]);

        // $monthWeek->save();

        // return redirect('/planWeeks')->with('success', 'Month and week saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $monthWeek = MonthWeek::find($id);

        // return view('month-week.edit', compact('monthWeek'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $monthWeek = MonthWeek::find($id);
        $monthWeek->delete();

        return redirect('/month-week')->with('success', 'Month and week deleted!');
    }




    public function executivetarget($id)
    {
        $execut = ExecutiveTarget::where('id', $id)->select('target_from', 'target_to')->first();

        return response()->JSON(['execut' => $execut], 200);
    }

    public function strategicget($id)
    {
        $strat = StrategicPlan::where('id', $id)->distinct()->select('start_plan_date', 'end_plan_date', 'revenue_achieved')->first();

        return response()->JSON(['strat' => $strat], 200);
    }
    public function compstat($id)
    {
        $comp = ActivityFollowPlanWeeksView::where('company_id', $id)->distinct()->select('company_id', 'strategic_plan_name', 'strategic_plan_id', 'start_plan_date', 'end_plan_date', 'revenue_achieved')->get();

        return response()->JSON(['comp' => $comp], 200);
    }
}
