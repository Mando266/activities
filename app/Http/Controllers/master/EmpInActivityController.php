<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\EmpInActivity;
use App\Models\Master\ActivityEmpAdminView;
use App\Models\Master\AdminUnit;
use App\Models\Master\Employee;

use DB;

use Illuminate\Support\Facades\Auth;
//الموظفين المشاركين فى النشاط

class EmpInActivityController extends Controller
{
    public function index()
    {
        $this->authorize(__FUNCTION__, ActivityEmpAdminView::class);
        $activity_id = request()->input('activity_id');
        if (Auth::user()->is_super_admin || is_null(Auth::user()->company_id)) {
            $activityempadminview = ActivityEmpAdminView::orderBy('id')
                ->where('activity_id', '=', request()->input('activity_id'))
                -> where('company_id',Auth::user()->company_id)
                ->paginate(10);
        } else {
            $activityempadminview = ActivityEmpAdminView::where('company_id', Auth::user()
                ->company_id)
                ->where('activity_id', '=', request()->input('activity_id'))
                ->orderBy('company_id')->paginate(10);
        }
        return view('master.empinactivity.index', [
            'activityempadminview' => $activityempadminview, 'activity_id' => $activity_id,


        ]);
    }

    public function create()
    {
        $this->authorize(__FUNCTION__, EmpInActivity::class);
        //  $admin_unit_id= request()->input('admin_unit_id');
        $emp = Employee::where('company_id', Auth::user()->company_id)->where('id', '!=', request()->input('emp_id'))
            ->get();

        $adminunit = AdminUnit::where('company_id', Auth::user()->company_id)

            ->get();

        //dd($adminunit);

        return view('master.empinactivity.create', ['adminunit' => $adminunit, 'emp' => $emp,]);
    }



    public function store(Request $request)
    {


        foreach ($request->input('empinactivity', []) as $empinactivity) {
            EmpInActivity::create([
                'admin_unit_id'      => $request->input('admin_unit_id'),

                'employee_id'   => $empinactivity['employee_id'],
                'activity_id' => $empinactivity['activity_id'],
                'notes' => $empinactivity['notes'],
            ]);
        }
        return redirect()->route('empinactivity.index', ['id' => request()->input('id'), 'activity_id' => request()->input('activity_id')])->with('success', trans('empinactivity.created'));
    }



    public function edit(EmpInActivity $empinactivity)
    {
        $this->authorize(__FUNCTION__, EmpInActivity::class);
        $emp = Employee::where('company_id', Auth::user()->company_id)->where('id', '!=', request()->input('emp_id'))
            ->get();
        $adminunit = AdminUnit::where('company_id', Auth::user()->company_id)

            ->get();



        return view('master.empinactivity.edit', ['empinactivity' => $empinactivity, 'emp' => $emp, 'adminunit' => $adminunit]);
    }




    public function update(Request $request, EmpInActivity $empinactivity)
    {
        $this->authorize(__FUNCTION__, EmpInActivity::class);
        $empinactivity->update($request->except('_token'));
        Auth::user()->clearCache();
        return redirect()->route('empinactivity.index', ['activity_id' => $empinactivity->activity_id,])
            ->with('success', trans('empinactivity.updated'));
    }



    public function destroy($id)
    {
        $empinactivity = EmpInActivity::Find($id);
        $empinactivity->delete();

        return redirect()->route('empinactivity.index', ['activity_id' => $empinactivity->activity_id])->with('success', trans('empinactivity.deleted'));
    }
}
