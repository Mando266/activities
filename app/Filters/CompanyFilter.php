<?php
namespace App\Filters;

use App\Filters\AbstractBasicFilter;

class CompanyFilter extends AbstractBasicFilter{
    public function filter($value)
    {
        return $this->builder->where('usage_id','like',"%{$value}%")
        ->orWhere('company_id',$value)->orWhere('contract_id',$value)->orWhere('gov_id',$value);

    }
}
