<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class TargetOutputExecutiveCompView extends Model
{
    protected $table = 'target_output_executive_comp_view';
    protected $guarded = [];
}
