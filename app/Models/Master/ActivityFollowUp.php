<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class ActivityFollowUp extends Model
{
    protected $table = 'activity_follow_up';
    protected $guarded = [];
}
