<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class TargetIndex extends Model
{
     
    protected $table = 'target_index';
    protected $guarded = [];
}
