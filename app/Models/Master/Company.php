<?php

namespace App\Models\Master;


use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'company';
    protected $guarded = [];

    public function strategic_plan(){
        return $this->hasMany(StrategicPlan::class,'company_id','id');

    }
}
