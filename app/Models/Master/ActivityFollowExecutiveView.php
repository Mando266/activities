<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class ActivityFollowExecutiveView extends Model
{
    protected $table = 'activity_follow_executive_view';
    protected $guarded = [];
}
