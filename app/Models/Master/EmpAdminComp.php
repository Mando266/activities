<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class EmpAdminComp extends Model
{
    protected $table = 'emp_admin_comp';
    protected $guarded = [];
}
