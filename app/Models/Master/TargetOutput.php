<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class TargetOutput extends Model
{
    protected $table = 'target_output';
    protected $guarded = [];
}
