<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class StrategicPlan extends Model
{
    protected $table = 'strategic_plan';
    protected $guarded = [];

    public function companies(){
        return $this->belongsTo(Country::class,'company_id','id');
    }
}
