<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class FollowActivityExeView extends Model
{
    protected $table = 'follow_activity_exe_view';
    protected $guarded = [];
}
