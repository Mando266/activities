<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class PlanImplWeeksCompView extends Model
{
    protected $table = 'plan_impl_weeks_comp_view';
    protected $guarded = [];
}
