<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class TargetIndexPlanExecutiveView extends Model
{
    protected $table = 'target_index_plan_executive_view';
    protected $guarded = [];
}
