<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class ActivityFollowUpView extends Model
{
    protected $table = 'activity_follow_up_view';
    protected $guarded = [];
}
