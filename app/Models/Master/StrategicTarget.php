<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class StrategicTarget extends Model
{
    
    protected $table = 'strategic_target';
    protected $guarded = [];

}
