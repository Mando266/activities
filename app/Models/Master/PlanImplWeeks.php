<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class PlanImplWeeks extends Model
{
    protected $table = 'plan_impl_weeks';
    protected $guarded = [];
}
