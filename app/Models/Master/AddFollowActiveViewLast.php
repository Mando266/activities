<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class AddFollowActiveViewLast extends Model
{
    protected $table = 'add_follow_active_view';
    protected $guarded = [];
}
