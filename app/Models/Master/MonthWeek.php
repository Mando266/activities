<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class MonthWeek extends Model
{
    protected $table = 'months_weeks';

    protected $fillable = ['month', 'week'];
}
