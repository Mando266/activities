<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class PlanYearMonthWeeksView extends Model
{
    protected $table = 'plan_year_month_weeks_view_last';
    protected $guarded = [];
}
