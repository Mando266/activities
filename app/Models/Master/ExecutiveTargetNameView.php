<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class ExecutiveTargetNameView extends Model
{
    protected $table = 'executive_target_name_view';
    protected $guarded = [];
}
