<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class FundingSources extends Model
{
    protected $table = 'funding_sources';
    protected $guarded = [];
}
