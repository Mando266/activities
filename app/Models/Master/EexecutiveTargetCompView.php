<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class EexecutiveTargetCompView extends Model
{
    protected $table = 'executive_target_comp_view';
    protected $guarded = [];
}
