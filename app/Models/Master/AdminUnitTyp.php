<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class AdminUnitTyp extends Model
{
    protected $table = 'admin_unit_typ';
    protected $guarded = [];
}
