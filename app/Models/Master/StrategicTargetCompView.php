<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class StrategicTargetCompView extends Model
{
    protected $table = 'strategic_target_comp_view';
    protected $guarded = [];

}
