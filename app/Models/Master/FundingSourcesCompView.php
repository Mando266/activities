<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class FundingSourcesCompView extends Model
{
    protected $table = 'funding_sources_comp_view';
    protected $guarded = [];
}
