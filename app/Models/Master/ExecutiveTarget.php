<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class ExecutiveTarget extends Model
{
    protected $table = 'executive_target';
    protected $guarded = [];
}
