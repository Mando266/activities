<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class StatusActivEndView extends Model
{
    protected $table = 'status_activ_end_view';
    protected $guarded = [];
}
