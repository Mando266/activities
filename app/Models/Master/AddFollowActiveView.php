<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class AddFollowActiveView extends Model
{
    protected $table = 'add_follow_active_view1';
    protected $guarded = [];
}
