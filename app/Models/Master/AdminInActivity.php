<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class AdminInActivity extends Model
{
    protected $table = 'admin_in_activity';
    protected $guarded = [];
}
