<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class ActivityFollowPlanWeeksView extends Model
{
    protected $table = 'activity_follow_up_plan_weeks_comp_view';
    protected $guarded = [];
}
