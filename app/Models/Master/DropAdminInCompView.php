<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class DropAdminInCompView extends Model
{
    protected $table = 'drop_admin_in_comp_view';
    protected $guarded = [];
}
