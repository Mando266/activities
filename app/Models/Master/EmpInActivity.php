<?php

namespace App\models\master;

use Illuminate\Database\Eloquent\Model;

class EmpInActivity extends Model
{
    protected $table = 'emp_in_activity';
    protected $guarded = [];
}
