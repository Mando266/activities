<?php

use App\Models\Master\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Master\MonthWeekController;
use App\Models\Master\StrategicPlanController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
//Route::group(['middleware' => 'auth:sanctum'], function () {

    Route::get('get-company-users',function(){
        $copmany =  Company::find(request()->input('company_id'));
        if(!is_null($copmany)){
            return $copmany->users()->get(['users.id','users.name']);
        }
        return [];
    });


    Route::post('/route', 'Master\EexecutiveTargetController@plan');//rehab
    Route::post('/plan', 'Master\StrategicPlanController@planstat');//rehab
    Route::post('/vv', 'Master\EexecutiveTargetController@ss');//rehab
   // Route::get('/execut/{id}', 'master\MonthWeekController@executivetarget');//rehab

    Route::get('/execut/{id}', 'Master\MonthWeekController@executivetarget');
    Route::get('/strat/{id}','Master\MonthWeekController@strategicget');
    Route::post('/statplan', 'Master\StrategicPlanController@stat_plan');//rehab-1-3-2023
    Route::get('/datafound/{strategic_plan_id}', 'Master\StrategicPlanController@data_found');//rehab-1-3-2023
    Route::get('/comp/{id}','Master\MonthWeekController@compstat');//rehab 16-3-2023

    Route::put('/updatefollow/{activity_id}/{planned_from}/{planned_to}','Master\ActivityFollowUpController@followupdate');
    Route::put('/dd/{activity_id}/{planned_from}/{planned_to}','Master\ActivityFollowUpController@followupdate');
    Route::put('/endactive','Master\ActivityFollowUpController@activityend');
    Route::post('/test88', 'Master\StrategicPlanController@planstat');//rehab-1-3-2023
    




    Route::post('/yy', 'Master\StrategicPlanController@test');//rehab-1-3-2023


//});
