<?php

use App\Models\ViewModel\RootMenuNode;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    /*
    |-------------------------------------------
    | admin routes
    |--------------------------------------------
    */
    Route::prefix('admin')->namespace('Admin')->group(function () {
        Route::resource('roles', 'RoleController');
        Route::resource('users', 'UserController');
        Route::get('profile', 'UserController@showProfile')->name('profile');

        Route::get('reset-password', 'ResetPasswordController@edit')->name('user.reset-password');
        Route::put('reset-password', 'ResetPasswordController@update');
    });
    /*
    |-------------------------------------------
    | master routes
    |--------------------------------------------
    */
    Route::prefix('master')->namespace('Master')->group(function () {
        Route::resource('company', 'CompanyController');
        Route::resource('activity', 'ActivityController');
        Route::resource('executivetarget', 'EexecutiveTargetController');
        Route::resource('empinactivity', 'EmpInActivityController');
        Route::resource('strategicplan', 'StrategicPlanController');
        Route::resource('employee', 'EmployeeController');
        Route::resource('adminunit', 'AdminUnitController');

        Route::resource('activityfollowup', 'ActivityFollowUpController');
        Route::resource('targetoutput', 'TargetOutputController');
        Route::resource('admininactivity', 'AdminInCompController');
        Route::resource('tasks', 'TasksController');
        Route::resource('planWeeks', 'MonthWeekController');

        Route::resource('fundingsources', 'FundingSourcesController');

        Route::resource('perfornanceindex', 'PerfornanceIndexController');
        Route::resource('strategictarget', 'StrategicTargetController');
        Route::resource('targetindex', 'TargetIndexController');
        Route::resource('follow', 'FollowActivityExeViewController');

    });
});
Auth::routes(['register' => false]);
